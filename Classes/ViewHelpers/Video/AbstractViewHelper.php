<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\ViewHelpers\Video;

/**
 * @package     imia_pageteaser
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
abstract class AbstractViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * @var array
     */
    static protected $provider = [];

    /**
     * @var array
     */
    static protected $identifier = [];

    /**
     * @param string $url
     * @return string
     */
    public function getProvider($url)
    {
        $hash = md5($url);
        if (!array_key_exists($hash, self::$provider)) {
            self::$provider[$hash] = null;

            if (stripos($url, 'youtube.com') || stripos($url, 'youtu.be')) {
                self::$provider[$hash] = 'youtube';
            } elseif (stripos($url, 'vimeo.com')) {
                self::$provider[$hash] = 'vimeo';
            } elseif (stripos($url, 'dailymotion.com')) {
                self::$provider[$hash] = 'dailymotion';
            }
        }

        return self::$provider[$hash];
    }

    /**
     * @param string $url
     * @return string
     */
    public function getIdentifier($url)
    {
        $hash = md5($url);
        if (!array_key_exists($hash, self::$identifier)) {
            self::$identifier[$hash] = null;
            $matches = null;

            switch ($this->getProvider($url)) {
                case 'youtube':
                    preg_match('/(youtube\.com\/(embed\/|watch.*?v=)|youtu\.be\/)([^=&]+)/ism', $url, $matches);
                    if ($matches[3]) {
                        self::$identifier[$hash] = $matches[3];
                    }
                    break;
                case 'vimeo':
                    preg_match('/(player\.vimeo.com\/video\/([^\?\/]+)|vimeo\.com\/([^\?\/]+))/ism', $url, $matches);
                    if ($matches[2]) {
                        self::$identifier[$hash] = $matches[2];
                    } elseif ($matches[3]) {
                        self::$identifier[$hash] = $matches[3];
                    }
                    break;
                case 'dailymotion':
                    preg_match('/dailymotion.com\/(embed\/)?video\/([^\?\/_]+)/ism', $url, $matches);
                    if ($matches[2]) {
                        self::$identifier[$hash] = $matches[2];
                    }
                    break;
            }
        }

        return self::$identifier[$hash];
    }
}