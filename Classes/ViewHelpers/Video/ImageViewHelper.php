<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\ViewHelpers\Video;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_pageteaser
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class ImageViewHelper extends AbstractViewHelper
{
    /**
     * Invokes a method of an object
     *
     * @param object $page
     * @return string
     */
    public function render(\TYPO3\CMS\Extbase\DomainObject\AbstractEntity $page = null)
    {
        if (!$page) {
            $page = $this->renderChildren();
        }

        $imageUrl = '';
        if ($this->getIdentifier($page->getUrl())) {
            switch ($this->getProvider($page->getUrl())) {
                case 'youtube':
                    $url = 'http://img.youtube.com/vi/' . $this->getIdentifier($page->getUrl()) . '/hqdefault.jpg';
                    $imageUrl = 'typo3temp/pics/youtube_' . $this->getIdentifier($page->getUrl()) . '.jpg';
                    file_put_contents(PATH_site . $imageUrl, GeneralUtility::getUrl($url));
                    break;
                case 'vimeo':
                    $pageInfo = unserialize(GeneralUtility::getUrl('http://vimeo.com/api/v2/video/' . $this->getIdentifier($page->getUrl()) . '.php'));
                    $imageUrl = 'typo3temp/pics/vimeo_' . $this->getIdentifier($page->getUrl()) . '.jpg';
                    file_put_contents(PATH_site . $imageUrl, GeneralUtility::getUrl($pageInfo[0]['thumbnail_large']));
                    break;
                case 'dailymotion':
                    $url = 'http://www.dailymotion.com/thumbnail/video/' . $this->getIdentifier($page->getUrl());
                    $imageUrl = 'typo3temp/pics/dailymotion_' . $this->getIdentifier($page->getUrl()) . '.jpg';
                    file_put_contents(PATH_site . $imageUrl, GeneralUtility::getUrl($url));
                    break;
            }
        }

        return $imageUrl;
    }
}