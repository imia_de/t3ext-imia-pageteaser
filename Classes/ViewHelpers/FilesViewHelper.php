<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\ViewHelpers;

use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_pageteaser
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class FilesViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * @var FileRepository
     */
    protected static $fileRepository;

    /**
     * @var array
     */
    protected static $files = [];

    /**
     * @param string $table
     * @param string $field
     * @param integer $id
     * @return array
     */
    public function render($table = 'pages', $field = 'media', $id = null)
    {
        if (!$id) {
            $page = $this->renderChildren();
            if (is_array($page)) {
                $id = (int)$page['uid'];
            } elseif (is_object($page)) {
                if (method_exists($page, 'getUid')) {
                    $id = $page->getUid();
                } elseif (method_exists($page, 'getId')) {
                    $id = $page->getId();
                } else {
                    $id = $page->id;
                }
            } else {
                $id = (int)$page;
            }
        }

        $key = $table . '_' . $field . '_' . $id;
        if (!array_key_exists($key, self::$files)) {
            self::$files[$key] = $this->getFileRepository()->findByRelation($table, $field, $id);
        }

        return self::$files[$key];
    }

    /**
     * @return FileRepository
     */
    protected function getFileRepository()
    {
        if (!self::$fileRepository) {
            self::$fileRepository = GeneralUtility::makeInstance(FileRepository::class);
        }

        return self::$fileRepository;
    }
}