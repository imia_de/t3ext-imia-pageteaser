<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2017 IMIA net based solutions (info@imia.de)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

namespace IMIA\ImiaPageteaser\ViewHelpers\Uri;

use IMIA\ImiaBaseExt\Service\FrontendService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * @package     imia_pageteaser
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class PageViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\Uri\PageViewHelper
{
    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('language', 'int', 'target sys_language_uid', 0);
    }

    /**
     * @param array $arguments
     * @param \Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return string Rendered page URI
     */
    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $oldUriBuilder = $renderingContext->getControllerContext()->getUriBuilder();
        if (TYPO3_MODE != 'FE') {
            $arguments['absolute'] = true;
            FrontendService::replace((int)$arguments['pageUid'], $arguments['language']);
            $renderingContext->getControllerContext()->setUriBuilder(FrontendService::getUriBuilder());
        }

        $uri = parent::renderStatic($arguments, $renderChildrenClosure, $renderingContext);

        if (TYPO3_MODE != 'FE') {
            FrontendService::reset();
            $renderingContext->getControllerContext()->setUriBuilder($oldUriBuilder);
        }

        return $uri;
    }
}