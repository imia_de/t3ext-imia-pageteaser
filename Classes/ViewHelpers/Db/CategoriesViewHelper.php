<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 IMIA net based solutinos (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\ViewHelpers\Db;

use IMIA\ImiaPageteaser\Domain\Repository\CategoryRepository;
use TYPO3\CMS\Core\Database\DatabaseConnection;

/**
 * @package     imia_pageteaser
 * @subpackage  ViewHelpers
 * @author      Lars Siemon <l.siemon@imia.de>
 */
class CategoriesViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @param integer $uid
     * @param array $record
     * @param string $repositoryClass
     * @param string $table
     * @param string $field
     * @return array
     */
    public function render($uid = null, $record = null, $repositoryClass = CategoryRepository::class,
                           $table = 'pages', $field = 'categories')
    {
        if (!$uid) {
            if (!$record) {
                $record = $this->renderChildren();
            }

            if (is_array($record) && $record['uid']) {
                $uid = (int)$record['uid'];
            } elseif (is_object($record) && method_exists('getUid', $record)) {
                $uid = (int)$record->getUid();
            } else {
                $uid = (int)$record;
            }
        }

        return $this->getRecordCategories($uid, $repositoryClass, $table, $field);
    }

    /**
     * @param integer $uid
     * @param string $repositoryClass
     * @param string $table
     * @param string $field
     * @return array
     */
    protected function getRecordCategories($uid, $repositoryClass, $table = 'pages', $field = 'categories')
    {
        $fieldNameCondition = ' AND fieldname = ' . $this->getDb()->fullQuoteStr($field, 'sys_category_record_mm');
        $tableNameCondition = ' AND tablenames = ' . $this->getDb()->fullQuoteStr($table, 'sys_category_record_mm');
        if (!$this->categoryRepository && $repositoryClass) {
            $this->categoryRepository = $this->objectManager->get($repositoryClass);
        }

        if ((float)TYPO3_version < 6.2) {
            $fieldNameCondition = '';
        }

        $result = $this->getDb()->exec_SELECTquery(
            'uid_local',
            'sys_category_record_mm',
            'uid_foreign = ' . (int)$uid . $tableNameCondition . $fieldNameCondition);

        $categoryUids = [];
        while ($row = $this->getDb()->sql_fetch_assoc($result)) {
            $categoryUids[] = (int)$row['uid_local'];
        }

        if (!$this->categoryRepository) {
            return $categoryUids;
        } else {
            if (count($categoryUids) > 0) {
                return $this->categoryRepository->findByInUid($categoryUids);
            } else {
                return [];
            }
        }
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}