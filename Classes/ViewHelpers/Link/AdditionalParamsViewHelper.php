<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\ViewHelpers\Link;

use IMIA\ImiaPageteaser\Domain\Repository\PageRepository;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * @package     imia_pageteaser
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class AdditionalParamsViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * @var array
     */
    private static $rootLineIds = [];

    /**
     * @var array
     */
    private static $mountPoints = [];

    /**
     * @param AbstractEntity $page
     * @param AbstractEntity $indexPage
     * @return array
     */
    public function render(AbstractEntity $indexPage, AbstractEntity $page = null)
    {
        $additionalParams = [];
        if (!$page) {
            $page = $this->renderChildren();
        }

        if ($page) {
            if ($indexPage->getUid() == $page->getPid()) {
                /** @var \IMIA\ImiaPageteaser\Domain\Model\Page $mountPage */
                $mountPage = $indexPage;
            } else {
                /** @var \IMIA\ImiaPageteaser\Domain\Model\Page $mountPage */
                $mountPage = $this->objectManager
                    ->get(PageRepository::class)
                    ->findOneByUid($page->getPid());
            }

            if ($mountPage && $mountPage->getDoktype() == 7) {
                // mount page found
            } else {
                $rootLineIds = $this->getRootlineIds();
                if ($mountPage && is_object($mountPage) && !in_array($mountPage->getPid(), $rootLineIds)
                    & !in_array($indexPage->getUid(), $this->getRootlineIds($page->getPid()))
                ) {
                    if (count($rootLineIds) == 1) {
                        $parentUid = $rootLineIds[0];
                    } else {
                        $parentUid = $rootLineIds[1];
                    }

                    $mountPointUid = $this->findMountPoint($parentUid, $this->getRootlineIds($page->getPid()));
                    $mountPage = $this->objectManager
                        ->get(PageRepository::class)
                        ->findOneByUid($mountPointUid);
                } else {
                    $mountPage = null;
                }
            }

            if ($mountPage) {
                $specialTargetDomain = $this->getTsfe()->getDomainNameForPid($mountPage->getUid());
                if ($this->getTsfe()->domainNameMatchesCurrentRequest($specialTargetDomain)) {
                    $additionalParams['MP'] = $mountPage->getMountPid() . '-' . $mountPage->getUid();
                }
            }
        }

        if ($GLOBALS['TSFE']) {
            $additionalParams['L'] = $GLOBALS['TSFE']->sys_language_uid;
        }

        return $additionalParams;
    }

    /**
     * @param integer $uid
     * @param array $mointPointUids
     * @return integer|bool
     */
    protected function findMountPoint($uid, $mointPointUids)
    {
        if (!isset(self::$mountPoints[$uid])) {
            $result = $this->getDb()->exec_SELECTquery(
                'uid, mount_pid, doktype',
                'pages',
                'pid = ' . (int)$uid . $GLOBALS['TSFE']->sys_page->enableFields('pages')
            );

            while ($row = $this->getDb()->sql_fetch_assoc($result)) {
                if ($row['doktype'] == 7 && in_array($row['mount_pid'], $mointPointUids)) {
                    self::$mountPoints[$uid] = $row['uid'];

                    return self::$mountPoints[$uid];
                }

                $subresult = $this->findMountPoint($row['uid'], $mointPointUids);
                if ($subresult) {
                    self::$mountPoints[$uid] = $subresult;

                    return self::$mountPoints[$uid];
                }
            }

            self::$mountPoints[$uid] = false;
        }

        return self::$mountPoints[$uid];
    }

    /**
     * @param integer $pageId
     * @return array
     */
    protected function getRootlineIds($pageId = 0)
    {
        if (!isset(self::$rootLineIds[$pageId])) {
            self::$rootLineIds[$pageId] = [];

            if ($pageId) {
                if ($GLOBALS['TSFE']->sys_page) {
                    foreach ($GLOBALS['TSFE']->sys_page->getRootLine($pageId, '', true) as $rootLinePage) {
                        self::$rootLineIds[$pageId][] = (int)$rootLinePage['uid'];
                    }
                }
            } else {
                if ($GLOBALS['TSFE']->rootLine && is_array($GLOBALS['TSFE']->rootLine)) {
                    foreach ($GLOBALS['TSFE']->rootLine as $rootLinePage) {
                        self::$rootLineIds[$pageId][] = (int)$rootLinePage['uid'];
                    }
                }
            }
        }

        return self::$rootLineIds[$pageId];
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }

    /**
     * @return TypoScriptFrontendController
     */
    protected function getTsfe()
    {
        return $GLOBALS['TSFE'];
    }
}