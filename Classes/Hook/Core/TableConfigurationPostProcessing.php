<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\Hook\Core;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_pageteaser
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class TableConfigurationPostProcessing implements \TYPO3\CMS\Core\Database\TableConfigurationPostProcessingHookInterface
{
    public function processData()
    {
        $_EXTKEY = 'imia_pageteaser';

        $doktypes = [
            100 => 'News',
            110 => 'Gallery',
            120 => 'Product',
            130 => 'Event',
            140 => 'Article',
        ];

        $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY]);
        $enabledDoktypes = array_map('intval', explode(',', $extConf['doktypes']));
        $pageTreeHiddenDoktypes = array_map('intval', explode(',', $extConf['doktypesPageTreeHidden']));

        $standardDoktypes = $shortcutDoktypes = $removeDoktypes = $hiddenDoktypes = $specialDoktypes = [];
        foreach ($doktypes as $doktype => $name) {
            $standardDoktypes[] = $doktype;
            $shortcutDoktypes[] = $doktype + 3;
            $shortcutDoktypes[] = $doktype + 4;
            $shortcutDoktypes[] = $doktype + 6;

            if (!in_array($doktype, $enabledDoktypes)) {
                $removeDoktypes[] = $doktype;
                $removeDoktypes[] = $doktype + 3;
                $removeDoktypes[] = $doktype + 4;
                $removeDoktypes[] = $doktype + 6;
            }
            if (in_array($doktype, $pageTreeHiddenDoktypes)) {
                $hiddenDoktypes[] = $doktype;
                $hiddenDoktypes[] = $doktype + 3;
                $hiddenDoktypes[] = $doktype + 4;
                $hiddenDoktypes[] = $doktype + 6;
            }

            if ($doktype === 110) {
                $specialDoktypes[] = $doktype + 1;
                $specialDoktypes[] = $doktype + 2;
                $specialDoktypes[] = $doktype + 5;

                if (!in_array($doktype, $enabledDoktypes)) {
                    $removeDoktypes[] = $doktype + 1;
                    $removeDoktypes[] = $doktype + 2;
                    $removeDoktypes[] = $doktype + 5;
                }
                if (in_array($doktype, $pageTreeHiddenDoktypes)) {
                    $hiddenDoktypes[] = $doktype + 1;
                    $hiddenDoktypes[] = $doktype + 2;
                    $hiddenDoktypes[] = $doktype + 5;
                }
            }
        }

        if (is_array($GLOBALS['pageTreeHiddenDoktypes'])) {
            $hiddenDoktypes = array_merge($hiddenDoktypes, $GLOBALS['pageTreeHiddenDoktypes']);
        }

        $allDoktypes = array_merge($standardDoktypes, $shortcutDoktypes, $specialDoktypes);
        foreach ($standardDoktypes as $doktype) {
            $GLOBALS['PAGES_TYPES'][$doktype] = [
                'allowedTables'     => '*',
                'onlyAllowedTables' => '',
            ];
        }
        foreach ($shortcutDoktypes as $doktype) {
            $GLOBALS['PAGES_TYPES'][$doktype] = [
                'allowedTables'     => '*',
                'onlyAllowedTables' => '',
            ];
        }

        ExtensionManagementUtility::addPageTSConfig('
            TCAdefaults.pages.lastUpdated = ' . time() . '
            TCEFORM {
                pages {
                    doktype.removeItems := addToList(' . implode(',', $removeDoktypes) . ')
                    teaser_image.disabled = 1
                    teaser_pages.disabled = 1
                }
                pages_language_overlay {
                    doktype.removeItems := addToList(' . implode(',', $removeDoktypes) . ')
                    teaser_image.disabled = 1
                }
            }

            [page|doktype = ' . implode('] || [page|doktype = ', $allDoktypes) . ']
                TCEFORM {
                    pages {
                        teaser_image.disabled = 0
                        abstract.disabled = 0
                        categories.disabled = 0
                        lastUpdated.disabled = 0
                        lastUpdated.label = LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/DB.xlf:page_lastupdated
                    }
                    pages_language_overlay {
                        teaser_image.disabled = 0
                        abstract.disabled = 0
                    }
                }
            [end]
        ');

        if (count($hiddenDoktypes) > 0) {
            ExtensionManagementUtility::addUserTSConfig('
                options.pageTree.excludeDoktypes = ' . implode(',', $hiddenDoktypes) . '
            ');

            $pageDoktypes = [];
            foreach ($GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'] as $doktypeItem) {
                $pageDoktypes[] = (int)$doktypeItem[1];
            }
            $pageDoktypes = array_unique(array_merge($pageDoktypes, $allDoktypes));

            foreach ($doktypes as $doktype => $doktypeName) {
                if (in_array($doktype, $hiddenDoktypes)) {
                    $condDoktypes = [$doktype];
                    $condDoktypes[] = $doktype + 3;
                    $condDoktypes[] = $doktype + 4;
                    $condDoktypes[] = $doktype + 6;

                    if ($doktype == 110) {
                        $condDoktypes[] = $doktype + 1;
                        $condDoktypes[] = $doktype + 2;
                        $condDoktypes[] = $doktype + 5;
                    }
                    $allButNotDoktypes = array_diff($pageDoktypes, $condDoktypes);

                    ExtensionManagementUtility::addPageTSConfig('
                        [page|doktype = ' . implode('] || [page|doktype = ', $condDoktypes) . ']
                            TCEFORM {
                                pages {
                                    doktype.removeItems = ' . implode(',', $allButNotDoktypes) . '
                                }
                                pages_language_overlay {
                                    doktype.removeItems = ' . implode(',', $allButNotDoktypes) . '
                                }
                            }
                        [else]
                            TCEFORM {
                                pages {
                                    doktype.removeItems := addToList(' . implode(',', $condDoktypes) . ')
                                }
                                pages_language_overlay {
                                    doktype.removeItems := addToList(' . implode(',', $condDoktypes) . ')
                                }
                            }
                        [end]
                    ');
                }
            }
        }

        if (TYPO3_MODE == 'BE') {
            $columnsOnlyTS = GeneralUtility::_GP('columnsOnlyTS');
            $overrideVals = GeneralUtility::_GP('overrideVals');
            $edit = GeneralUtility::_GP('edit');
            $table = null;
            if ($edit && is_array($edit)) {
                $table = array_shift(array_keys($edit));
            }
            if ($columnsOnlyTS && $table) {
                $columns = array_map('trim', explode(',', $columnsOnlyTS));

                if (is_array($GLOBALS['TCA'][$table]['columns'])) {
                    $disabledFields = [];
                    foreach (array_keys($GLOBALS['TCA'][$table]['columns']) as $field) {
                        if (!in_array($field, $columns) && !(is_array($overrideVals) && is_array($overrideVals[$table]) &&
                                in_array($field, array_keys($overrideVals[$table])))
                        ) {
                            $disabledFields[] = $field;
                        }
                    }

                    if (count($disabledFields) > 0) {
                        ExtensionManagementUtility::addPageTSConfig(
                            "\nTCEFORM." . $table . " {\n" .
                            "\t" . implode(".disabled = 1\n\t", $disabledFields) . ".disabled = 1\n" .
                            "}\n"
                        );
                    }
                }
            }

            if (ExtensionManagementUtility::isLoaded('dd_googlesitemap')) {
                ExtensionManagementUtility::addFieldsToPalette(
                    'pages', 'miscalt', 'tx_ddgooglesitemap_priority, tx_ddgooglesitemap_change_frequency'
                );
            }

            $doktypes = [
                100 => 'News',
                110 => 'Gallery',
                120 => 'Product',
                130 => 'Event',
                140 => 'Article',
            ];

            /** @var \TYPO3\CMS\Core\Imaging\IconRegistry $iconRegistry */
            $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
            $iconRegistry->registerIcon(
                'contentelement-pageteaser',
                \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                ['source' => ExtensionManagementUtility::siteRelPath($_EXTKEY) . 'Resources/Public/Images/Icons/Plugin.svg']
            );
            foreach ($doktypes as $doktype => $name) {
                $iconRegistry->registerIcon(
                    'tcarecords-pages-' . $doktype,
                    \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
                    ['source' => ExtensionManagementUtility::siteRelPath($_EXTKEY) . 'Resources/Public/Images/Icons/PageDoktype' . $name . '.png']
                );
                $GLOBALS['TCA']['pages']['ctrl']['typeicon_classes'][$doktype] = 'tcarecords-pages-' . $doktype;

                $iconRegistry->registerIcon(
                    'tcarecords-pages-' . ($doktype + 3),
                    \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
                    ['source' => ExtensionManagementUtility::siteRelPath($_EXTKEY) . 'Resources/Public/Images/Icons/PageDoktype' . $name . 'External.png']
                );
                $GLOBALS['TCA']['pages']['ctrl']['typeicon_classes'][($doktype + 3)] = 'tcarecords-pages-' . ($doktype + 3);

                $iconRegistry->registerIcon(
                    'tcarecords-pages-' . ($doktype + 4),
                    \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
                    ['source' => ExtensionManagementUtility::siteRelPath($_EXTKEY) . 'Resources/Public/Images/Icons/PageDoktype' . $name . 'Shortcut.png']
                );
                $GLOBALS['TCA']['pages']['ctrl']['typeicon_classes'][($doktype + 4)] = 'tcarecords-pages-' . ($doktype + 4);

                $iconRegistry->registerIcon(
                    'tcarecords-pages-' . ($doktype + 6),
                    \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
                    ['source' => ExtensionManagementUtility::siteRelPath($_EXTKEY) . 'Resources/Public/Images/Icons/PageDoktype' . $name . 'File.png']
                );
                $GLOBALS['TCA']['pages']['ctrl']['typeicon_classes'][($doktype + 6)] = 'tcarecords-pages-' . ($doktype + 6);

                if ($doktype === 110) {
                    $iconRegistry->registerIcon(
                        'tcarecords-pages-' . ($doktype + 1),
                        \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
                        ['source' => ExtensionManagementUtility::siteRelPath($_EXTKEY) . 'Resources/Public/Images/Icons/PageDoktype' . $name . 'Images.png']
                    );
                    $GLOBALS['TCA']['pages']['ctrl']['typeicon_classes'][($doktype + 1)] = 'tcarecords-pages-' . ($doktype + 1);

                    $iconRegistry->registerIcon(
                        'tcarecords-pages-' . ($doktype + 2),
                        \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
                        ['source' => ExtensionManagementUtility::siteRelPath($_EXTKEY) . 'Resources/Public/Images/Icons/PageDoktype' . $name . 'Video.png']
                    );
                    $GLOBALS['TCA']['pages']['ctrl']['typeicon_classes'][($doktype + 2)] = 'tcarecords-pages-' . ($doktype + 2);

                    $iconRegistry->registerIcon(
                        'tcarecords-pages-' . ($doktype + 5),
                        \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
                        ['source' => ExtensionManagementUtility::siteRelPath($_EXTKEY) . 'Resources/Public/Images/Icons/PageDoktype' . $name . 'Pdf.png']
                    );
                    $GLOBALS['TCA']['pages']['ctrl']['typeicon_classes'][($doktype + 5)] = 'tcarecords-pages-' . ($doktype + 5);
                }
            }

            $iconRegistry->registerIcon(
                'tcarecords-pages-196',
                \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
                ['source' => ExtensionManagementUtility::siteRelPath($_EXTKEY) . 'Resources/Public/Images/Icons/PageDoktypeDefaultFile.png']
            );
            $GLOBALS['TCA']['pages']['ctrl']['typeicon_classes'][196] = 'tcarecords-pages-196';
        }
    }
}