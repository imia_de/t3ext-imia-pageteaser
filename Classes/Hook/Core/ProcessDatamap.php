<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutinos (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\Hook\Core;

use IMIA\ImiaPageteaser\Utility\CacheUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;
use TYPO3\CMS\Core\Utility\MathUtility;

/**
 * @package     imia_usu
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class ProcessDatamap
{
    /**
     * @param string $status
     * @param string $table
     * @param string $id
     * @param array $fields
     * @param \TYPO3\CMS\Core\DataHandling\DataHandler $dataHandler
     */
    public function processDatamap_afterDatabaseOperations($status, $table, $id, &$fields, &$dataHandler)
    {
        /** @var CacheUtility $cacheUtility */
        $cacheUtility = GeneralUtility::makeInstance(CacheUtility::class);
        $cacheUtility->clearCache($table, $id);
    }
}