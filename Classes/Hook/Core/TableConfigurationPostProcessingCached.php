<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\Hook\Core;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_pageteaser
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class TableConfigurationPostProcessingCached implements \TYPO3\CMS\Core\Database\TableConfigurationPostProcessingHookInterface
{
    public function processData()
    {
        $GLOBALS['TCA']['pages']['columns']['content_from_pid']['displayCond'] = [
            'OR' => [
                'FIELD:mount_pid_ol:REQ:false',
                'FIELD:doktype:!=:7',
            ],
        ];
        $GLOBALS['TCA']['pages']['ctrl']['requestUpdate'] .= ',mount_pid_ol';
        ExtensionManagementUtility::addToAllTCAtypes('pages', 'content_from_pid', '7', 'after:mount_pid');

        $GLOBALS['TCA']['pages_language_overlay']['columns']['sys_language_uid']['config']['showIconTable'] = false;

        $doktypeItems = $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'];
        $newDoktypeItems = [];
        foreach ($doktypeItems as $key => $item) {
            if ($item[0] == 'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.doktype.div.special') {
                $newDoktypeItems[$key] = $doktypeItems[196];
            }
            if ($key != 196) {
                $newDoktypeItems[$key] = $item;
            }
        }
        $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'] = $newDoktypeItems;

        $doktypeItemsPageAlt = $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'];
        $newDoktypeItemsPageAlt = [];
        foreach ($doktypeItemsPageAlt as $key => $item) {
            if ($item[0] == 'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.doktype.div.special') {
                $newDoktypeItemsPageAlt[$key] = $doktypeItemsPageAlt[196];
            }
            if ($key != 196) {
                $newDoktypeItemsPageAlt[$key] = $item;
            }
        }
        $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'] = $newDoktypeItemsPageAlt;
    }
}