<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\Hook\Core;

use IMIA\ImiaPageteaser\Utility\CacheUtility;
use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * @package     imia_pageteaser
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class ProcessCmdmap
{
    /**
     * @param DataHandler $dataHandler
     */
    public function processCmdmap_afterFinish(DataHandler &$dataHandler)
    {
        foreach ($dataHandler->datamap as $table => $data) {
            foreach ($data as $rawId => $fields) {
                $fields['uid'] = $dataHandler->substNEWwithIDs[$rawId] ?: $rawId;

                switch ($table) {
                    case 'pages_language_overlay':
                        $record = BackendUtility::getRecordWSOL($table, $fields['uid']);

                        foreach ($GLOBALS['TCA']['pages_language_overlay']['columns'] as $key => $column) {
                            $fieldConfig = $column['config'];
                            if ($fieldConfig['foreign_table'] == 'sys_file_reference') {
                                $references = $this->getDb()->exec_SELECTgetRows('*', 'sys_file_reference',
                                    'fieldname = ' . $this->getDb()->fullQuoteStr($key, 'sys_file_reference') .
                                    ' AND tablenames = ' . $this->getDb()->fullQuoteStr($table, 'sys_file_reference') .
                                    ' AND pid != ' . (int)$record['pid'] . ' AND uid_foreign = ' . (int)$record['uid']);

                                if ($references) {
                                    foreach ($references as $fileReference) {
                                        $fileReference['pid'] = (int)$record['pid'];

                                        $this->getDb()->exec_UPDATEquery(
                                            'sys_file_reference',
                                            'uid = ' . $fileReference['uid'],
                                            $fileReference);
                                    }
                                }
                            }
                        }

                        if ($_REQUEST['_saveandclosedok'] || $_REQUEST['_savedok'] || $_REQUEST['_savedokview']) {
                            $returnUrl = GeneralUtility::_GP('returnUrl');

                            /** @var ObjectManager $objectManager */
                            $objectManager = GeneralUtility::makeInstance(ObjectManager::class);

                            /** @var UriBuilder $uriBuilder */
                            $uriBuilder = $objectManager->get(UriBuilder::class);

                            if ($_REQUEST['_savedok'] || !$returnUrl) {
                                $columnsOnly = GeneralUtility::_GP('columnsOnly');
                                $columnsOnlyTS = GeneralUtility::_GP('columnsOnlyTS');

                                $parameters = [
                                    'edit' => [
                                        'pages_language_overlay' => [
                                            $record['uid'] => 'edit',
                                        ],
                                    ],
                                ];
                                if ($columnsOnly) {
                                    $parameters['edit']['pages_language_overlay']['columnsOnly'] = $columnsOnly;
                                }
                                if ($columnsOnlyTS) {
                                    $parameters['edit']['pages_language_overlay']['columnsOnlyTS'] = $columnsOnlyTS;
                                }

                                if ($returnUrl) {
                                    $parameters['returnUrl'] = $returnUrl;
                                }

                                $uri = $uriBuilder->buildUriFromRoute('record_edit', $parameters);
                            } elseif ($_REQUEST['_saveandclosedok']) {
                                if ($returnUrl) {
                                    $uri = $returnUrl;
                                } else {
                                    $uri = $uriBuilder->buildUriFromModule('web_layout', ['id' => $fields['pid'], 'language' => $fields['create_language']]);
                                }
                            } elseif ($_REQUEST['_savedokview']) {
                                $uri = null;
                            } else {
                                $uri = $returnUrl;
                            }

                            if ($uri && $rawId != $record['uid']) {
                                HttpUtility::redirect((string)$uri);
                            }
                        }
                        break;
                    case 'pages':
                        foreach ($GLOBALS['TCA']['pages']['columns'] as $key => $column) {
                            $fieldConfig = $column['config'];
                            if ($fieldConfig['foreign_table'] == 'sys_file_reference') {
                                $references = $this->getDb()->exec_SELECTgetRows('*', 'sys_file_reference',
                                    'fieldname = ' . $this->getDb()->fullQuoteStr($key, 'sys_file_reference') .
                                    ' AND tablenames = ' . $this->getDb()->fullQuoteStr($table, 'sys_file_reference') .
                                    ' AND pid != ' . (int)$fields['uid'] . ' AND uid_foreign = ' . (int)$fields['uid']);

                                if ($references) {
                                    foreach ($references as $fileReference) {
                                        $fileReference['pid'] = (int)$fields['uid'];

                                        $this->getDb()->exec_UPDATEquery(
                                            'sys_file_reference',
                                            'uid = ' . $fileReference['uid'],
                                            $fileReference);
                                    }
                                }
                            }
                        }

                        $record = BackendUtility::getRecordWSOL($table, $fields['uid']);
                        if ($_REQUEST['_saveandclosedok'] || $_REQUEST['_savedok'] || $_REQUEST['_savedokview']) {
                            $overlayUid = null;
                            if ($record['create_language']) {
                                $l18nCfg = 1;
                                if (in_array((int)$fields['l18n_cfg'], [2, 3])) {
                                    $l18nCfg = 3;
                                }

                                $this->getDb()->exec_UPDATEquery($table, 'uid = ' . $fields['uid'], [
                                    'create_language' => 0,
                                    'l18n_cfg'        => $l18nCfg,
                                ]);

                                $fileReferences = [];
                                $overlayFields = $record;
                                foreach ($overlayFields as $key => $value) {
                                    if (!array_key_exists($key, $GLOBALS['TCA']['pages_language_overlay']['columns'])) {
                                        unset($overlayFields[$key]);
                                    } else {
                                        $fieldConfig = $GLOBALS['TCA']['pages_language_overlay']['columns'][$key]['config'];

                                        if ($fieldConfig['foreign_table'] == 'sys_file_reference') {
                                            $references = $this->getDb()->exec_SELECTgetRows('*', 'sys_file_reference',
                                                'fieldname = ' . $this->getDb()->fullQuoteStr($key, 'sys_file_reference') .
                                                ' AND tablenames = ' . $this->getDb()->fullQuoteStr($table, 'sys_file_reference') .
                                                ' AND uid_foreign = ' . (int)$fields['uid']);

                                            if ($references) {
                                                foreach ($references as $fileReference) {
                                                    unset($fileReference['uid']);
                                                    $fileReferences[] = $fileReference;
                                                }
                                            }
                                        }
                                    }
                                }

                                unset($overlayFields['t3ver_label']);
                                unset($overlayFields['tx_impexp_origuid']);

                                $overlayFields['pid'] = $fields['uid'];
                                $overlayFields['sys_language_uid'] = $record['create_language'];

                                $this->getDb()->exec_INSERTquery('pages_language_overlay', $overlayFields);
                                $overlayUid = $this->getDb()->sql_insert_id();

                                foreach ($fileReferences as $fileReference) {
                                    $fileReference['uid_foreign'] = $overlayUid;
                                    $fileReference['tablenames'] = 'pages_language_overlay';

                                    $this->getDb()->exec_INSERTquery('sys_file_reference', $fileReference);
                                }
                            }

                            $returnUrl = GeneralUtility::_GP('returnUrl');

                            /** @var ObjectManager $objectManager */
                            $objectManager = GeneralUtility::makeInstance(ObjectManager::class);

                            /** @var UriBuilder $uriBuilder */
                            $uriBuilder = $objectManager->get(UriBuilder::class);

                            if ($_REQUEST['_savedok'] || !$returnUrl) {
                                $columnsOnly = GeneralUtility::_GP('columnsOnly');
                                $columnsOnlyTS = GeneralUtility::_GP('columnsOnlyTS');

                                if ($record['create_language'] && $overlayUid) {
                                    $parameters = [
                                        'edit' => [
                                            'pages_language_overlay' => [
                                                $overlayUid => 'edit',
                                            ],
                                        ],
                                    ];
                                    if ($columnsOnly) {
                                        $parameters['edit']['pages_language_overlay']['columnsOnly'] = $columnsOnly;
                                    }
                                    if ($columnsOnlyTS) {
                                        $parameters['edit']['pages_language_overlay']['columnsOnlyTS'] = $columnsOnlyTS;
                                    }
                                } else {
                                    $parameters = [
                                        'edit' => [
                                            'pages' => [
                                                $record['uid'] => 'edit',
                                            ],
                                        ],
                                    ];
                                    if ($columnsOnly) {
                                        $parameters['edit']['pages']['columnsOnly'] = $columnsOnly;
                                    }
                                    if ($columnsOnlyTS) {
                                        $parameters['edit']['pages']['columnsOnlyTS'] = $columnsOnlyTS;
                                    }
                                }

                                if ($returnUrl) {
                                    $parameters['returnUrl'] = $returnUrl;
                                }

                                $uri = $uriBuilder->buildUriFromRoute('record_edit', $parameters);
                            } elseif ($_REQUEST['_saveandclosedok']) {
                                $uri = $uriBuilder->buildUriFromModule('web_layout', ['id' => $fields['uid'], 'language' => $fields['create_language']]);
                            } elseif ($_REQUEST['_savedokview']) {
                                $uri = null;
                            } else {
                                $uri = $returnUrl;
                            }

                            if ($uri && ($rawId != $record['uid'] || $record['create_language'])) {
                                HttpUtility::redirect((string)$uri);
                            }
                        }
                        break;
                }
            }
        }

        /** @var CacheUtility $cacheUtility */
        $cacheUtility = GeneralUtility::makeInstance(CacheUtility::class);

        if ($dataHandler instanceof \IMIA\ImiaBaseExt\Xclass\Core\DataHandling\DataHandler) {
            foreach ($dataHandler->getDeletedRecords() AS $table => $recordUids) {
                foreach ($recordUids as $uid) {
                    $cacheUtility->clearCache($table, $uid);
                }
            }
        }
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}