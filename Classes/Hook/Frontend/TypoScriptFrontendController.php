<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\Hook\Frontend;

use IMIA\ImiaPageteaser\Controller\TeaserController;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_pageteaser
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class TypoScriptFrontendController
{
    /**
     * @var bool
     */
    static protected $pageRendered = false;

    /**
     * @param array $params
     * @param \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $pObj
     */
    public function configArrayPostProc(&$params, $pObj)
    {
        if (isset($params['config']['absRefPrefix']) && $params['config']['absRefPrefix'] == 'absolute') {
            $params['config']['absRefPrefix'] = GeneralUtility::getIndpEnv('TYPO3_REQUEST_HOST') . '/';
        }
    }

    /**
     * @param array $params
     * @param \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $pObj
     */
    public function contentPostProc(&$params, $pObj)
    {
        self::$pageRendered = true;

        /** @var \TYPO3\CMS\Core\Cache\Frontend\FrontendInterface $runtimeCache */
        $runtimeCache = GeneralUtility::makeInstance(CacheManager::class)->getCache('cache_runtime');
        $cachedCacheLifetimeIdentifier = 'core-tslib_fe-get_cache_timeout';
        $runtimeCache->remove($cachedCacheLifetimeIdentifier);
    }

    /**
     * @param array $params
     * @param \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $pObj
     * @return integer
     */
    public function getCacheTimeout($params, $pObj)
    {
        if (self::$pageRendered) {
            if (TeaserController::getCacheTimeout() != PHP_INT_MAX) {
                $expires = TeaserController::getCacheTimeout() - $GLOBALS['EXEC_TIME'];
                if ($expires > 0 && $params['cacheTimeout'] > $expires) {
                    $params['cacheTimeout'] = $expires;
                }
            }
        }

        return $params['cacheTimeout'];
    }
}