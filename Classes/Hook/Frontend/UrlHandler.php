<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\Hook\Frontend;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;
use IMIA\ImiaPageteaser\Xclass\Frontend\Page\PageRepository;

/**
 * @package     imia_pageteaser
 * @subpackage  Hook
 * @author      David Frerich <d.frerich@imia.de>
 */
class UrlHandler implements \TYPO3\CMS\Frontend\Http\UrlHandlerInterface
{
    /**
     * @inheritdoc
     */
    public function canHandleCurrentUrl()
    {
        if (in_array($this->getTypoScriptFrontendController()->page['doktype'], PageRepository::getExternalDoktypes())) {
            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function handle()
    {
        $externalUrl = $this->getPageRepository()->getExtURL($this->getTypoScriptFrontendController()->page);
        if ($externalUrl && $externalUrl != GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL')) {
            HttpUtility::redirect($externalUrl, HttpUtility::HTTP_STATUS_301);
        }
    }

    /**
     * @return \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController
     */
    protected function getTypoScriptFrontendController()
    {
        return $GLOBALS['TSFE'];
    }

    /**
     * @return PageRepository
     */
    protected function getPageRepository()
    {
        return GeneralUtility::makeInstance(PageRepository::class);
    }
}