<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\Controller;

use IMIA\ImiaBaseExt\Utility\Session\Storage\SessionStorage;
use IMIA\ImiaPageteaser\Domain\Model\Page;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use IMIA\ImiaPageteaser\Domain\Repository\PageRepository;
use TYPO3\CMS\Lang\LanguageService;

/**
 * @package     imia_pageteaser
 * @subpackage  Controller
 * @author      David Frerich <d.frerich@imia.de>
 */
class BackendController extends \IMIA\ImiaBaseExt\Controller\ActionController
{
    /**
     * @var array
     */
    protected $pageData;

    /**
     * @var array
     */
    protected $tsConfig;

    /**
     * @var array
     */
    protected $extConf;

    /**
     * @var array
     */
    protected $moduleConf;

    /**
     * @var integer
     */
    protected $pageId;

    /**
     * @var SessionStorage
     */
    protected $sessionStorage;

    /**
     * @var PageRepository
     */
    protected $pageRepository;

    /**
     * @param SessionStorage $sessionStorage
     */
    public function injectSessionStorage(SessionStorage $sessionStorage)
    {
        $this->sessionStorage = $sessionStorage;
    }

    /**
     * @param PageRepository $pageRepository
     */
    public function injectPageRepository(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    /**
     * @param ViewInterface $view
     */
    public function initializeView(ViewInterface $view)
    {
        parent::initializeView($view);

        $this->controllerContext->getFlashMessageQueue()->getAllMessagesAndFlush();
        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['imia_pageteaser']);

        $view->assign('controller', $this->getRequest()->getControllerName());
        $view->assign('action', $this->getRequest()->getControllerActionName());
        $view->assign('pageData', $this->getPageData());
        $view->assign('extConf', $this->extConf);
        $view->assign('moduleConf', $this->getModuleConfig());

        if ($this->request->hasArgument('pid')) {
            $this->redirectToUri($this->uriBuilder->reset()
                ->setArguments(['id' => (int)$this->request->getArgument('pid')])->setAddQueryString(true)->uriFor());
        }
    }

    public function indexAction()
    {
        if ($this->isInPage()) {
            if ($this->getParam('showHidden') ?: true) {
                $this->pageRepository->setIgnoreEnableFields(true);
                $this->view->assign('showHidden', $this->getParam('showHidden') ?: true);
            }

            $listFields = array_merge(
                ['title'],
                count($this->getModuleConfig('listFields')) > 0
                    ? array_keys($this->getModuleConfig('listFields'))
                    : []
            );

            if (in_array('sorting', $listFields)) {
                $sortingField = 'sorting';
                $sortingDirection = 'asc';
            } else {
                if ($this->getParam('sortingField') && in_array($this->getParam('sortingField'), $listFields) && $this->getParam('sortingField') != 'categories') {
                    $sortingField = $this->getParam('sortingField');
                    $sortingDirection = $this->getParam('sortingDirection') == 'desc' ? 'desc' : 'asc';
                } else {
                    if (in_array('lastUpdated', $listFields)) {
                        $sortingField = 'lastUpdated';
                        $sortingDirection = 'desc';
                    } else {
                        $sortingField = 'title';
                        $sortingDirection = 'asc';
                    }
                }
            }

            $sorting = [
                $sortingField,
                $sortingDirection,
            ];

            $language = $this->getParam('language') !== null ? (int)$this->getParam('language') : -1;
            if (!in_array($language, array_keys($this->getLanguages()))) {
                $language = -1;
            }
            $this->view->assign('language', $language);

            $page = (int)$this->getParam('page') ?: 1;
            if ($page <= 0) {
                $page = 0;
            }

            $perpage = (int)$this->getParam('perpage') ?: 20;
            if ($perpage <= 0) {
                $perpage = 20;
            }

            $offset = $perpage * $page - $perpage;

            if ($this->getParam('recursive') ?: false) {
                $pageUids = $this->getRecursiveSubpages($this->getPageId());
            } else {
                $pageUids = [$this->getPageId()];
            }
            $this->view->assign('recursive', $this->getParam('recursive') ?: false);

            if ($language >= 0) {
                $this->pageRepository->setLanguageUid($language);
            } else {
                $this->pageRepository->setShowAll(true);
            }

            $itemCount = $this->pageRepository->countBySearchAndPidsAndDoktypesAndCategories(
                $this->getParam('search') ?: '',
                $pageUids,
                $this->getModuleConfig('doktypes'),
                null,
                null
            );

            while ($offset > $itemCount) {
                $offset -= $perpage;
            }

            // for sorting over multiple pages
            $modifiedPerpage = $perpage + 1;
            $modifiedOffset = $offset;
            if ($modifiedOffset > 0) {
                $modifiedOffset -= 2;
                $modifiedPerpage += 2;

                if ($modifiedOffset < 0) {
                    $modifiedOffset = 0;
                }
            }

            $items = $this->pageRepository->findBySearchAndPidsAndDoktypesAndCategories(
                $this->getParam('search') ?: '',
                $pageUids,
                $this->getModuleConfig('doktypes'),
                null,
                null,
                $modifiedPerpage,
                $modifiedOffset,
                $sorting
            );

            $prevItem = null;
            $prevPrevItem = null;

            $finalItems = [];

            $i = 0;
            /** @var Page $item */
            foreach ($items as $item) {
                if ($prevItem) {
                    /** @var Page $prevItem */
                    $prevItem->setNext($item->getUid() * -1);
                }

                if ($prevPrevItem) {
                    /** @var Page $prevPrevItem */
                    $item->setPrev($prevPrevItem->getUid() * -1);
                } elseif ($prevItem) {
                    $item->setPrev($item->getPid());
                }

                $prevPrevItem = $prevItem;
                $prevItem = $item;

                $i++;
                if ($modifiedOffset - $offset + $i > 0) {
                    if ($modifiedOffset - $offset + $i <= $perpage) {
                        $finalItems[] = $item;
                    }
                }
            }

            $this->view->assign('search', $this->getParam('search') ?: '');
            $this->view->assign('items', $finalItems);
            $this->view->assign('pages', ceil($itemCount / $perpage));
            $this->view->assign('page', $page);
            $this->view->assign('perpage', $perpage);
            $this->view->assign('sortingField', $sortingField);
            $this->view->assign('sortingDirection', $sortingDirection);
        }
    }

    /**
     * @param integer $id
     * @param boolean $includeSelf
     * @return array
     */
    public function getRecursiveSubpages($id, $includeSelf = true)
    {
        $uids = [];
        if ($includeSelf) {
            $uids[] = (int)$id;
        }

        $res = $this->getDb()->exec_SELECTquery('uid', 'pages', 'pid = ' . $id . ' AND deleted = 0');
        while ($row = $this->getDb()->sql_fetch_assoc($res)) {
            $uids = array_merge($uids, $this->getRecursiveSubpages((int)$row['uid']));
        }

        return array_unique($uids);
    }

    /**
     * @return array
     */
    public function getLanguages()
    {
        $tsConfig = $this->getTSConfig();
        if (isset($tsConfig['mod.']['SHARED.']['defaultLanguageLabel'])) {
            $defaultFlag = $tsConfig['mod.']['SHARED.']['defaultLanguageFlag'];
            $defaultLabel = $tsConfig['mod.']['SHARED.']['defaultLanguageLabel'];
        } else {
            $defaultLabel = $this->translate('backend_language_default');
            $defaultFlag = '';
        }

        $languageOptions = [
            -1 => $this->translate('backend_language_all'),
            0  => $defaultLabel,
        ];
        $languages = [
            -1 => [
                'uid'   => -1,
                'title' => $this->translate('backend_language_all'),
                'flag'  => 'multiple',
            ],
            0  => [
                'uid'   => 0,
                'title' => $defaultLabel,
                'flag'  => $defaultFlag,
            ],
        ];

        $result = $this->getDb()->exec_SELECTquery('uid, title, flag', 'sys_language', '1=1');
        while ($row = $this->getDb()->sql_fetch_assoc($result)) {
            if ($this->getBackendUser()->checkLanguageAccess($row['uid'])) {
                $languageOptions[$row['uid']] = $row['title'];
                $languages[$row['uid']] = $row;
            }
        }

        $this->view->assign('languageOptions', $languageOptions);
        $this->view->assign('languages', $languages);

        return $languages;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getParam($name)
    {
        $param = null;
        if ($this->request->hasArgument($name)) {
            $param = $this->request->getArgument($name);
            $this->sessionStorage->set('param_' . $name, $param);
        } elseif ($this->sessionStorage->has('param_' . $name)) {
            $param = $this->sessionStorage->get('param_' . $name);
        }

        return $param;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getModuleConfig($key = null)
    {
        if (!$this->moduleConf) {
            $pluginNamespace = $this->getPluginNamespace();
            $module = (int)substr($pluginNamespace, strlen($pluginNamespace) - 1);

            $fieldData = $this->extConf['module' . $module . 'Fields'];
            if ($fieldData == '*') {
                $fields = null;
            } else {
                $fields = ['l18n_cfg', 'l18n_cmd', 'create_language'];
                foreach (preg_split('/[, ]+/ism', $fieldData) as $field) {
                    if (trim($field)) {
                        $fields[] = trim($field);
                    }
                }
                $fields = array_unique($fields);
            }

            $tsConfig = BackendUtility::getTCEFORM_TSconfig('pages', $this->getPageData());
            $fieldListData = $this->extConf['module' . $module . 'FieldsList'];

            $fieldsList = [];
            foreach (preg_split('/[, ]+/ism', $fieldListData) as $field) {
                $field = trim($field);
                if ($field) {
                    $label = $GLOBALS['TCA']['pages']['columns'][$field]['label'];
                    $label = $tsConfig[$field]['label'] ?: $label;
                    $label = $tsConfig[$field]['label.'][$this->getLang()->lang] ?: $label;
                    $label = $this->getLang()->sL($label);
                    if (strrpos($label, ':') === strlen($label) - 1) {
                        $label = substr($label, 0, -1);
                    }

                    $fieldsList[$field] = $label;
                }
            }

            $titleFieldLabel = $GLOBALS['TCA']['pages']['columns']['title']['label'];
            $titleFieldLabel = $tsConfig['title']['label'] ?: $titleFieldLabel;
            $titleFieldLabel = $tsConfig['title']['label.'][$this->getLang()->lang] ?: $titleFieldLabel;
            $titleFieldLabel = $this->getLang()->sL($titleFieldLabel);
            if (strrpos($titleFieldLabel, ':') === strlen($titleFieldLabel) - 1) {
                $titleFieldLabel = substr($titleFieldLabel, 0, -1);
            }

            $this->view->assign('titleFieldLabel', $titleFieldLabel);

            $allDoktypes = [];
            foreach ($GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'] as $item) {
                $allDoktypes[(int)$item[1]] = $this->translate($item[0]);
            }

            $basicDoktypes = [];
            $doktypeString = trim($this->extConf['module' . $module . 'Doktypes']);
            foreach ($doktypeString ? array_map('intval', explode(',', $doktypeString)) : [] as $doktype) {
                if (array_key_exists($doktype, $allDoktypes)) {
                    $basicDoktypes[$doktype] = preg_replace('/ \([^\(\)]+\)$/ism', '', $allDoktypes[$doktype]);
                }
            };

            $doktypes = array_keys($basicDoktypes);
            foreach ($doktypes as $doktype) {
                $doktypes[] = $doktype + 3;
                $doktypes[] = $doktype + 4;
                $doktypes[] = $doktype + 6;

                if ($doktype == 110) {
                    $doktypes[] = $doktype + 1;
                    $doktypes[] = $doktype + 2;
                    $doktypes[] = $doktype + 5;
                }
            }

            $this->moduleConf = [
                'title'         => $this->extConf['module' . $module . 'Title'],
                'description'   => $this->extConf['module' . $module . 'Description'],
                'doktypesBasic' => $basicDoktypes,
                'doktypes'      => $doktypes,
                'fields'        => $fields,
                'listFields'    => $fieldsList,
                'pid'           => $this->extConf['module' . $module . 'Pid'] ?: null,
                'pidRootpage'   => $this->extConf['module' . $module . 'PidRootpage'] ? true : false,
            ];
        }

        if ($key) {
            return $this->moduleConf[$key] ?: null;
        } else {
            return $this->moduleConf;
        }
    }

    /**
     * @return array
     */
    public function getTSConfig()
    {
        if (!$this->tsConfig) {
            $this->tsConfig = BackendUtility::getPagesTSconfig($this->getPageId());
        }

        return $this->tsConfig;
    }

    /**
     * @return boolean
     */
    public function isInPage()
    {
        $pageData = $this->getPageData();
        if ($pageData && $pageData['uid']) {
            return true;
        } else {
            $this->showInfo('nopage');

            return false;
        }
    }

    /**
     * @return integer
     */
    public function getPageId()
    {
        if (!$this->pageId) {
            $pids = $this->getModulePageIds();
            $pid = null;
            if (count($pids) == 1) {
                $pid = array_pop($pids);
            }

            if ($pid) {
                $this->pageId = $pid;
            } else {
                if ((int)GeneralUtility::_GP('id')) {
                    $this->pageId = (int)GeneralUtility::_GP('id');
                    $this->sessionStorage->set('pageteaser_backend_pid_' . json_encode($pids), $this->pageId);
                } else {
                    $this->pageId = (int)$this->sessionStorage->get('pageteaser_backend_pid_' . json_encode($pids));
                }
            }

            if (count($pids) > 0) {
                if (!$this->pageId) {
                    $this->redirectToUri($this->uriBuilder->reset()
                        ->setArguments(['id' => $pids[0]])->setAddQueryString(true)->uriFor());
                }

                $pluginNamespace = $this->getPluginNamespace();
                $module = (int)substr($pluginNamespace, strlen($pluginNamespace) - 1);
                $pidRootPage = $this->extConf['module' . $module . 'PidRootpage'] ? true : false;

                $pidSelection = [];
                $result = $this->getDb()->exec_SELECTquery('uid, pid, title, nav_title', 'pages',
                    'uid IN(' . implode(',', $pids) . ') AND deleted = 0');
                while ($row = $this->getDb()->sql_fetch_assoc($result)) {
                    $pidSelection[$row['uid']] = $row['nav_title'] ?: $row['title'];

                    if ($pidRootPage) {
                        $rootPage = $this->getRootPage($row['pid']);
                        if ($rootPage) {
                            $pidSelection[$row['uid']] = $rootPage['nav_title'] ?: $rootPage['title'];
                        }
                    }
                }

                $this->view->assign('pidSelection', $pidSelection);
                $this->view->assign('pid', $this->pageId);
            }
        }

        return $this->pageId;
    }

    /**
     * @param integer $pid
     * @return array|FALSE|NULL
     */
    protected function getRootPage($pid)
    {
        $page = $this->getDb()->exec_SELECTgetSingleRow('title, nav_title, uid, pid, is_siteroot', 'pages', 'uid = ' . (int)$pid);
        if ($page) {
            if (!$page['is_siteroot'] && $page['pid']) {
                $page = $this->getRootPage($page['pid']);
            }
        }

        return $page;
    }

    /**
     * @return array
     */
    public function getModulePageIds()
    {
        $pluginNamespace = $this->getPluginNamespace();
        $module = (int)substr($pluginNamespace, strlen($pluginNamespace) - 1);
        $pid = $this->extConf['module' . $module . 'Pid'];

        $pids = [];
        if ($pid) {
            $pids = array_map('intval', explode(',', $pid));
        }

        return $pids;
    }

    /**
     * @param string $key
     * @return array|null
     */
    public function getPageData($key = null)
    {
        if (!$this->pageData) {
            $this->pageData = BackendUtility::getRecordWSOL('pages', $this->getPageId(), '*');
            if (!in_array((int)$this->pageData['doktype'], [199, 254])) {
                $this->pageData['webView'] = true;
            }
        }

        if ($key && $this->pageData && is_array($this->pageData)) {
            return $this->pageData[$key];
        } else {
            return $this->pageData;
        }
    }

    /**
     * @param string $key
     * @param array $arguments
     */
    public function showInfo($key, $arguments = null)
    {
        $this->showFlashMessage($key, $arguments, AbstractMessage::INFO);
    }

    /**
     * @param string $key
     * @param array $arguments
     */
    public function showSuccess($key, $arguments = null)
    {
        $this->showFlashMessage($key, $arguments, AbstractMessage::OK);
    }

    /**
     * @param string $key
     * @param array $arguments
     */
    public function showWarning($key, $arguments = null)
    {
        $this->showFlashMessage($key, $arguments, AbstractMessage::WARNING);
    }

    /**
     * @param string $key
     * @param array $arguments
     */
    public function showFlashMessage($key, $arguments = null, $type = AbstractMessage::INFO)
    {
        $this->addFlashMessage(
            $this->translate('backend_' . $key, $arguments),
            null,
            $type,
            false
        );
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }

    /**
     * @return LanguageService
     */
    protected function getLang()
    {
        return $GLOBALS['LANG'];
    }

    /**
     * @return \TYPO3\CMS\Core\Authentication\BackendUserAuthentication
     */
    protected function getBackendUser()
    {
        return $GLOBALS['BE_USER'];
    }
}