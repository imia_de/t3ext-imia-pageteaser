<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\Controller;

use IMIA\ImiaBaseExt\Utility\Cache;
use IMIA\ImiaBaseExt\Utility\Helper;
use IMIA\ImiaBaseExt\Utility\Session\Storage\SessionStorage;
use IMIA\ImiaPageteaser\Domain\Model\Page;
use IMIA\ImiaPageteaser\Domain\Repository\CategoryRepository;
use IMIA\ImiaPageteaser\Domain\Repository\PageAltRepository;
use IMIA\ImiaPageteaser\Domain\Repository\PageRepository;
use IMIA\ImiaPageteaser\View\JSONView;
use IMIA\ImiaPageteaser\View\RSSView;
use IMIA\ImiaPageteaser\View\XMLView;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;

/**
 * @package     imia_pageteaser
 * @subpackage  Controller
 * @author      David Frerich <d.frerich@imia.de>
 */
class TeaserController extends \IMIA\ImiaBaseExt\Controller\ActionController
{
    /**
     * @var array
     */
    static protected $displayedRecordUids = [];

    /**
     * @var array
     */
    static protected $lastModified = [];

    /**
     * @var array
     */
    static protected $filterOptions = [];

    /**
     * @var Cache
     */
    static protected $cache;

    /**
     * @var integer
     */
    static protected $cacheTimeout = PHP_INT_MAX;

    /**
     * @var integer
     */
    protected $limit;

    /**
     * @var array
     */
    protected $sessionData;

    /**
     * @var string
     */
    protected $sessionDataKey;

    /**
     * @var array
     */
    protected $doktypes;

    /**
     * @var array
     */
    protected $categories;

    /**
     * @var array
     */
    protected $filter;

    /**
     * @var integer
     */
    protected $pageNum;

    /**
     * @var integer
     */
    protected $pageUid;

    /**
     * @var array
     */
    protected $contentData;

    /**
     * @var boolean
     */
    protected $allowParamOverwrite = false;

    /**
     * @var int
     */
    protected $contentUid;

    /**
     * @var SessionStorage
     */
    protected $sessionStorage;

    /**
     * @var PageRepository
     */
    protected $pageRepository;

    /**
     * @var PageAltRepository
     */
    protected $pageAltRepository;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @var array
     */
    protected $pages;

    /**
     * @var array
     */
    protected $result = [];

    /**
     * @return int
     */
    public static function getCacheTimeout()
    {
        return self::$cacheTimeout;
    }

    /**
     * @param SessionStorage $sessionStorage
     */
    public function injectSessionStorage(SessionStorage $sessionStorage)
    {
        $this->sessionStorage = $sessionStorage;
    }

    /**
     * @param PageRepository $pageRepository
     */
    public function injectPageRepository(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
        $this->pageRepository->setIgnoreEnableFields(true);
        $this->pageRepository->setEnableFieldsToBeIgnored(['starttime', 'endtime']);
    }

    /**
     * @param PageAltRepository $pageAltRepository
     */
    public function injectPageAltRepository(PageAltRepository $pageAltRepository)
    {
        $this->pageAltRepository = $pageAltRepository;
    }

    /**
     * @param CategoryRepository $categoryRepository
     */
    public function injectCategoryRepository(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\InvalidNumberOfConstraintsException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnexpectedTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function indexAction()
    {
        $contentUid = $this->getContentObject()->data['l18n_parent'] ?: $this->getContentObject()->data['uid'];
        if ($this->getSetting('rss.enable') && $this->getContentObject() && $this->getContentObject()->data['uid']) {
            $GLOBALS['TSFE']->additionalHeaderData[] = '<link rel="alternate" type="application/rss+xml" href="' .
                $this->uriBuilder->setCreateAbsoluteUri(true)->setTargetPageUid($GLOBALS['TSFE']->id)->setTargetPageType(255)
                    ->uriFor(null, ['cid' => $contentUid]) . '" />';
        }

        if (count($this->request->getArguments()) == 0) {
            $this->loadArguments($this->request->getPluginName() == 'teaser' ? 'teaser_cached' : 'teaser');
        }

        if (in_array($GLOBALS['TSFE']->type, [253, 254, 255])) {
            switch ($GLOBALS['TSFE']->type) {
                case 255:
                    $this->initializeViewByClass(RSSView::class);
                    $this->resolvedSettings['limit'] = $this->settings['limit'] = $this->getSetting('rss.limit') ?: 20;
                    break;
                case 254:
                    $this->initializeViewByClass(JSONView::class);
                    break;
                case 253:
                    $this->initializeViewByClass(XMLView::class);
                    break;
            }
        } else {
            $this->callHook('index', [&$this]);

            $this->view->assign('indexPage', $this->getPage());
            $this->view->assign('categories', $this->getCategories());
            $this->view->assign('contentObject', $this->getContentData());
            $this->view->assign('contentUid', $contentUid);
            $this->view->assign('sysLanguageUid', $this->getLang());
            $this->view->assign('sorting', $this->getSorting());

            $filter = [];
            $filterOptions = [];
            if (!$this->getSetting('disableFilter')) {
                $filter = $this->getFilter();
                $filterOptions = $this->getFilterOptions();
            }

            $this->view->assign('filter', $filter);
            $this->view->assign('filterOptions', $filterOptions);
        }

        if ($this->getLimit() > 0) {
            $result = $this->getResult();

            if ($result && count($result) > 0) {
                $cacheTimeout = $this->getFirstTimeValueForRecord('pages', $result);
                if ($cacheTimeout != PHP_INT_MAX && $this->getContentObject()->data['uid']) {
                    if (!$GLOBALS['ContentCache-Lifetime']) {
                        $GLOBALS['ContentCache-Lifetime'] = [];
                    }
                    $GLOBALS['ContentCache-Lifetime'][$this->getContentObject()->data['uid']] = $cacheTimeout - $GLOBALS['EXEC_TIME'];
                }
                self::$cacheTimeout = min(self::$cacheTimeout, $cacheTimeout);
            }

            $pages = $this->getPages($result);
            $pagesCount = $this->getPagesCount($result);

            /** @var Page $page */
            foreach ($pages as $page) {
                self::$displayedRecordUids[] = $page->getUid();
            }

            $this->view->assign('pages', $pages);
            $this->view->assign('pagesCount', $pagesCount);

            $this->view->assign('pageNum', $this->getPageNum());
            $this->view->assign('pageCount', ceil($pagesCount / $this->getLimit(false)));
        }
    }

    /**
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\InvalidNumberOfConstraintsException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnexpectedTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function navigationAction()
    {
        $this->allowParamOverwrite = true;
        $this->updateSettings();

        $result = $this->getResult();
        $pageUid = $GLOBALS['TSFE']->id;
        $pageFound = false;
        $prev = null;
        $next = null;

        $backPid = (int)GeneralUtility::_GP('backPid');
        $overview = $backPid ?: $GLOBALS['TSFE']->page['pid'];

        foreach ($result as $uid) {
            if ($pageFound) {
                $next = $uid;
                break;
            } elseif ($uid == $pageUid) {
                $pageFound = true;
            } else {
                $prev = $uid;
            }
        }

        $additionalParams = [];
        if ($backPid) {
            $additionalParams['backPid'] = $backPid;
        }
        if ($this->getContentUid() && $this->getContentUid() != $this->configurationManager->getContentObject()->data['uid']) {
            $additionalParams['contentUid'] = $this->getContentUid();
        }

        $this->pageRepository->setIgnoreEnableFields(false);
        $this->view->assign('prev', $this->pageRepository->findOneByUid((int)$prev));
        $this->view->assign('next', $this->pageRepository->findOneByUid((int)$next));
        $this->view->assign('overview', $this->pageRepository->findOneByUid((int)$overview));
        $this->view->assign('additionalParams', $additionalParams);
        $this->pageRepository->setIgnoreEnableFields(true);
    }

    /**
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\InvalidNumberOfConstraintsException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnexpectedTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function categoriesAction()
    {
        $this->allowParamOverwrite = true;
        $this->updateSettings();

        $filter = [];
        $filterOptions = [];
        $selectedCategories = [];
        if (!$this->getSetting('disableFilter')) {
            $filter = $this->getFilter();
            if (count($filter) == 0) {
                $requestFilter = GeneralUtility::_GP('tx_imiapageteaser_teaser_cached');
                if (is_array($requestFilter) && array_key_exists('filter', $requestFilter)) {
                    $filter = $requestFilter['filter'];
                }
                else{
                    $requestFilter = GeneralUtility::_GP('tx_imiapageteaser_teaser');
                    if (is_array($requestFilter) && array_key_exists('filter', $requestFilter)) {
                        $filter = $requestFilter['filter'];
                    }
                }
            }
            $filterOptions = $this->getFilterOptions();

            if (isset($filter['category'])) {
                $selectedCategories = $this->categoryRepository->findByUids(
                        array_map('intval', explode(',', $filter['category']))
                );
            }
        }

        $this->view->assign('filter', $filter);
        $this->view->assign('filterOptions', $filterOptions);
        $this->view->assign('contentUid', $this->contentUid);

        //backwards compatible
        $this->view->assign('listContentUid', $this->contentUid);

        //add all selected categories form Category-Plugin-Settings
        //reload content-data
        $this->allowParamOverwrite = false;
        $this->contentData = null;
        $this->view->assign('categories', $this->getCategories());
        $this->view->assign('selectedCategories', $selectedCategories);
    }

    /**
     * @param bool $filtered
     * @return mixed
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\InvalidNumberOfConstraintsException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnexpectedTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function getResult($filtered = true)
    {
        $key = $filtered ? 'filtered' : 'default';
        if (!isset($this->result[$key])) {
            $result = null;
            switch ($this->getSetting('displayType')) {
                case 'selectedpages':
                    $result = $this->getSelectedPages($filtered);
                    break;
                case 'allpages':
                    $result = $this->getAllPages($filtered);
                    break;
                case 'teaserpages':
                    $result = $this->getTeaserPages();
                    break;
                case 'selectedteaserpages':
                    $result = $this->getSelectedTeaserPages();
                    break;
                case 'subpages':
                case 'subpagesrecursive':
                case 'selectedsubpages':
                case 'selectedsubpagesrecursive':
                default:
                    $result = $this->getSubPages(
                        $filtered,
                        $this->getPageUidsForSubPages($this->getSetting('displayType'))
                    );
                    break;
            }

            if (!$result) {
                $result = [];
            }

            $this->result[$key] = $result;
        }

        return $this->result[$key];
    }

    /**
     * @param string $tableName
     * @param array $uids
     * @return int
     */
    protected function getFirstTimeValueForRecord($tableName, $uids)
    {
        $now = $GLOBALS['ACCESS_TIME'];
        $result = PHP_INT_MAX;

        $enableFields = $this->getContentObject()->enableFields(
            $tableName,
            false,
            ['starttime' => true, 'endtime' => true]
        );

        $timeFields = [];
        $selectFields = [];
        $whereConditions = [];
        foreach (['starttime', 'endtime'] as $field) {
            if (isset($GLOBALS['TCA'][$tableName]['ctrl']['enablecolumns'][$field])) {
                $timeFields[$field] = $GLOBALS['TCA'][$tableName]['ctrl']['enablecolumns'][$field];
                $selectFields[$field]
                    = 'MIN('
                    . 'CASE WHEN ' . $timeFields[$field] . ' <= ' . $now
                    . ' THEN NULL ELSE ' . $timeFields[$field] . ' END'
                    . ') AS ' . $field;
                $whereConditions[$field] = $timeFields[$field] . '>' . $now;
            }
        }

        // if starttime or endtime are defined, evaluate them
        if (!empty($timeFields)) {
            // find the timestamp, when the current page's content changes the next time
            $row = $this->getDb()->exec_SELECTgetSingleRow(
                implode(', ', $selectFields),
                $tableName,
                'uid IN (' . implode(',', $uids) . ')'
                . ' AND (' . implode(' OR ', $whereConditions) . ')'
                . $enableFields
            );
            if ($row) {
                foreach ($timeFields as $timeField => $_) {
                    // if a MIN value is found, take it into account for the
                    // cache lifetime we have to filter out start/endtimes < $now,
                    // as the SQL query also returns rows with starttime < $now
                    // and endtime > $now (and using a starttime from the past
                    // would be wrong)
                    if ($row[$timeField] !== null && (int)$row[$timeField] > $now) {
                        $result = min($result, (int)$row[$timeField]);
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param mixed $contents
     * @return string
     */
    protected function getHash($contents)
    {
        if (is_array($contents)) {
            foreach ($contents as &$value) {
                if (is_array($value)) {
                    $value = $this->getHash($value);
                } elseif (is_object($value)) {
                    $value = spl_object_hash($value);
                }
            }
            ksort($contents);
        }

        return md5(json_encode($contents));
    }

    /**
     * @param array $result
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function getPages($result)
    {
        if (!$this->pages) {
            $excludeUids = [];
            if ($this->getSetting('excludeUids')) {
                $excludeUids = array_map('intval', explode(',', $this->getSetting('excludeUids')));
            }
            if ($this->getSetting('duplicate')) {
                $excludeUids = array_unique(array_merge($excludeUids, self::$displayedRecordUids));
            }

            $this->pageRepository->setIgnoreEnableFields(false);
            $pages = $this->pageRepository->findByUids(
                $result,
                $this->getSetting('date'),
                $excludeUids,
                $this->getLimit(),
                $this->getOffset(),
                [$this->getSorting(), $this->getSetting('sortingDirection')]
            );

            if ($this->getSetting('displayType') == 'selectedpages' && $this->getSetting('sorting') == 'selected') {
                $pageOrder = [];
                $pageCount = 0;
                foreach (explode(',', $this->getSetting('pages')) as $pageUid) {
                    $pageOrder[(int)$pageUid] = ++$pageCount;
                }

                $orderedPages = [];
                foreach ($pages as $page) {
                    $orderedPages[$pageOrder[$page->getUid()]] = $page;
                }

                ksort($orderedPages);
                $pages = $orderedPages;
            }

            $storagePids = $this->getPageUidsForSubPages($this->getSetting('displayType'), false);
            $mountPages = [];
            foreach ($pages as $page) {
                if (!in_array($page->getPid(), $storagePids)) {
                    $mountPages[] = $page;
                }
            }

            if (count($mountPages) > 0) {
                $resolvedMounts = array_flip($this->pageRepository->getResolvedPageUids($storagePids));
                /** @var Page $page */
                foreach ($mountPages as $page) {
                    if (array_key_exists($page->getPage()->getPid(), $resolvedMounts)) {
                        $page->setPid($resolvedMounts[$page->getPage()->getPid()]);
                    }
                }
            }
            $this->pageRepository->setIgnoreEnableFields(true);
            $this->pages = $pages;
        }

        return $this->pages;
    }

    /**
     * @return mixed|string
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    public function getSorting()
    {
        $sorting = $this->getSetting('sorting');
        if ($sorting == 'selected') {
            $sorting = 'sorting';
        }

        if ($this->request->hasArgument('sorting')) {
            $availableSortings = [];
            foreach ($GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_imiapageteaser.']['flexforms.']['sortings.'] as $sortConf) {
                $availableSortings[] = $sortConf['value'];
            }
            if (in_array($this->request->getArgument('sorting'), $availableSortings)) {
                $sorting = $this->request->getArgument('sorting');
            }
        }

        return $sorting;
    }

    /**
     * @param array $result
     * @return integer
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function getPagesCount($result)
    {
        $excludeUids = [];
        if ($this->getSetting('excludeUids')) {
            $excludeUids = array_map('intval', explode(',', $this->getSetting('excludeUids')));
        }

        $this->pageRepository->setIgnoreEnableFields(false);
        $pagesCount = $this->pageRepository->countByUids(
            $result,
            $this->getSetting('date'),
            $excludeUids
        );
        $this->pageRepository->setIgnoreEnableFields(true);

        return ($pagesCount - (int)$this->getSetting('offset'));
    }

    /**
     * @param string $orderDirection
     * @return Page
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\InvalidNumberOfConstraintsException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnexpectedTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function getFirstPage($orderDirection = 'asc')
    {
        $cacheKey = 'firstlast_' . $this->getContentKey() . '_' .
            (isset($GLOBALS['TSFE']->fe_user->user['usergroup']) ? $GLOBALS['TSFE']->fe_user->user['usergroup'] : '') . '_' .
            $GLOBALS['TSFE']->sys_language_uid . '_' . $orderDirection;

        if (!$this->getCache()->hasCache($cacheKey) || ($this->getCache()->hasCache($cacheKey)
                && $this->getCache()->getModified($cacheKey) < $this->getLastModified('page'))
        ) {
            switch ($this->getSetting('displayType')) {
                case 'selectedpages':
                    $result = $this->getSelectedPages(false, true, $orderDirection);
                    break;
                case 'allpages':
                    $result = $this->getAllPages(false, true, $orderDirection);
                    break;
                case 'teaserpages':
                    $result = $this->getTeaserPages(true, $orderDirection);
                    break;
                case 'selectedteaserpages':
                    $result = $this->getSelectedTeaserPages(true, $orderDirection);
                    break;
                case 'subpages':
                case 'subpagesrecursive':
                case 'selectedsubpages':
                case 'selectedsubpagesrecursive':
                default:
                    $result = $this->getSubPages(
                        false,
                        $this->getPageUidsForSubPages($this->getSetting('displayType')),
                        true,
                        $orderDirection
                    );
                    break;
            }

            $page = [];
            if ($result) {
                foreach ($result->toArray() as $key => $val) {
                    if (!is_array($val) && !is_object($val) && $val) {
                        $page[$key] = $val;
                    }
                }
            }

            $this->getCache()->writeCache($cacheKey, $page);
        } else {
            $page = $this->getCache()->loadCache($cacheKey);
        }

        return $page;
    }

    /**
     * @return Page
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\InvalidNumberOfConstraintsException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnexpectedTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function getLastPage()
    {
        return $this->getFirstPage('desc');
    }

    /**
     * @return array
     */
    public function getContentData()
    {
        $this->getContentDataByParam();
        $this->getContentDataByConfiguration();
        $this->getContentDataByManager();

        return $this->contentData;
    }

    protected function getContentDataByParam()
    {
        if (!$this->contentData && $this->allowParamOverwrite) {
            $contentUid = GeneralUtility::_GP('contentUid');
            if (isset($contentUid)) {
                $this->contentUid = (int)$contentUid;
                $this->contentData = $this->loadContentData();
            }
        }
    }

    protected function getContentDataByConfiguration()
    {
        if (!$this->contentData && $this->allowParamOverwrite) {

            $contentUid = $this->getSetting('contentUid');
            if (!isset($contentUid)) {
                //fallback for old settings name
                $this->contentUid = $this->getSetting('listContentUid');
            }
            if (isset($contentUid)) {
                $this->contentUid = (int)$contentUid;
                $this->contentData = $this->loadContentData();
            }
        }
    }

    /**
     * @return array
     */
    protected function loadContentData()
    {
        return array_shift($this->getDb()->exec_SELECTgetRows(
                    '*',
                    'tt_content',
                    'uid = ' . $this->contentUid));
    }

    protected function getContentDataByManager()
    {
        if (!$this->contentData) {
            $this->contentData = $this->configurationManager->getContentObject()->data;
        }
    }

    /**
     * @param boolean $onlyContent
     * @return integer
     */
    public function getContentUid($onlyContent = false)
    {
        $contentData = $this->getContentData();
        $contentUid = $contentData['uid'];
        if ($onlyContent && $contentData['uid'] == $GLOBALS['TSFE']->id && array_key_exists('TSconfig', $contentData)) {
            $contentUid = null;
        }

        return $contentUid;
    }

    /**
     * @return mixed
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function getPage()
    {
        return $this->pageRepository->findOneByUid($this->getPageUid());
    }

    /**
     * @return integer
     */
    public function getPageUid()
    {
        if (!$this->pageUid) {
            if ($this->allowParamOverwrite && GeneralUtility::_GP('backPid')) {
                $this->pageUid = (int)GeneralUtility::_GP('backPid');
            } else {
                $this->pageUid = $GLOBALS['TSFE']->id;
            }
        }

        return $this->pageUid;
    }

    /**
     * @return mixed
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\InvalidNumberOfConstraintsException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnexpectedTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function getFilterOptions()
    {
        $key = md5(json_encode($this->getSettings()));
        if (!isset(self::$filterOptions[$key])) {
            $firstPage = $this->getFirstPage();
            $lastPage = $this->getLastPage();

            $filterOptions = [
                'month'      => [0 => $this->translate('filter_choose')],
                'year'       => [0 => $this->translate('filter_choose')],
                'month-year' => [0 => $this->translate('filter_choose')],
                'categories' => [0 => $this->translate('filter_choose')],
            ];

            if ($firstPage) {
                for ($month = 1; $month <= 12; $month++) {
                    $filterOptions['month'][$month] = $this->translate('filter_month_' . $month);
                }

                if ($firstPage['lastUpdated']) {
                    $startMonth = (int)date('m', $firstPage['lastUpdated']);
                    $endMonth = 12;

                    if ($this->getSetting('date') == 'past') {
                        for ($year = (int)date('Y', $firstPage['lastUpdated']); $year <= date('Y'); $year++) {
                            $filterOptions['year'][$year] = $year;

                            if ($year == (int)date('Y')) {
                                $endMonth = (int)date('m');
                            }
                            for ($month = $startMonth; $month <= $endMonth; $month++) {
                                $filterOptions['month-year'][$month . '-' . $year] = $this->translate('filter_month_' . $month) . ' ' . $year;
                            }
                            $startMonth = 1;
                        }
                    } elseif ($lastPage) {
                        for ($year = (int)date('Y', $firstPage['lastUpdated']); $year <= (int)date('Y', $lastPage['lastUpdated']); $year++) {
                            $filterOptions['year'][$year] = $year;

                            if ($year == (int)date('Y', $lastPage['lastUpdated'])) {
                                $endMonth = (int)date('m', $lastPage['lastUpdated']);
                            }
                            for ($month = $startMonth; $month <= $endMonth; $month++) {
                                $filterOptions['month-year'][$month . '-' . $year] = $this->translate('filter_month_' . $month) . ' ' . $year;
                            }
                            $startMonth = 1;
                        }
                    } else {
                        for ($year = (int)date('Y', $firstPage['lastUpdated']); $year <= date('Y'); $year++) {
                            $filterOptions['year'][$year] = $year;

                            if ($year == (int)date('Y')) {
                                $endMonth = (int)date('m');
                            }
                            for ($month = $startMonth; $month <= $endMonth; $month++) {
                                $filterOptions['month-year'][$month . '-' . $year] = $this->translate('filter_month_' . $month) . ' ' . $year;
                            }
                            $startMonth = 1;
                        }
                    }
                }
            }

            $pageUids = array_map('intval', $this->getResult(false));
            if (count($pageUids) > 0) {
                $result = $this->getDb()->exec_SELECTquery(
                    'DISTINCT uid_local AS uid',
                    'sys_category_record_mm',
                    'uid_foreign IN(' . implode(',', $pageUids) . ') AND tablenames = "pages" AND fieldname="categories"'
                );
                $categoryUids = [];
                while ($row = $this->getDb()->sql_fetch_assoc($result)) {
                    $categoryUids[] = (int)$row['uid'];
                }
                $filterOptions['categories'] = $this->categoryRepository->findByUids($categoryUids);
            }

            $this->callHook('getFilterOptions', [&$filterOptions, $pageUids, $this]);

            self::$filterOptions[$key] = $filterOptions;
        }

        return self::$filterOptions[$key];
    }

    /**
     * @param string $orderDirection
     * @return string
     */
    public function getRealOrderDirection($orderDirection)
    {
        if (strtolower($this->getSetting('sortingDirection')) == 'asc') {
            if (strtolower($orderDirection) == 'asc') {
                $realOrderDirection = 'asc';
            } else {
                $realOrderDirection = 'desc';
            }
        } else {
            if (strtolower($orderDirection) == 'asc') {
                $realOrderDirection = 'desc';
            } else {
                $realOrderDirection = 'asc';
            }
        }

        return $realOrderDirection;
    }

    /**
     * @param string $type
     * @return object
     */
    public function getLastModified($type)
    {
        if (!array_key_exists($type, self::$lastModified)) {
            switch ($type) {
                case 'pageAlt':
                    self::$lastModified[$type] = $this->pageAltRepository->findLastModified();
                    break;
                case 'page':
                default:
                    self::$lastModified[$type] = $this->pageRepository->findLastModified();
                    break;
            }
        }

        return self::$lastModified[$type];
    }

    /**
     * @return array
     */
    public function getDoktypes()
    {
        if (!$this->doktypes) {
            $this->doktypes = [];
            foreach (explode(',', $this->getSetting('doktypes')) as $doktype) {
                $this->doktypes[] = (int)$doktype;

                switch ((int)$doktype) {
                    case 1:
                        $this->doktypes[] = 2;
                        $this->doktypes[] = 3;
                        $this->doktypes[] = 4;
                        break;
                    default:
                        for ($i = 1; $i < 10; $i++) {
                            $this->doktypes[] = (int)$doktype + $i;
                        }
                        break;
                }

                $this->callHook('modifyFrontendTypes', [$doktype, &$this->doktypes, &$this]);
            }
        }

        return $this->doktypes;
    }

    /**
     * @return array|mixed
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    public function getFilter()
    {
        if (!$this->filter) {
            $this->filter = [];

            $sessionData = null;
            if (!$this->getSetting('disablePersistentFilter')) {
                $sessionData = $this->getSessionData();

                if (array_key_exists('filter', $sessionData) && $sessionData['filter']) {
                    $this->filter = $sessionData['filter'];
                }
            }

            if ($this->request->hasArgument('filter') && $this->request->getArgument('filter')) {
                $this->filter = (array)$this->request->getArgument('filter');
            }

            if (!$this->getSetting('disablePersistentFilter')) {
                $sessionData['filter'] = $this->filter;
                $this->setSessionData($sessionData);
            }

            $newFilter = $this->getSetting('filter');
            if ($newFilter && is_array($newFilter)) {
                ArrayUtility::mergeRecursiveWithOverrule($newFilter, $this->filter);
                $this->filter = $newFilter;
            }

            // Hook begin
            foreach ($this->getHookObjects() as $hook) {
                if (method_exists($hook, 'getFilter')) {
                    $hook->getFilter($this->filter, $this->getSettings(), $this);
                }
            }
            // Hook end
        }

        return $this->filter;
    }

    /**
     * @return int
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    public function getPageNum()
    {
        if (!$this->pageNum) {
            $this->pageNum = 1;
            if ($this->request->hasArgument('page') && $this->request->getArgument('page')) {
                $this->pageNum = (int)$this->request->getArgument('page');
            }
        }

        return $this->pageNum;
    }

    /**
     * @param bool $respectModification
     * @return int
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    public function getLimit($respectModification = true)
    {
        if (!$this->limit) {
            $this->limit = (int)$this->getSetting('limit');

            foreach ($this->getHookObjects() as $hook) {
                if (method_exists($hook, 'getLimit')) {
                    $hook->getLimit($this->limit, $this->getFilter(), $this);
                }
            }
        }

        if ($respectModification) {
            $showPages = false;
            if ($this->request->hasArgument('showPages') && $this->request->getArgument('showPages')) {
                $showPages = (int)$this->request->getArgument('showPages');
            } elseif (GeneralUtility::_GP('showPages')) {
                $showPages = (int)GeneralUtility::_GP('showPages');
            }

            if ($showPages) {
                return $this->limit * $showPages;
            } else {
                return $this->limit;
            }
        } else {
            return $this->limit;
        }
    }

    /**
     * @return float|int
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    public function getOffset()
    {
        $offset = (int)$this->getSetting('offset');

        return $this->getPageNum() * $this->getLimit() - $this->getLimit() + $offset;
    }

    /**
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\InvalidNumberOfConstraintsException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnexpectedTypeException
     */
    public function getCategories()
    {
        if (!$this->categories) {
            $contentUid = $this->getContentUid(true);
            if (!$contentUid) {
                if ($this->getSetting('categories')) {
                    $this->categories = $this->categoryRepository->findByUids(
                        array_map('intval', explode(',', $this->getSetting('categories')))
                    );
                } else {
                    $this->categories = [];
                }
            } else {
                $this->categories = Helper::getRecordCategories(
                    $contentUid,
                    'tt_content',
                    'categories',
                    $this->categoryRepository
                );
            }
        }

        return $this->categories;
    }

    /**
     * @return array
     */
    public function getSessionData()
    {
        if (!$this->sessionData) {
            $this->sessionData = $this->sessionStorage->get($this->getSessionDataKey());

            if (!$this->sessionData) {
                $this->sessionData = [];
            }
        }

        return $this->sessionData;
    }

    /**
     * @param array $sessionData
     */
    public function setSessionData($sessionData)
    {
        $this->sessionData = $sessionData;
        $this->sessionStorage->set($this->getSessionDataKey(), $this->sessionData);
    }

    /**
     * @return integer
     */
    public function getLang()
    {
        return $GLOBALS['TSFE']->sys_language_uid;
    }

    /**
     * @return Cache
     */
    public function getCache()
    {
        if (!self::$cache) {
            self::$cache = new Cache(PATH_site . 'typo3temp/var/Cache/Code/cache_core/imia_pageteaser/');
        }

        return self::$cache;
    }

    /**
     * @param bool $filtered
     * @param bool $getFirst
     * @param string $orderDirection
     * @return array|mixed
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\InvalidNumberOfConstraintsException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnexpectedTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    protected function getSelectedPages($filtered = true, $getFirst = false, $orderDirection = null)
    {
        if (!$getFirst) {
            $result = $this->pageRepository->findUidsByUidsAndDoktypesAndCategories(
                array_map('intval', explode(',', $this->getSetting('pages'))),
                $this->getDoktypes(),
                $this->getCategories(),
                $filtered ? $this->getFilter() : null,
                [$this->getSorting(), $this->getRealOrderDirection($orderDirection)]
            );
        } else {
            $result = array_shift($this->pageRepository->findByUidsAndDoktypesAndCategories(
                array_map('intval', explode(',', $this->getSetting('pages'))),
                $this->getDoktypes(),
                $this->getCategories(),
                null,
                1,
                0,
                ['lastUpdated', $orderDirection]
            ));
        }

        return $result;
    }

    /**
     * @param boolean $filtered
     * @param boolean $getFirst
     * @param string $orderDirection
     * @return array|mixed
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\InvalidNumberOfConstraintsException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnexpectedTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    protected function getAllPages($filtered = true, $getFirst = false, $orderDirection = 'asc')
    {
        if (!$getFirst) {
            $result = $this->pageRepository->findUidsByDoktypesAndCategories(
                $this->getDoktypes(),
                $this->getCategories(),
                $filtered ? $this->getFilter() : null,
                [$this->getSorting(), $this->getRealOrderDirection($orderDirection)]
            );
        } else {
            $result = array_shift($this->pageRepository->findByDoktypesAndCategories(
                $this->getDoktypes(),
                $this->getCategories(),
                null,
                1,
                0,
                ['lastUpdated', $orderDirection]
            ));
        }

        return $result;
    }

    /**
     * @param bool $getFirst
     * @param string $orderDirection
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    protected function getTeaserPages($getFirst = false, $orderDirection = 'asc')
    {
        if (!$getFirst) {
            $result = $this->pageRepository->findTeaserPagesByUids(
                [$this->getPageUid()]
            );
        } else {
            $pageUids = $this->pageRepository->findTeaserPagesByUids(
                array_map('intval', explode(',', $this->getSetting('pages')))
            );

            if (strtolower($orderDirection) == 'desc') {
                $pageUid = array_pop($pageUids);
            } else {
                $pageUid = array_shift($pageUids);
            }

            $result = $this->pageRepository->findOneByUid($pageUid);
        }

        return $result;
    }

    /**
     * @param bool $getFirst
     * @param string $orderDirection
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    protected function getSelectedTeaserPages($getFirst = false, $orderDirection = null)
    {
        if (!$getFirst) {
            $result = $this->pageRepository->findTeaserPagesByUids(
                array_map('intval', explode(',', $this->getSetting('pages')))
            );
        } else {
            $pageUids = $this->pageRepository->findTeaserPagesByUids(
                array_map('intval', explode(',', $this->getSetting('pages')))
            );

            if (strtolower($orderDirection) == 'desc') {
                $pageUid = array_pop($pageUids);
            } else {
                $pageUid = array_shift($pageUids);
            }

            $result = $this->pageRepository->findOneByUid($pageUid);
        }

        return $result;
    }

    /**
     * @param bool $filtered
     * @param array $pageUids
     * @param bool $getFirst
     * @param string $orderDirection
     * @return array|mixed
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\InvalidNumberOfConstraintsException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnexpectedTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    protected function getSubPages($filtered = true, $pageUids, $getFirst = false, $orderDirection = null)
    {
        if (!$getFirst) {
            $result = $this->pageRepository->findUidsByPidsAndDoktypesAndCategories(
                $pageUids,
                $this->getDoktypes(),
                $this->getCategories(),
                $filtered ? $this->getFilter() : null,
                [$this->getSorting(), $this->getRealOrderDirection($orderDirection)]
            );
        } else {
            $result = array_shift($this->pageRepository->findByPidsAndDoktypesAndCategories(
                $pageUids,
                $this->getDoktypes(),
                $this->getCategories(),
                null,
                1,
                0,
                ['lastUpdated', strtolower($orderDirection)]
            ));
        }

        return $result;
    }

    /**
     * @param string $displayType
     * @param boolean $resolveMounts
     * @return array
     */
    protected function getPageUidsForSubPages($displayType, $resolveMounts = true)
    {
        $cacheKey = 'storage-pids_' . $this->getContentKey() . '_' . ($resolveMounts ? '_m' : '_nm') . '_' .
            (isset($GLOBALS['TSFE']->fe_user->user['usergroup']) ? $GLOBALS['TSFE']->fe_user->user['usergroup'] : '');

        if (!$this->getCache()->hasCache($cacheKey) || ($this->getCache()->hasCache($cacheKey)
                && $this->getCache()->getModified($cacheKey) < $this->getLastModified('page'))
        ) {
            $recursive = false;
            switch ($displayType) {
                case 'selectedsubpagesrecursive':
                    $recursive = true;
                case 'selectedsubpages':
                    $pageUids = array_map('intval', explode(',', $this->getSetting('pages')));
                    break;
                case 'subpagesrecursive':
                    $recursive = true;
                case 'subpages':
                default:
                    $pageUids = [$this->getPageUid()];
                    break;
            }

            if ($recursive) {
                $allPageUids = [];
                foreach ($pageUids as $pageUid) {
                    $allPageUids = array_merge($allPageUids, $this->getSubPageUidsForPageUidRecursive($pageUid, $resolveMounts));
                }

                $pageUids = array_unique($allPageUids);
                $this->getCache()->writeCache($cacheKey, $pageUids);
            }

            if (count($pageUids) == 0) {
                $pageUids[] = 0;
            }
        } else {
            $pageUids = $this->getCache()->loadCache($cacheKey);
        }

        if (!is_array($pageUids)) {
            $pageUids = [];
        }

        return $pageUids;
    }

    /**
     * @param integer $pageUid
     * @param boolean $resolveMounts
     * @return array
     */
    protected function getSubPageUidsForPageUidRecursive($pageUid, $resolveMounts = true)
    {
        $pageUids = [];
        $page = $this->getDb()->exec_SELECTgetSingleRow('uid, doktype, mount_pid', 'pages',
            'uid = ' . (int)$pageUid . $GLOBALS['TSFE']->sys_page->enableFields('pages'));

        if ($page) {
            if ($page['doktype'] == 7 && $page['mount_pid'] && $resolveMounts) {
                $pageUid = (int)$page['mount_pid'];
            }

            // recursive Query with DB-object instead of extbase for better performance
            $res = $this->getDb()->exec_SELECTquery('uid', 'pages', 'pid = ' . (int)$pageUid . $GLOBALS['TSFE']->sys_page->enableFields('pages'));
            while ($row = $this->getDb()->sql_fetch_assoc($res)) {
                $pageUids = array_merge($pageUids, $this->getSubPageUidsForPageUidRecursive((int)$row['uid'], $resolveMounts));
            }

            $pageUids[] = $pageUid;
        }

        return $pageUids;
    }

    /**
     * @return string
     */
    protected function getSessionDataKey()
    {
        if (!$this->sessionDataKey) {
            $this->sessionDataKey = 'pt_' . $this->getPageUid() . '_' . $this->getContentKey();

            foreach ($this->getHookObjects() as $hook) {
                if (method_exists($hook, 'getSessionDataKey')) {
                    $hook->getSessionDataKey($this->sessionDataKey, $this->getPageUid(), $this->getContentData(), $this->getSettings(), $this);
                }
            }
        }

        return $this->sessionDataKey;
    }

    /**
     * @return string
     */
    protected function getContentKey()
    {
        return $this->getContentUid(true) ?: md5(serialize($this->getContentData()) . serialize($this->getSettings()));
    }

    /**
     * Overwrite settings, if alternative contentUid is defined
     */
    protected function updateSettings()
    {
        $contentData = $this->getContentData();
        if ($contentData['uid'] != $this->configurationManager->getContentObject()->data['uid']) {
            $flexFormService = $this->objectManager->get('TYPO3\CMS\Extbase\Service\FlexFormService');
            $flexFormData = $flexFormService->convertFlexFormContentToArray($contentData['pi_flexform']);
            if ($flexFormData && array_key_exists('settings', $flexFormData) && is_array($flexFormData['settings'])) {
                foreach ($flexFormData['settings'] as $key => $val) {
                    if ($key == 'categories') {
                        continue; //skip categories
                    }
                    $this->settings[$key] = $val;
                }
                //force new resolving --> prevent loading old cached settings
                $this->resolvedSettings = null;
            }
        }
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }

    /**
     * @param ViewInterface $view
     */
    protected function initializeView(ViewInterface $view)
    {
        parent::initializeView($view);
        $this->pageRepository->setLogicalCategoriesOperator($this->getSetting('categoriesLogicalOperator'));
    }
}