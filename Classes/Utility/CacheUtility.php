<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\Utility;

use IMIA\ImiaBaseExt\Traits\HookTrait;
use IMIA\ImiaBaseExt\Utility\Helper;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_usu
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class CacheUtility implements \TYPO3\CMS\Core\SingletonInterface
{
    use HookTrait;

    /**
     * @var array
     */
    protected static $cleanedPages = [];

    /**
     * @var array
     */
    protected static $cleanedContents = [];

    /**
     * @var \TYPO3\CMS\Core\Cache\Frontend\FrontendInterface|boolean
     */
    static protected $cache = null;

    /**
     * @var DataHandler
     */
    protected $dataHandler;

    /**
     * @param string $table
     * @param integer $id
     */
    public function clearCache($table, $id)
    {
        $allowedTables = ['pages', 'pages_language_overlay', 'sys_category'];
        $this->callHook('allowedTables', [&$allowedTables, $this]);

        if (in_array($table, $allowedTables)) {
            $record = BackendUtility::getRecord($table, $id, '*', '', false);

            if ($record) {
                $pageUids = [];
                $contentUids = [];

                if ($table == 'pages') {
                    $pageUids[] = (int)$id;
                } else {
                    $pageUids[] = (int)$record['pid'];
                }

                switch ($table) {
                    case 'pages':
                    case 'pages_language_overlay':
                        $doktypes = [1, 2, 3, 4];
                        foreach ([100, 110, 120, 130, 140, 150, 160, 170, 180, 190] as $doktype) {
                            $doktypes[] = $doktype;
                            $doktypes[] = $doktype + 1;
                            $doktypes[] = $doktype + 2;
                            $doktypes[] = $doktype + 3;
                            $doktypes[] = $doktype + 4;
                            $doktypes[] = $doktype + 5;
                            $doktypes[] = $doktype + 6;
                            $doktypes[] = $doktype + 7;
                            $doktypes[] = $doktype + 8;
                            $doktypes[] = $doktype + 9;
                        }

                        if (in_array((int)$record['doktype'], $doktypes)) {
                            $result = $this->getDb()->exec_SELECTquery('uid, pid, pi_flexform', 'tt_content',
                                'CType = "list" AND list_type IN("imiapageteaser_teaser_cached", "imiapageteaser_teaser_categories")' .
                                BackendUtility::deleteClause('tt_content')
                            );

                            $pageCategories = Helper::getRecordCategories(
                                $table == 'pages' ? (int)$record['uid'] : (int)$record['pid'],
                                'pages',
                                'categories'
                            );

                            while ($row = $this->getDb()->sql_fetch_assoc($result)) {
                                if ($row['pi_flexform']) {
                                    $contentCategories = Helper::getRecordCategories(
                                        $row['uid'],
                                        'tt_content',
                                        'categories'
                                    );

                                    $categoryFound = false;
                                    if (is_array($pageCategories) && is_array($contentCategories)) {
                                        foreach ($pageCategories as $pageCategory) {
                                            if (in_array($pageCategory, $contentCategories)) {
                                                $categoryFound = true;
                                                break;
                                            }
                                        }
                                    } else {
                                        $categoryFound = true;
                                    }
                                    
                                    if ($categoryFound) {
                                        $flexForm = GeneralUtility::xml2array($row['pi_flexform']);

                                        if (isset($flexForm['data']['sDEF']['lDEF'])) {
                                            $pluginSettings = $flexForm['data']['sDEF']['lDEF'];

                                            $doktypeSetting = $pluginSettings['settings.doktypes']['vDEF'];
                                            if ($doktypeSetting) {
                                                if (!is_array($doktypeSetting)) {
                                                    $doktypeSetting = explode(',', $doktypeSetting);
                                                }
                                                $doktypeSetting = array_map('intval', $doktypeSetting);
                                            }

                                            foreach ($doktypeSetting as $doktype) {
                                                if ($doktype >= 100) {
                                                    $doktypeSetting[] = $doktype + 1;
                                                    $doktypeSetting[] = $doktype + 2;
                                                    $doktypeSetting[] = $doktype + 3;
                                                    $doktypeSetting[] = $doktype + 4;
                                                    $doktypeSetting[] = $doktype + 5;
                                                    $doktypeSetting[] = $doktype + 6;
                                                    $doktypeSetting[] = $doktype + 7;
                                                    $doktypeSetting[] = $doktype + 8;
                                                    $doktypeSetting[] = $doktype + 9;
                                                }
                                            }

                                            if (!$doktypeSetting || in_array((int)$record['doktype'], $doktypeSetting)) {
                                                $pid = (int)$row['pid'];
                                                $uid = (int)$row['uid'];
                                                $pageUids[$pid] = $pid;
                                                $contentUids[$uid] = $uid;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (ExtensionManagementUtility::isLoaded('imia_base')) {
                            if (\IMIA\ImiaBase\Utility\ContentCache::enabled()) {
                                \IMIA\ImiaBase\Utility\ContentCache::flushByTag('pagecontent-' . $record['uid']);
                            }
                        }
                        break;
                    case 'sys_category':
                        $doktypes = [1, 2, 3, 4];
                        foreach ([100, 110, 120, 130, 140, 150, 160, 170, 180, 190] as $doktype) {
                            $doktypes[] = $doktype;
                            $doktypes[] = $doktype + 1;
                            $doktypes[] = $doktype + 2;
                            $doktypes[] = $doktype + 3;
                            $doktypes[] = $doktype + 4;
                            $doktypes[] = $doktype + 5;
                            $doktypes[] = $doktype + 6;
                            $doktypes[] = $doktype + 7;
                            $doktypes[] = $doktype + 8;
                            $doktypes[] = $doktype + 9;
                        }

                        if (!$record['doktype'] || in_array((int)$record['doktype'], $doktypes)) {
                            $result = $this->getDb()->exec_SELECTquery('uid, pid', 'tt_content',
                                'CType = "list" AND list_type IN("imiapageteaser_teaser_cached", "imiapageteaser_teaser_categories")' .
                                BackendUtility::deleteClause('tt_content')
                            );
                            while ($row = $this->getDb()->sql_fetch_assoc($result)) {
                                $pageUids[] = (int)$row['pid'];
                                $contentUids[] = (int)$row['uid'];
                            }
                        }
                        break;
                }

                $this->callHook('findUids', [&$pageUids, &$contentUids, $record, $table, $this]);

                $pageUids = array_unique($pageUids);
                if (count($pageUids) > 0) {
                    foreach ($pageUids as $uid) {
                        $this->cleanPage($uid);
                    }
                }

                $contentUids = array_unique($contentUids);
                if (count($contentUids) > 0) {
                    foreach ($contentUids as $uid) {
                        $this->cleanContent($uid);
                    }
                }
            }
        }
    }

    /**
     * @param integer $pageUid
     */
    public function cleanPage($pageUid)
    {
        if (!in_array($pageUid, self::$cleanedPages)) {
            $this->getDatahandler()->clear_cacheCmd($pageUid);

            if (ExtensionManagementUtility::isLoaded('imia_base')) {
                \IMIA\ImiaBase\Utility\Cache::flushByTag('pagecontent-' . $pageUid);
            }

            self::$cleanedPages[] = $pageUid;
        }
    }

    /**
     * @param integer $contentUid
     */
    public function cleanContent($contentUid)
    {
        if (ExtensionManagementUtility::isLoaded('imia_base') && !in_array($contentUid, self::$cleanedContents)) {
            if (\IMIA\ImiaBase\Utility\ContentCache::enabled()) {
                \IMIA\ImiaBase\Utility\ContentCache::flushByTag('content-' . $contentUid);
            }
            self::$cleanedContents[] = $contentUid;
            if (self::getCache()) {
                self::getCache()->flushByTag('content-' . $contentUid);

                $content = $this->getDb()->exec_SELECTgetSingleRow(
                    'uid, grid_parent',
                    'tt_content',
                    'uid = ' . $contentUid
                );
                if ($content && $content['grid_parent']) {
                    $this->cleanContent((int)$content['grid_parent']);
                }
            }
        }
    }

    /**
     * @return DataHandler
     */
    protected function getDatahandler()
    {
        if (!$this->dataHandler) {
            $this->dataHandler = GeneralUtility::makeInstance(DataHandler::class);
            $this->dataHandler->stripslashes_values = false;
            $this->dataHandler->start([], []);
        }

        return $this->dataHandler;
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }

    /**
     * @return bool|\TYPO3\CMS\Core\Cache\Frontend\FrontendInterface
     */
    protected static function getCache()
    {
        if (self::$cache === null) {
            if ($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tx_imiabase']) {
                /** @var CacheManager $cacheManager */
                $cacheManager = $contentCache = GeneralUtility::makeInstance(CacheManager::class);

                try {
                    self::$cache = $cacheManager->getCache('tx_imiabase');
                } catch (\Exception $e) {
                }
            }
        }

        return self::$cache;
    }
}