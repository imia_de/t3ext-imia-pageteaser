<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\User;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\DatabaseConnection;

/**
 * @package     imia_pageteaser
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class Page
{
    /**
     * @param array $config
     * @param \TYPO3\CMS\Backend\Form\FormDataProvider\EvaluateDisplayConditions $evaluateDisplayConditions
     * @return boolean
     */
    public function checkCreateLanguage($config, $evaluateDisplayConditions)
    {
        $showCreateLanguage = false;
        $existingLanguage = $this->getDb()->exec_SELECTgetSingleRow('COUNT(uid) AS count', 'pages_language_overlay',
            'pid = ' . (int)$config['record']['uid'] . ' AND deleted = 0');

        if (!$existingLanguage || (int)$existingLanguage['count'] === 0) {
            $showCreateLanguage = true;
        }

        return $showCreateLanguage;
    }

    /**
     * @return boolean
     */
    public function hasCreateLanguages()
    {
        return true;
    }

    /**
     * @param string $key
     * @return string
     */
    protected function translate($key)
    {
        return $GLOBALS['LANG']->sL('LLL:EXT:imia_pageteaser/Resources/Private/Language/locallang.xlf:' . $key);
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}