<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\Domain\Repository;

use IMIA\ImiaBaseExt\Domain\Repository\NoStorageRepository;
use IMIA\ImiaBaseExt\Utility\Helper;
use IMIA\ImiaPageteaser\Domain\Model\Category;
use IMIA\ImiaPageteaser\Domain\Model\Page;
use IMIA\ImiaPageteaser\Domain\Model\PageAlt;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;

/**
 * @package     imia_pageteaser
 * @subpackage  Domain\Repository
 * @author      David Frerich <d.frerich@imia.de>
 */
class PageRepository extends NoStorageRepository
{
    /**
     * @var PageAltRepository
     */
    public $pageAltRepository;

    /**
     * @var CategoryRepository
     */
    public $categoryRepository;

    /**
     * @var boolean
     */
    protected $showAll = false;

    /**
     * @var string
     */
    protected $logicalCategoriesOperator = 'OR';

    /**
     * @return string
     */
    public function getLogicalCategoriesOperator()
    {
        return $this->logicalCategoriesOperator;
    }

    /**
     * @param string $logicalCategoriesOperator
     * @return $this
     */
    public function setLogicalCategoriesOperator($logicalCategoriesOperator)
    {
        $this->logicalCategoriesOperator = $logicalCategoriesOperator;

        return $this;
    }

    /**
     * @param bool $ignoreEnableFields
     * @return $this
     */
    public function setIgnoreEnableFields($ignoreEnableFields)
    {
        $this->getPageAltRepository()->setIgnoreEnableFields($ignoreEnableFields);
        parent::setIgnoreEnableFields($ignoreEnableFields);

        return $this;
    }

    /**
     * @param boolean $showAll
     * @return $this
     */
    public function setShowAll($showAll)
    {
        $this->showAll = $showAll;

        return $this;
    }

    /**
     * @param array $uids
     * @param string $date
     * @param array $excludeUids
     * @return integer
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function countByUids(array $uids, $date = 'all', $excludeUids = [])
    {
        if (count($uids) > 0) {
            $query = $this->createQueryByUids($uids, $date, $excludeUids);

            return $query->count();
        } else {
            return 0;
        }
    }

    /**
     * @param array $uids
     * @param string $date
     * @param array $excludeUids
     * @param integer $limit
     * @param integer $offset
     * @param array $sorting
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface|array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findByUids(array $uids, $date = 'all', $excludeUids = [], $limit = null, $offset = null, $sorting = null)
    {
        if (count($uids) > 0) {
            $query = $this->createQueryByUids($uids, $date, $excludeUids, $limit, $offset, $sorting);

            return $this->executeQuery($query, $limit, $offset, $sorting);
        } else {
            return [];
        }
    }

    /**
     * @param array $uids
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findTeaserPagesByUids(array $uids)
    {
        if (count($uids) > 0) {
            $query = $this->createQuery();
            $query->matching(
                $query->in('uid', $uids)
            );

            $uids = [];
            $result = $this->executeArrayQuery($query, ['teaser_pages']);
            foreach ($result as $row) {
                $uids = array_merge($uids, array_map('intval', explode(',', $row['teaser_pages'])));
            }

            return array_unique($uids);
        } else {
            return [];
        }
    }

    /**
     * @param array $doktypes
     * @param array $categories
     * @param array $filter
     * @param array $sorting
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function findUidsByDoktypesAndCategories(array $doktypes, $categories = null,
                                                    array $filter = null, $sorting = ['sorting', 'asc'])
    {
        $query = $this->createQueryByDoktypesAndCategories($doktypes, $categories, $filter, null, null, $sorting);

        $pageUids = $this->executeUidArrayQuery($query);
        $this->applyFilterForLanguage($pageUids, $filter);

        return $pageUids;
    }

    /**
     * @param array $pIds
     * @param array $doktypes
     * @param array $categories
     * @param array $filter
     * @param array $sorting
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function findUidsByPidsAndDoktypesAndCategories(array $pIds, array $doktypes, $categories = null,
                                                           array $filter = null, $sorting = ['sorting', 'asc'])
    {
        $query = $this->createQueryByPidsAndDoktypesAndCategories(
            $this->getResolvedPageUids($pIds), $doktypes, $categories, $filter, null, null, $sorting);

        $pageUids = $this->executeUidArrayQuery($query);
        $this->applyFilterForLanguage($pageUids, $filter);

        return $pageUids;
    }

    /**
     * @param int $parentPageId
     * @param int $archiveDateSeconds
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findByPidAndArchivedateOlderThan($parentPageId, $archiveDateSeconds)
    {
        $query = $this->createQuery();

        $constraints = [
            $query->equals('pid', $parentPageId),
            $query->greaterThan('archivedate', 0),
            $query->lessThanOrEqual('archivedate', (time() - $archiveDateSeconds)),
        ];

        $query->matching($query->logicalAnd($constraints));

        return $this->executeQuery($query);
    }

    /**
     * @param array $uIds
     * @param array $doktypes
     * @param array $categories
     * @param array $filter
     * @param array $sorting
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function findUidsByUidsAndDoktypesAndCategories(array $uIds, array $doktypes, $categories = null,
                                                           array $filter = null, $sorting = ['sorting', 'asc'])
    {
        $query = $this->createQueryByUidsAndDoktypesAndCategories($uIds, $doktypes, $categories, $filter, null, null, $sorting);

        $pageUids = $this->executeUidArrayQuery($query);
        $this->applyFilterForLanguage($pageUids, $filter);

        return $pageUids;
    }

    /**
     * @param array $doktypes
     * @param array $categories
     * @param array $filter
     * @return integer
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function countByDoktypesAndCategories(array $doktypes, $categories = null, array $filter = null)
    {
        $query = $this->createQueryByDoktypesAndCategories($doktypes, $categories, $filter);

        return $query->count();
    }

    /**
     * @param array $pIds
     * @param array $doktypes
     * @param array $categories
     * @param array $filter
     * @return integer
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function countByPidsAndDoktypesAndCategories(array $pIds, array $doktypes, $categories = null, array $filter = null)
    {
        $query = $this->createQueryByPidsAndDoktypesAndCategories(
            $this->getResolvedPageUids($pIds), $doktypes, $categories, $filter);

        return $query->count();
    }

    /**
     * @param string $search
     * @param array $pIds
     * @param array $doktypes
     * @param array $categories
     * @param array $filter
     * @return integer
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function countBySearchAndPidsAndDoktypesAndCategories($search, array $pIds, array $doktypes, $categories = null, array $filter = null)
    {
        $query = $this->createQueryBySearchAndPidsAndDoktypesAndCategories(
            $search, $this->getResolvedPageUids($pIds), $doktypes, $categories, $filter);

        return $query->count();
    }

    /**
     * @param array $uIds
     * @param array $doktypes
     * @param array $categories
     * @param array $filter
     * @return integer
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function countByUidsAndDoktypesAndCategories(array $uIds, array $doktypes, $categories = null, array $filter = null)
    {
        $query = $this->createQueryByUidsAndDoktypesAndCategories($uIds, $doktypes, $categories, $filter);

        return $query->count();
    }

    /**
     * @param array $doktypes
     * @param array $categories
     * @param array $filter
     * @param integer $limit
     * @param integer $offset
     * @param array $sorting
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function findByDoktypesAndCategories(array $doktypes, $categories = null,
                                                array $filter = null, $limit = null, $offset = null, $sorting = ['sorting', 'asc'])
    {
        $query = $this->createQueryByDoktypesAndCategories($doktypes, $categories, $filter, $limit, $offset, $sorting);

        return $this->executeQuery($query, $limit, $offset, $sorting);
    }

    /**
     * @param array $pIds
     * @param array $doktypes
     * @param array $categories
     * @param array $filter
     * @param integer $limit
     * @param integer $offset
     * @param array $sorting
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function findByPidsAndDoktypesAndCategories(array $pIds, array $doktypes, $categories = null,
                                                       array $filter = null, $limit = null, $offset = null, $sorting = ['sorting', 'asc'])
    {
        $query = $this->createQueryByPidsAndDoktypesAndCategories(
            $this->getResolvedPageUids($pIds), $doktypes, $categories, $filter, $limit, $offset, $sorting);

        return $this->executeQuery($query, $limit, $offset, $sorting);
    }

    /**
     * @param string $search
     * @param array $pIds
     * @param array $doktypes
     * @param array $categories
     * @param array $filter
     * @param integer $limit
     * @param integer $offset
     * @param array $sorting
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function findBySearchAndPidsAndDoktypesAndCategories($search, array $pIds, array $doktypes, $categories = null,
                                                                array $filter = null, $limit = null, $offset = null, $sorting = ['sorting', 'asc'])
    {
        $query = $this->createQueryBySearchAndPidsAndDoktypesAndCategories(
            $search, $this->getResolvedPageUids($pIds), $doktypes, $categories, $filter, $limit, $offset, $sorting);

        return $this->executeQuery($query, $limit, $offset, $sorting);
    }

    /**
     * @param array $uIds
     * @param array $doktypes
     * @param array $categories
     * @param array $filter
     * @param integer $limit
     * @param integer $offset
     * @param array $sorting
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function findByUidsAndDoktypesAndCategories(array $uIds, array $doktypes, $categories = null,
                                                       array $filter = null, $limit = null, $offset = null, $sorting = ['sorting', 'asc'])
    {
        $query = $this->createQueryByUidsAndDoktypesAndCategories($uIds, $doktypes, $categories, $filter, $limit, $offset, $sorting);

        return $this->executeQuery($query, $limit, $offset, $sorting);
    }

    /**
     * @param string $search
     * @param array $pIds
     * @param array $doktypes
     * @param array $categories
     * @param array $filter
     * @param integer $limit
     * @param integer $offset
     * @param array $sorting
     * @return \TYPO3\CMS\Extbase\Persistence\QueryInterface
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function createQueryBySearchAndPidsAndDoktypesAndCategories($search, array $pIds, array $doktypes, $categories = null,
                                                                       array $filter = null, $limit = null, $offset = null, $sorting = ['sorting', 'asc'])
    {
        if (count($pIds) == 0) {
            $pIds = [0];
        }

        $query = $this->createQuery($limit, $offset, $sorting);
        $constraints = [
            $query->in('pid', $pIds),
            $query->in('doktype', $doktypes),
        ];
        if ($search) {
            $constraints[] = $query->logicalOr([
                $query->like('title', '%' . $search . '%'),
                $query->like('navTitle', '%' . $search . '%'),
            ]);
        }
        $this->addCategoriesConstraints($query, $constraints, $categories);
        $this->addFilterConstraints($query, $constraints, $filter);

        $query->matching(
            $query->logicalAnd($constraints)
        );

        return $query;
    }

    /**
     * @param array $pIds
     * @param array $doktypes
     * @param array $categories
     * @param array $filter
     * @param integer $limit
     * @param integer $offset
     * @param array $sorting
     * @return \TYPO3\CMS\Extbase\Persistence\QueryInterface
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function createQueryByPidsAndDoktypesAndCategories(array $pIds, array $doktypes, $categories = null,
                                                              array $filter = null, $limit = null, $offset = null, $sorting = ['sorting', 'asc'])
    {
        if (count($pIds) == 0) {
            $pIds = [0];
        }

        $query = $this->createQuery($limit, $offset, $sorting);
        $constraints = [
            $query->in('pid', $pIds),
            $query->in('doktype', $doktypes),
        ];
        $this->addCategoriesConstraints($query, $constraints, $categories);
        $this->addFilterConstraints($query, $constraints, $filter);

        $query->matching(
            $query->logicalAnd($constraints)
        );

        return $query;
    }

    /**
     * @param array $uIds
     * @param array $doktypes
     * @param array $categories
     * @param array $filter
     * @param integer $limit
     * @param integer $offset
     * @param array $sorting
     * @return \TYPO3\CMS\Extbase\Persistence\QueryInterface
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    public function createQueryByUidsAndDoktypesAndCategories(array $uIds, array $doktypes, $categories = null,
                                                              array $filter = null, $limit = null, $offset = null, $sorting = ['sorting', 'asc'])
    {
        $query = $this->createQuery($limit, $offset, $sorting);
        $constraints = [
            $query->in('uid', $uIds),
            $query->in('doktype', $doktypes),
        ];
        $this->addCategoriesConstraints($query, $constraints, $categories);
        $this->addFilterConstraints($query, $constraints, $filter);

        $query->matching(
            $query->logicalAnd($constraints)
        );

        return $query;
    }

    /**
     * @param integer $limit
     * @param integer $offset
     * @param array $sorting
     * @return \TYPO3\CMS\Extbase\Persistence\QueryInterface
     */
    public function createQuery($limit = null, $offset = null, $sorting = null)
    {
        // l18n_cfg and sorting problems, limiting / offsetting in executeQuery
        $limit = null;
        $offset = null;

        if ($sorting !== null && (!is_array($sorting) || !array_key_exists($sorting[0], $GLOBALS['TCA']['pages']['columns']))) {
            $sorting = 'uid';
        }

        return parent::createQuery($limit, $offset, $sorting);
    }

    /**
     * @param array $uids
     * @param string $date
     * @param array $excludeUids
     * @param integer $limit
     * @param integer $offset
     * @param array $sorting
     * @return \TYPO3\CMS\Extbase\Persistence\QueryInterface
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    protected function createQueryByUids(array $uids, $date = 'all', $excludeUids = [], $limit = null, $offset = null, $sorting = null)
    {
        $query = $this->createQuery($limit, $offset, $sorting);

        $constraints = [$query->in('uid', $uids)];
        if ($excludeUids) {
            $constraints[] = $query->logicalNot($query->in('uid', $excludeUids));
        }

        switch ($date) {
            case 'future':
                $constraints[] = $query->greaterThanOrEqual('lastUpdated', time());
                break;
            case 'past':
                $constraints[] = $query->lessThanOrEqual('lastUpdated', time());
                break;
            case 'notarchived':
                $constraints[] = $query->logicalOr([
                    $query->greaterThanOrEqual('archivedate', time()),
                    $query->equals('archivedate', null),
                    $query->equals('archivedate', 0),
                ]);
                break;
            case 'archived':
                $constraints[] = $query->logicalAnd([
                    $query->lessThanOrEqual('archivedate', time()),
                    $query->logicalNot($query->equals('archivedate', null)),
                    $query->logicalNot($query->equals('archivedate', 0)),
                ]);
                break;
            default:
                break;
        }

        $query->matching($query->logicalAnd($constraints));

        return $query;
    }

    /**
     * @param array $doktypes
     * @param array $categories
     * @param array $filter
     * @param integer $limit
     * @param integer $offset
     * @param array $sorting
     * @return \TYPO3\CMS\Extbase\Persistence\QueryInterface
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    protected function createQueryByDoktypesAndCategories(array $doktypes, $categories = null,
                                                          array $filter = null, $limit = null, $offset = null, $sorting = ['sorting', 'asc'])
    {
        $query = $this->createQuery($limit, $offset, $sorting);

        $constraints = [
            $query->in('doktype', $doktypes),
        ];
        $this->addCategoriesConstraints($query, $constraints, $categories);
        $this->addFilterConstraints($query, $constraints, $filter);

        $query->matching(
            $query->logicalAnd($constraints)
        );

        return $query;
    }

    /**
     * @param array $pageUids
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function getResolvedPageUids(array $pageUids)
    {
        $pageUids[] = -1;

        $query = $this->createQuery();
        $query->matching($query->in('uid', $pageUids));

        $resolvedPageUids = [-1];
        foreach ($query->execute() as $page) {
            if ($page->getDoktype() == 7 && $page->getMountPid()) {
                $resolvedPageUids[$page->getUid()] = $page->getMountPid();
            } elseif ($page->getContentFromPid()) {
                $resolvedPageUids[$page->getUid()] = $page->getContentFromPid();
            } else {
                $resolvedPageUids[$page->getUid()] = $page->getUid();
            }
        }

        return $resolvedPageUids;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
     * @param array $constraints
     * @param array $categories
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    protected function addCategoriesConstraints($query, array &$constraints, $categories = null)
    {
        if ($categories && count($categories) > 0) {
            $categoryConstraints = [];
            foreach ($categories as $category) {
                /** @var Category $category */
                $categoryConstraints[] = $query->contains('categories', $category);
            }

            if ($this->logicalCategoriesOperator == 'AND') {
                $constraints[] = $query->logicalAnd($categoryConstraints);
            } else {
                $constraints[] = $query->logicalOr($categoryConstraints);
            }
        }
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
     * @param array $constraints
     * @param array $filter
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception\UnsupportedMethodException
     */
    protected function addFilterConstraints($query, array &$constraints, array $filter = null)
    {
        if ($filter && count($filter) > 0) {
            if ($this->getLanguageUid() == 0) {
                if (array_key_exists('word', $filter) && $filter['word']) {
                    $searchWordContraints = [];
                    foreach ($this->getSearchWordFields() as $field) {
                        $searchWordContraints[] = $query->like($field, '%' . $filter['word'] . '%');
                    }
                    if (count($searchWordContraints) > 0) {
                        $constraints[] = $query->logicalOr($searchWordContraints);
                    }
                }

                if (array_key_exists('abc', $filter) && $filter['abc']) {
                    $abc = explode('|', $filter['abc']);

                    $abcConstraints = [];
                    foreach ($abc as $letter) {
                        $letter = strtolower(substr(trim($letter), 0, 1));
                        if ($letter) {
                            $abcConstraints[] = $query->like('title', $letter . '%');
                        }
                    }

                    if (count($abcConstraints) > 0) {
                        $constraints[] = $query->logicalOr($abcConstraints);
                    }
                }
            }

            if (array_key_exists('year', $filter) && $filter['year']) {
                if (array_key_exists('month', $filter) && $filter['month']) {
                    if (array_key_exists('day', $filter) && $filter['day']) {
                        // year and month and day
                        $constraints[] = $query->logicalAnd([
                            $query->greaterThanOrEqual($filter['archivedate'] ? 'archivedate' : 'lastUpdated', mktime(0, 0, 0, (int)$filter['month'], (int)$filter['day'], (int)$filter['year'])),
                            $query->lessThan('lastUpdated', mktime(0, 0, 0, (int)$filter['month'], (int)$filter['day'] + 1, (int)$filter['year'])),
                        ]);
                    } else {
                        // year and month
                        $constraints[] = $query->logicalAnd([
                            $query->greaterThanOrEqual($filter['archivedate'] ? 'archivedate' : 'lastUpdated', mktime(0, 0, 0, (int)$filter['month'], 1, (int)$filter['year'])),
                            $query->lessThan('lastUpdated', mktime(0, 0, 0, (int)$filter['month'] + 1, 1, (int)$filter['year'])),
                        ]);
                    }
                } else {
                    // years
                    $constraints[] = $query->logicalAnd([
                        $query->greaterThanOrEqual($filter['archivedate'] ? 'archivedate' : 'lastUpdated', mktime(0, 0, 0, 1, 1, (int)$filter['year'])),
                        $query->lessThan('lastUpdated', mktime(0, 0, 0, 13, 1, (int)$filter['year'])),
                    ]);
                }
            } elseif (array_key_exists('month', $filter) && $filter['month']) {
                $monthConstraints = [];
                for ($i = 2000; $i <= (int)date('Y'); $i++) {
                    // month
                    $monthConstraints = $query->logicalAnd([
                        $query->greaterThanOrEqual($filter['archivedate'] ? 'archivedate' : 'lastUpdated', mktime(0, 0, 0, (int)$filter['month'], 1, $i)),
                        $query->lessThan('lastUpdated', mktime(0, 0, 0, (int)$filter['month'] + 1, 1, $i)),
                    ]);
                }
                $constraints[] = $query->logicalOr($monthConstraints);
            } elseif (array_key_exists('month-year', $filter) && $filter['month-year']) {
                // month-year in one field
                $myFilter = explode('-', $filter['month-year']);
                if (count($myFilter) == 2) {
                    $constraints[] = $query->logicalAnd([
                        $query->greaterThanOrEqual($filter['archivedate'] ? 'archivedate' : 'lastUpdated', mktime(0, 0, 0, (int)$myFilter[0], 1, (int)$myFilter[1])),
                        $query->lessThan('lastUpdated', mktime(0, 0, 0, (int)$myFilter[0] + 1, 1, (int)$myFilter[1])),
                    ]);
                }
            } elseif (array_key_exists('date', $filter) && $filter['date']) {
                // date in one field
                $date = strtotime($filter['date']);
                if ($date) {
                    $dateTime = new \DateTime();
                    $dateTime->setTimestamp($date);
                    $dateTimeTo = clone $dateTime;

                    $dateTime->setTime(0, 0);
                    $dateTimeTo->modify('+1 day');

                    $constraints[] = $query->logicalAnd([
                        $query->greaterThanOrEqual($filter['archivedate'] ? 'archivedate' : 'lastUpdated', $dateTime->getTimestamp()),
                        $query->lessThan('lastUpdated', $dateTimeTo->getTimestamp()),
                    ]);
                }
            } else {
                if (array_key_exists('dateFrom', $filter) && $filter['dateFrom']) {
                    // date in one field
                    $date = strtotime($filter['dateFrom']);
                    if ($date) {
                        $dateTime = new \DateTime();
                        $dateTime->setTimestamp($date);
                        $dateTime->setTime(0, 0);

                        $constraints[] = $query->logicalAnd([
                            $query->greaterThanOrEqual($filter['archivedate'] ? 'archivedate' : 'lastUpdated', $dateTime->getTimestamp()),
                        ]);
                    }
                }
                if (array_key_exists('dateTo', $filter) && $filter['dateTo']) {
                    // date in one field
                    $date = strtotime($filter['dateTo']);
                    if ($date) {
                        $dateTime = new \DateTime();
                        $dateTime->setTimestamp($date);
                        $dateTime->setTime(23, 59);

                        $constraints[] = $query->logicalAnd([
                            $query->lessThanOrEqual($filter['archivedate'] ? 'archivedate' : 'lastUpdated', $dateTime->getTimestamp()),
                        ]);
                    }
                }
            }

            if (array_key_exists('pid', $filter) && $filter['pid']) {
                $constraints[] = $query->equals('pid', (int)$filter['pid']);
            }

            $respectSysLanguage = $this->getCategoryRepository()->getRespectSysLanguage();
            $this->getCategoryRepository()->setRespectSysLanguage(false);

            if (array_key_exists('categories', $filter) && $filter['categories'] && is_array($filter['categories'])) {
                $categories = $this->getCategoryRepository()
                    ->findByUids($filter['categories']);
                $categoryContraints = [];
                foreach ($categories as $category) {
                    $categoryContraints[] = $query->contains('categories', $category);
                }

                if (count($categoryContraints) > 0) {
                    $constraints[] = $query->logicalOr($categoryContraints);
                }
            }

            if (array_key_exists('category', $filter) && $filter['category']) {
                $this->getDb()->store_lastBuiltQuery = true;

                $category = $this->getCategoryRepository()->findOneByUid((int)$filter['category']);
                if ($category) {
                    $constraints[] = $query->contains('categories', $category);
                }
            }

            $this->getCategoryRepository()->setRespectSysLanguage($respectSysLanguage);

            $additionalFilterOptions = array_diff(array_keys($filter), $this->getFilterKeyOptions());
            foreach ($additionalFilterOptions as $filterKey) {
                if (array_key_exists($filterKey, $filter) && $filter[$filterKey]) {
                    $this->getFilterContraint($query, $filter, $filterKey, $constraints);
                }
            }
        }
    }

    /**
     * @param QueryInterface $query
     * @param array $filter
     * @param string $filterKey
     * @param array $constraints
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    protected function getFilterContraint($query, $filter, $filterKey, &$constraints)
    {
        if (is_array($filter[$filterKey])) {
            $hasObjects = false;
            foreach ($filter[$filterKey] as $value) {
                if (is_object($value)) {
                    $hasObjects = true;
                }
            }
            if ($hasObjects) {
                $subconstraints = [];
                foreach ($filter[$filterKey] as $value) {
                    if (is_object($value)) {
                        $subconstraints[] = $query->contains($filterKey, $value);
                    } else {
                        $subconstraints[] = $query->like($filterKey, $value);
                    }
                }

                $constraints[] = $query->logicalOr($subconstraints);
            } else {
                $constraints[] = $query->in($filterKey, $filter[$filterKey]);
            }
        } elseif (is_object($filter[$filterKey])) {
            $constraints[] = $query->contains($filterKey, $filter[$filterKey]);
        } else {
            $constraints[] = $query->like($filterKey, $filter[$filterKey]);
        }
    }

    /**
     * @return array
     */
    protected function getFilterKeyOptions()
    {
        return [
            'word', 'abc', 'year', 'month', 'day', 'pid', 'month-year', 'categories', 'category',
            'archivedate', 'date', 'dateFrom', 'dateTo',
        ];
    }

    /**
     * @return array
     */
    protected function getSearchWordFields()
    {
        return ['title', 'subtitle', 'abstract'];
    }

    /**
     * @param array $pageUids
     * @param array $filter
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    protected function applyFilterForLanguage(&$pageUids, $filter)
    {
        // get all localized pages or original record
        // workaround for l18nCfg and limit
        $pages = $this->findByUids($pageUids);
        $pageUids = [];
        foreach ($pages as $page) {
            $pageUids[] = $page->getUid();
        }

        if ($this->getLanguageUid() > 0) {
            if ($filter && count($filter) > 0) {
                foreach ($pages as $page) {
                    if (array_key_exists('word', $filter) && $filter['word']) {
                        if (!(stripos($page->getTitle(), $filter['word']) !== false
                                || stripos($page->getSubtitle(), $filter['word']) !== false
                                || stripos($page->getAbstract(), $filter['word'])) !== false
                        ) {
                            $pageUids = array_diff($pageUids, [$page->getPage()->getUid()]);
                        }
                    }

                    if (array_key_exists('abc', $filter) && $filter['abc']) {
                        $pageLetter = strtolower(substr(trim($page->getTitle()), 0, 1));
                        $isInFilter = -1;

                        $abc = explode('|', $filter['abc']);
                        foreach ($abc as $letter) {
                            $letter = strtolower(substr(trim($letter), 0, 1));
                            if ($letter) {
                                if ($isInFilter == -1) {
                                    $isInFilter = 0;
                                }

                                if ($pageLetter == $letter) {
                                    $isInFilter = 1;
                                }
                            }
                        }

                        if ($isInFilter == 0) {
                            $pageUids = array_diff($pageUids, [$page->getPage()->getUid()]);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
     * @param integer $limit
     * @param integer $offset
     * @param array $sorting
     * @return array
     */
    protected function executeQuery($query, $limit = null, $offset = null, $sorting = null)
    {
        $pages = $query->execute();

        $pageUids = [];
        $pagesArray = [];
        foreach ($pages as $page) {
            $pageUids[] = $page->getUid();
            $pagesArray[$page->getUid()] = $page;
        }

        if ($this->getLanguageUid() > 0 && count($pageUids) > 0) {
            $altPages = $this->getPageAltRepository()->findByPagesAndLanguage($pageUids, $this->getLanguageUid());
            foreach ($altPages as $altPage) {
                /** @var PageAlt $altPage */
                $pagesArray[$altPage->getPage()->getUid()] = $altPage;
            }
        }

        if (!$this->showAll) {
            $newPagesArray = [];

            // l18n_cfg setting
            foreach ($pagesArray as $key => $page) {
                /** @var Page $page */
                switch ($page->getL18nCfg()) {
                    case 1:
                        // hide default translation
                        if ($this->getLanguageUid() != 0) {
                            $newPagesArray[] = $page;
                        }
                        break;
                    case 2:
                        // hide page if no translation for current language exists
                        if ($this->getLanguageUid() == $page->getSysLanguageUid()) {
                            $newPagesArray[] = $page;
                        }
                        break;
                    case 3:
                        // hide default translation
                        // hide page if no translation for current language exists
                        if ($this->getLanguageUid() != 0 && $this->getLanguageUid() == $page->getSysLanguageUid()) {
                            $newPagesArray[] = $page;
                        }
                        break;
                    case 0:
                    default:
                        $newPagesArray[] = $page;
                        break;
                }
            }

            $pagesArray = $newPagesArray;
        }

        if ($sorting) {
            $pagesArray = Helper::sortObjectArrayByProperty($pagesArray, $sorting[0], strtolower($sorting[1]));
        }
        $pagesArray = array_slice($pagesArray, $offset ?: 0, $limit ?: 99999999);

        return $pagesArray;
    }

    /**
     * @return integer
     */
    public function getLanguageUid()
    {
        if ($this->languageUid !== null) {
            $languageUid = $this->languageUid;
        } else {
            $languageUid = 0;
            if ($GLOBALS['TSFE']) {
                $languageUid = $GLOBALS['TSFE']->sys_language_uid;
            }
        }

        return $languageUid;
    }

    /**
     * @return PageAltRepository
     */
    protected function getPageAltRepository()
    {
        if (!$this->pageAltRepository) {
            $this->pageAltRepository = $this->getObjectManager()->get(PageAltRepository::class);
        }

        return $this->pageAltRepository;
    }

    /**
     * @return CategoryRepository
     */
    protected function getCategoryRepository()
    {
        if (!$this->categoryRepository) {
            $this->categoryRepository = $this->getObjectManager()->get(CategoryRepository::class);
        }

        return $this->categoryRepository;
    }

    /**
     * @return ObjectManager
     */
    protected function getObjectManager()
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);

        return $objectManager;
    }
}