<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\Domain\Model;

use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use IMIA\ImiaBaseExt\Annotation\SQL;
use IMIA\ImiaBaseExt\Annotation\TCA;

/**
 * @package     imia_pageteaser
 * @subpackage  Domain\Model
 * @author      David Frerich <d.frerich@imia.de>
 *
 * @TCA\Table(
 *  sortby = "sorting",
 *  default_sortby = "ORDER BY sorting ASC"
 * )
 */
class Category extends \IMIA\ImiaBaseExt\Domain\Model\Category
{
    /**
     * @SQL\Column(type="int", length=11)
     *
     * @var integer
     */
    protected $sorting;

    /**
     * @var \IMIA\ImiaPageteaser\Domain\Model\Category|NULL
     * @lazy
     */
    protected $parent = null;

    /**
     * @SQL\Column(type="int", length=11)
     * @TCA\Inline(
     *  foreign_table = "sys_category",
     *  foreign_field = "parent",
     *  maxitems = 9999
     * )
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\IMIA\ImiaPageteaser\Domain\Model\Category>
     */
    protected $children;

    /**
     * @SQL\Column(name="items", create=false)
     * @TCA\Passthrough
     *
     * @var integer
     */
    protected $items;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new ObjectStorage();
    }

    /**
     * @return int
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\IMIA\ImiaPageteaser\Domain\Model\Category>
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return \IMIA\ImiaPageteaser\Domain\Model\Category
     */
    public function getParent()
    {
        if ($this->parent instanceof \TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy) {
            $this->parent->_loadRealInstance();
        }

        return $this->parent;
    }

    /**
     * @return array
     */
    public function getAllChildren()
    {
        $children = [];
        foreach ($this->getChildren() as $category) {
            $children[] = $category;
            $children = array_merge($children, $category->getAllChildren());
        }

        return $children;
    }

    /**
     * @return int
     */
    public function getItems()
    {
        return $this->items;
    }
}