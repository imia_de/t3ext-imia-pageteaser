<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\View\Teaser;

use IMIA\ImiaPageteaser\Domain\Model\Page;

/**
 * @package     imia_pageteaser
 * @subpackage  View\Teaser
 * @author      David Frerich <d.frerich@imia.de>
 */
class IndexJSON extends \TYPO3\CMS\Extbase\Mvc\View\AbstractView
{
    public function render()
    {
        $cleanPages = [];
        $pages = $this->variables['pages'];

        /** @var Page $page */
        foreach ($pages as $page) {
            $cleanPages[] = [
                'id'          => $page->getUid(),
                'start'       => $page->getLastUpdated()->format('Y-m-d'),
                'title'       => $page->getTitle(),
                'description' => $page->getDescription(),
            ];
        }

        return json_encode($cleanPages);
    }
}