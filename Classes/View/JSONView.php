<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\View;

use IMIA\ImiaBaseExt\Domain\Model\BaseEntity;
use IMIA\ImiaBaseExt\Traits\HookTrait;
use IMIA\ImiaPageteaser\Domain\Model\Page;
use Spatie\ArrayToXml\ArrayToXml;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * @package     imia_pageteaser
 * @subpackage  View\Teaser
 * @author      David Frerich <d.frerich@imia.de>
 */
class JSONView extends \TYPO3\CMS\Extbase\Mvc\View\AbstractView
{
    use HookTrait;

    /**
     * @var array
     */
    protected $settings;

    /**
     * @return string
     */
    public function render()
    {
        $content = ['items' => []];
        foreach ($this->variables['pages'] as $page) {
            $content['items'][] = $this->renderPage($page);
        }

        $content['pagesCount'] = $this->variables['pagesCount'];
        $content['pageNum'] = $this->variables['pageNum'];
        $content['pageCount'] = $this->variables['pageCount'];

        $this->callHook('render', [&$content, &$this]);

        return json_encode($content);
    }

    /**
     * @param Page $page
     * @return array
     */
    protected function renderPage($page)
    {
        $fields = array_map('trim', explode(',', $this->getSettings('fields')));
        if (count($fields) > 0) {
            $row = [];
            foreach ($fields as $field) {
                if ($field == 'url') {
                    $row[$field] = $this->getTSFE()->cObj->typoLink('', [
                        'parameter'    => $page->getUid(),
                        'useCacheHash' => 1,
                        'returnLast'   => 'url',
                    ]);
                    continue;
                }

                $getter = 'get' . ucfirst($field);
                if (method_exists($page, $getter)) {
                    $value = $page->$getter();
                    if (is_object($value)) {
                        if ($value instanceof ObjectStorage) {
                            $newValue = [];
                            foreach ($value as $subvalue) {
                                if ($subvalue instanceof BaseEntity) {
                                    $newValue[] = $subvalue->getRow();
                                } elseif ($subvalue instanceof AbstractEntity) {
                                    $subRow = [
                                        'uid' => $subvalue->getUid(),
                                    ];
                                    if (method_exists($subvalue, 'getTitle')) {
                                        $subRow['title'] = $subvalue->getTitle();
                                    }
                                    if (method_exists($subvalue, 'getName')) {
                                        $subRow['name'] = $subvalue->getName();
                                    }

                                    $newValue[] = $subRow;
                                } else {
                                    $newValue[] = (array)$subvalue;
                                }
                            }

                            $value = $newValue;
                        } elseif ($value instanceof BaseEntity) {
                            $value = $value->getRow();
                        } elseif ($value instanceof AbstractEntity) {
                            $value = $value->getUid();
                        } elseif ($value instanceof \DateTime) {
                            $value = $value->format('Y-m-d H:i');
                        } elseif ($value instanceof FileReference) {
                            $value = $value->getPublicUrl();
                        } else {
                            $value = (array)$value;
                        }
                    }

                    $row[$field] = $value;
                }
            }
        } else {
            $row = $page->getRow(false);
        }

        $this->callHook('renderPage', [&$row, $page, &$this]);

        return $row;
    }

    /**
     * @param string $content
     * @return string
     */
    protected function wrap($content)
    {
        $wrappedContent = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><root>' . $content . '</root>';

        $this->callHook('wrap', [&$wrappedContent, $content, &$this]);

        return $wrappedContent;
    }

    /**
     * @param array $array
     * @return string
     */
    protected function arrayToXml($array)
    {
        return trim(preg_replace('/^<root>(.*)<\/root>$/ism', '$1',
            trim(str_replace('<?xml version="1.0"?>', '',
                ArrayToXml::convert($array)))));
    }

    /**
     * @param string $key
     * @return array|mixed|null
     */
    protected function getSettings($key = null)
    {
        if (!$this->settings) {
            $this->settings = $this->getTSFE()->tmpl->setup['pageTeaserJSON.']['settings.'];
        }

        if ($key) {
            return isset($this->settings[$key]) ? $this->settings[$key] : null;
        } else {
            return $this->settings;
        }
    }

    /**
     * @return TypoScriptFrontendController
     */
    protected function getTSFE()
    {
        return $GLOBALS['TSFE'];
    }
}