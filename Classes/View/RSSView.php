<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\View;

use IMIA\ImiaPageteaser\Domain\Model\Page;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * @package     imia_pageteaser
 * @subpackage  View\Teaser
 * @author      David Frerich <d.frerich@imia.de>
 */
class RSSView extends XMLView
{
    /**
     * @param Page $page
     * @return string
     */
    protected function renderPage($page)
    {
        $link = $this->getTSFE()->cObj->typoLink('', [
            'parameter'    => $page->getUid(),
            'useCacheHash' => 1,
            'returnLast'   => 'url',
        ]);

        $categoryTitles = [];
        foreach ($page->getCategories() as $category) {
            $categoryTitles[] = $category->getTitle();
        }

        if (isset($this->variables['settings']['rss']['html']) && $this->variables['settings']['rss']['html']) {
            $image = '';
            if ($page->getTeaserImage()) {
                $image = $this->getTSFE()->cObj->cObjGetSingle('IMAGE', [
                    'file'  => $page->getTeaserImage()->getUid(),
                    'file.' => [
                        'treatIdAsReference' => 1,
                        'maxW'               => 150,
                    ],
                ]);
            }

            $description = '<table border="0" cellpadding="2" cellspacing="3"><tr><td width="150">' . ($page->getTeaserImage() ? $image : '') .
                '</td><td>' . nl2br($page->getAbstract()) . '</td></tr></table>';
        } else {
            $description = $page->getAbstract();
        }

        $item = [
            'title'       => $page->getTitle(),
            'link'        => $link,
            'description' => $description,
            'guid'        => [
                '@attributes' => [
                    'isPermaLink' => true,
                ],
                '@value'      => $link,
            ],
            'pubDate'     => $page->getLastUpdated()->format(\DateTime::RSS),
        ];

        if ($page->getAuthor()) {
            $item['author'] = $page->getAuthor();
        }
        if (count($categoryTitles) > 0) {
            $item['category'] = implode(', ', $categoryTitles);
        }
        if ($page->getTeaserImage()) {
            $item['enclosure'] = [
                '@attributes' => [
                    'url'    => $page->getTeaserImage()->getPublicUrl(),
                    'type'   => $page->getTeaserImage()->getMimeType(),
                    'length' => strlen($page->getTeaserImage()->getContents()),
                ],
            ];
            $item['media:content'] = [
                '@attributes' => [
                    'xmlns:media' => 'http://search.yahoo.com/mrss/',
                    'url'         => $page->getTeaserImage()->getPublicUrl(),
                    'type'        => $page->getTeaserImage()->getMimeType(),
                    'fileSize'    => strlen($page->getTeaserImage()->getContents()),
                    'medium'      => 'image',
                ],
            ];
        }

        $this->callHook('renderPageItem', [&$item, $page, &$this]);

        $content = $this->arrayToXml([
            'item' => $item,
        ]);

        $this->callHook('renderPage', [&$content, $page, &$this]);

        return $content;
    }

    /**
     * @param string $content
     * @return string
     */
    protected function wrap($content)
    {
        $date = new \DateTime('NOW');

        if (isset($this->variables['settings']['rss']['title']) && $this->variables['settings']['rss']['title']) {
            $title = $this->variables['settings']['rss']['title'];
        } else {
            $title = ($this->variables['contentObject']['header'] ?: $GLOBALS['TSFE']->page['title']) . ' | ' . $GLOBALS['TSFE']->rootLine[0]['title'];
        }

        $description = '';
        if (isset($this->variables['settings']['rss']['description']) && $this->variables['settings']['rss']['description']) {
            $description = $this->variables['settings']['rss']['description'];
        }

        $image = '';
        if (isset($this->variables['settings']['rss']['image']) && $this->variables['settings']['rss']['image']) {
            /** @var ObjectManager $objectManager */
            $objectManager = GeneralUtility::makeInstance(ObjectManager::class);

            /** @var FileRepository $fileRepository */
            $fileRepository = $objectManager->get(FileRepository::class);

            $files = $fileRepository->findByRelation('tt_content', 'rssImage', $this->variables['contentObject']['uid']);
            if ($files && count($files) > 0) {
                /** @var FileReference $file */
                $file = array_first($files);
                $image = $file->getPublicUrl();
            }
        }

        $copyright = '';
        if (isset($this->variables['settings']['rss']['copyright']) && $this->variables['settings']['rss']['copyright']) {
            $copyright = $this->variables['settings']['rss']['copyright'];
        }

        $managingEditor = '';
        if (isset($this->variables['settings']['rss']['managingEditor']) && $this->variables['settings']['rss']['managingEditor']) {
            $managingEditor = $this->variables['settings']['rss']['managingEditor'];
        }

        $webMaster = '';
        if (isset($this->variables['settings']['rss']['webMaster']) && $this->variables['settings']['rss']['webMaster']) {
            $webMaster = $this->variables['settings']['rss']['webMaster'];
        }

        $category = '';
        if (isset($this->variables['settings']['rss']['category']) && $this->variables['settings']['rss']['category']) {
            $category = $this->variables['settings']['rss']['category'];
        }

        $link = $this->getTSFE()->cObj->typoLink('', [
            'parameter'       => $this->getTSFE()->id,
            'addQueryString'  => 1,
            'addQueryString.' => [
                'exclude' => 'type,tx_imiapageteaser_teaser_cached,tx_imiapageteaser_teaser',
            ],
            'useCacheHash'    => 1,
            'returnLast'      => 'url',
        ]);

        $info = [
            'title'         => $title,
            'link'          => $link,
            'language'      => $this->getTSFE()->lang,
            'pubDate'       => $date->format(\DateTime::RSS),
            'lastBuildDate' => $date->format(\DateTime::RSS),
            'generator'     => 'TYPO3 IMIA Pageteaser',
            'ttl'           => '60',
        ];

        if ($description) {
            $info['description'] = $description;
        }
        if ($copyright) {
            $info['copyright'] = $copyright;
        }
        if ($managingEditor) {
            $info['managingEditor'] = $managingEditor;
        }
        if ($webMaster) {
            $info['webMaster'] = $webMaster;
        }
        if ($category) {
            $info['category'] = $category;
        }
        if ($image) {
            $info['image'] = [
                'url'   => GeneralUtility::getIndpEnv('TYPO3_REQUEST_HOST') . '/' . $image,
                'title' => $title,
                'link'  => $link,
            ];
        }

        $this->callHook('info', [&$info, &$this]);

        $wrappedContent = '<?xml version="1.0" encoding="utf-8"?><rss version="2.0"><channel>' . $this->arrayToXml($info) . $content . '</channel></rss>';

        $this->callHook('wrap', [&$wrappedContent, $content, &$this]);

        return $wrappedContent;
    }
}