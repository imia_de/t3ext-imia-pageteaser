<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\Xclass\Frontend\Controller;

use IMIA\ImiaBaseExt\Traits\HookTrait;
use TYPO3\CMS\Core\Error\Http\ServiceUnavailableException;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\Page\PageRepository;

/**
 * @package     imia_pageteaser
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class TypoScriptFrontendController extends \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController
{
    use HookTrait;

    /**
     * @var array
     */
    protected static $shortcutDoktypes = [104, 114, 124, 134, 144];

    /**
     * @var array
     */
    protected static $fileDoktypes = [196, 106, 116, 126, 136, 146];

    public function sendCacheHeaders()
    {
        $indexedSearch = false;
        if (is_array($this->config['INTincScript'])) {
            foreach ($this->config['INTincScript'] as $key => $conf) {
                if (isset($conf['conf']['extensionName']) && $conf['conf']['extensionName'] == 'IndexedSearch') {
                    $indexedSearch = true;
                }
            }
        }

        if ($indexedSearch) { // dont send cache headers for indexed search (no-store = true and back issue)
            $headers = [
                'Cache-Control: private',
            ];
            $this->isClientCachable = false;

            // Now, if a backend user is logged in, tell him in the Admin Panel log what the caching status would have been:
            if ($this->beUserLogin) {
                if ($this->isStaticCacheble()) {
                    $this->getTimeTracker()->setTSlogMessage('Cache-headers with max-age "' . ($this->cacheExpires - $GLOBALS['EXEC_TIME']) . '" would have been sent');
                } else {
                    $reasonMsg = '';
                    $reasonMsg .= !$this->no_cache ? '' : 'Caching disabled (no_cache). ';
                    $reasonMsg .= !$this->isINTincScript() ? '' : '*_INT object(s) on page. ';
                    $reasonMsg .= !is_array($this->fe_user->user) ? '' : 'Frontend user logged in. ';
                    $this->getTimeTracker()->setTSlogMessage('Cache-headers would disable proxy caching! Reason(s): "' . $reasonMsg . '"', 1);
                }
            }

            // Send headers:
            foreach ($headers as $hL) {
                header($hL);
            }
        } else {
            parent::sendCacheHeaders();
        }
    }

    /**
     * @return array
     */
    public static function getShortcutDoktypes()
    {
        $shortcutDoktypes = self::$shortcutDoktypes;
        if (is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['tx_imiapageteaser']['Teaser'])) {
            foreach ($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['tx_imiapageteaser']['Teaser'] as $_classRef) {
                $hookObject = GeneralUtility::getUserObj($_classRef);
                if (method_exists($hookObject, 'getAdditionalShortcut')) {
                    call_user_func_array([$hookObject, 'getAdditionalShortcut'], [&$shortcutDoktypes]);
                }
            }
        }

        return $shortcutDoktypes;
    }

    /**
     * @return array
     */
    public static function getFileDoktypes()
    {
        $fileDoktypes = self::$fileDoktypes;
        if (is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['tx_imiapageteaser']['Teaser'])) {
            foreach ($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['tx_imiapageteaser']['Teaser'] as $_classRef) {
                $hookObject = GeneralUtility::getUserObj($_classRef);
                if (method_exists($hookObject, 'getAdditionalFile')) {
                    call_user_func_array([$hookObject, 'getAdditionalFile'], [&$fileDoktypes]);
                }
            }
        }

        return $fileDoktypes;
    }

    /**
     * @throws ServiceUnavailableException
     * @throws \TYPO3\CMS\Core\Error\Http\PageNotFoundException
     * @throws \Exception
     */
    public function getPageAndRootline()
    {
        parent::getPageAndRootline();

        if (in_array($this->page['doktype'], self::getShortcutDoktypes())) {
            $this->MP = '';
            $this->originalShortcutPage = $this->page;

            try {
                $this->page = $this->getPageShortcut($this->page['shortcut'], $this->page['shortcut_mode'], $this->page['uid']);
            } catch (\Exception $e) {
                $this->page = $this->getPageShortcut($this->page['shortcut'], 3, $this->page['uid']);
            }

            $this->id = $this->page['uid'];
            $this->rootLine = $this->sys_page->getRootLine($this->id, $this->MP);
            if (!count($this->rootLine)) {
                $ws = $this->whichWorkspace();
                if ($this->sys_page->error_getRootLine_failPid == -1 && $ws) {
                    $this->sys_page->versioningPreview = true;
                    $this->rootLine = $this->sys_page->getRootLine($this->id, $this->MP);
                }
                if (!count($this->rootLine)) {
                    $message = 'The requested page didn\'t have a proper connection to the tree-root!';
                    if ($this->checkPageUnavailableHandler()) {
                        $this->pageUnavailableAndExit($message);
                    } else {
                        $rootline = '(' . $this->sys_page->error_getRootLine . ')';
                        GeneralUtility::sysLog($message, 'cms', GeneralUtility::SYSLOG_SEVERITY_ERROR);
                        throw new ServiceUnavailableException($message . '<br /><br />' . $rootline, 1301648167);
                    }
                }
                $this->fePreview = 1;
            }

            if ($this->checkRootlineForIncludeSection()) {
                if (!count($this->rootLine)) {
                    $message = 'The requested page was not accessible!';
                    if ($this->checkPageUnavailableHandler()) {
                        $this->pageUnavailableAndExit($message);
                    } else {
                        GeneralUtility::sysLog($message, 'cms', GeneralUtility::SYSLOG_SEVERITY_ERROR);
                        throw new ServiceUnavailableException($message, 1301648234);
                    }
                } else {
                    $el = reset($this->rootLine);
                    $this->id = $el['uid'];
                    $this->page = $this->sys_page->getPage($this->id);
                    $this->rootLine = $this->sys_page->getRootLine($this->id, $this->MP);
                }
            }
        }
    }

    /**
     * @param integer $SC
     * @param integer $mode
     * @param integer $thisUid
     * @param integer $itera
     * @param array $pageLog
     * @param bool $disableGroupCheck If true, the group check is disabled when fetching the target page (needed e.g. for menu generation)
     * @return mixed
     * @throws \RuntimeException
     * @throws \TYPO3\CMS\Core\Error\Http\PageNotFoundException
     */
    public function getPageShortcut($SC, $mode, $thisUid, $itera = 20, $pageLog = [], $disableGroupCheck = false)
    {
        $page = parent::getPageShortcut($SC, $mode, $thisUid, $itera, $pageLog, $disableGroupCheck);

        if (in_array($page['doktype'], self::getShortcutDoktypes())) {
            if (!in_array($page['uid'], $pageLog) && $itera > 0) {
                $pageLog[] = $page['uid'];
                $page = $this->getPageShortcut($page['shortcut'], $page['shortcut_mode'], $page['uid'], $itera - 1, $pageLog);
            } else {
                $pageLog[] = $page['uid'];
                $message = 'Page shortcuts were looping in uids ' . implode(',', $pageLog) . '...!';
                GeneralUtility::sysLog($message, 'cms', GeneralUtility::SYSLOG_SEVERITY_ERROR);
                throw new \RuntimeException($message, 1294587212);
            }
        }

        return $page;
    }

    /**
     */
    public function checkPageForShortcutRedirect()
    {
        if (!empty($this->originalShortcutPage) && in_array($this->originalShortcutPage['doktype'], self::getShortcutDoktypes())) {
            $this->calculateLinkVars();

            $cObj = GeneralUtility::makeInstance('TYPO3\\CMS\\Frontend\\ContentObject\\ContentObjectRenderer');
            $parameter = $this->page['uid'];
            $type = GeneralUtility::_GET('type');
            if ($type) {
                $parameter .= ',' . $type;
            }
            $redirectUrl = $cObj->typoLink_URL(['parameter' => $parameter]);
            $redirectStatus = HttpUtility::HTTP_STATUS_301;
            if ($this->originalShortcutPage['shortcut_mode'] == PageRepository::SHORTCUT_MODE_RANDOM_SUBPAGE) {
                $redirectStatus = HttpUtility::HTTP_STATUS_307;
            }

            HttpUtility::redirect($redirectUrl, $redirectStatus);
        } elseif (in_array($this->page['doktype'], self::getFileDoktypes())) {
            $om = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
            /** @var FileRepository $fileRepository */
            $fileRepository = $om->get('TYPO3\CMS\Core\Resource\FileRepository');

            $file = null;
            if ($this->page['_PAGES_OVERLAY']) {
                $files = $fileRepository->findByRelation('pages_language_overlay', 'file', $this->page['_PAGES_OVERLAY_UID']);
                if (!$files || count($files) == 0) {
                    $files = $fileRepository->findByRelation('pages', 'file', $this->page['uid']);
                }
            } else {
                $files = $fileRepository->findByRelation('pages', 'file', $this->page['uid']);
            }

            if ($files && count($files) > 0) {
                $file = array_shift($files);
            }

            if ($file) {
                HttpUtility::redirect($file->getPublicUrl(), HttpUtility::HTTP_STATUS_301);
            }
        } else {
            parent::checkPageForShortcutRedirect();
        }

        $externalUrl = $this->sys_page->getExtURL($this->page);
        if ($externalUrl && $externalUrl != GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL')) {
            HttpUtility::redirect($externalUrl, HttpUtility::HTTP_STATUS_301);
        }
    }

    /**
     * @inhertitdoc
     */
    public function makeCacheHash()
    {
        // No need to test anything if caching was already disabled.
        if ($this->no_cache && !$GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFoundOnCHashError']) {
            return;
        }
        $GET = GeneralUtility::_GET();
        if ($this->cHash && is_array($GET)) {
            // Make sure we use the page uid and not the page alias
            $GET['id'] = $this->id;
            $this->cHash_array = $this->cacheHash->getRelevantParameters(GeneralUtility::implodeArrayForUrl('', $GET));
            $cHash_calc = $this->cacheHash->calculateCacheHash($this->cHash_array);
            if ($cHash_calc && $cHash_calc != $this->cHash) {
                if ($GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFoundOnCHashError']) {
                    $this->pageNotFoundAndExit('Request parameters could not be validated (&cHash comparison failed)');
                } else {
                    $this->disableCache();
                    $this->getTimeTracker()->setTSlogMessage('The incoming cHash "' . $this->cHash . '" and calculated cHash "' . $cHash_calc . '" did not match, so caching was disabled. The fieldlist used was "' . implode(',', array_keys($this->cHash_array)) . '"', 2);
                }
            }
        } elseif (is_array($GET)) {
            // No cHash is set, check if that is correct
            if ($this->cacheHash->doParametersRequireCacheHash(GeneralUtility::implodeArrayForUrl('', $GET))) {
                $this->reqCHash();
            }
        }
    }

    public function initUserGroups()
    {
        parent::initUserGroups();

        $this->callHook('initUserGroups', [$this]);
    }

    /**
     * @inheritdoc
     */
    public function isUserOrGroupSet()
    {
        return (is_array($this->fe_user->user) && isset($this->fe_user->user['uid'])) || $this->gr_list !== '0,-1';
    }

    /**
     * Initializes the page renderer object
     */
    protected function initPageRenderer()
    {
        if ($this->pageRenderer !== null) {
            return;
        }

        $this->pageRenderer = GeneralUtility::makeInstance('TYPO3\CMS\Core\Page\PageRenderer');
        $this->pageRenderer->setTemplateFile('EXT:frontend/Resources/Private/Templates/MainPage.html');
    }

    /**
     * Builds a typolink to the current page, appends the type paremeter if required
     * and redirects the user to the generated URL using a Location header.
     */
    protected function redirectToCurrentPage()
    {
        $this->calculateLinkVars();
        // Instantiate \TYPO3\CMS\Frontend\ContentObject to generate the correct target URL

        /** @var $cObj ContentObjectRenderer */
        $cObj = GeneralUtility::makeInstance(ContentObjectRenderer::class);
        $parameter = $this->page['uid'];
        $type = GeneralUtility::_GET('type');

        if ($type && MathUtility::canBeInterpretedAsInteger($type)) {
            $parameter .= ',' . $type;
        }

        $redirectUrl = $cObj->typoLink_URL([
            'parameter'      => $parameter,
            'addQueryString' => true,

            'addQueryString.' => ['exclude' => 'id'],
        ]);

        // Prevent redirection loop
        if (!empty($redirectUrl) && GeneralUtility::getIndpEnv('REQUEST_URI') !== '/' . $redirectUrl) {
            // redirect and exit
            HttpUtility::redirect($redirectUrl, HttpUtility::HTTP_STATUS_301);
        }
    }
}