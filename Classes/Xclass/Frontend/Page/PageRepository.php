<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\Xclass\Frontend\Page;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_pageteaser
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class PageRepository extends \TYPO3\CMS\Frontend\Page\PageRepository
{
    /**
     * @var array
     */
    protected static $externalDoktypes = [3, 103, 112, 113, 123, 133, 143, 153, 163, 173, 183, 193];

    /**
     * @return array
     */
    public static function getExternalDoktypes()
    {
        $externalDoktypes = self::$externalDoktypes;
        if (is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['tx_imiapageteaser']['Teaser'])) {
            foreach ($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['tx_imiapageteaser']['Teaser'] as $_classRef) {
                $hookObject = GeneralUtility::getUserObj($_classRef);
                if (method_exists($hookObject, 'getAdditionalExternal')) {
                    call_user_func_array([$hookObject, 'getAdditionalExternal'], [&$externalDoktypes]);
                }
            }
        }

        return $externalDoktypes;
    }

    /**
     * @param array $pagerow
     * @return string
     */
    public function getExtURL($pagerow)
    {
        if (in_array((int)$pagerow['doktype'], self::getExternalDoktypes())) {
            $redirectTo = $this->urltypes[$pagerow['urltype']] . $pagerow['url'];
            // If relative path, prefix Site URL:
            $uI = parse_url($redirectTo);
            // Relative path assumed now.
            if (!$uI['scheme'] && $redirectTo[0] !== '/') {
                $redirectTo = GeneralUtility::getIndpEnv('TYPO3_SITE_URL') . $redirectTo;
            }

            return $redirectTo;
        } else {
            return parent::getExtURL($pagerow);
        }
    }

    /**
     * @inheritdoc
     */
    public function enableFields($table, $show_hidden = -1, $ignore_array = [], $noVersionPreview = false)
    {
        if (TYPO3_MODE == 'BE') {
            return parent::deleteClause($table);
        } else {
            if ($show_hidden > 0 && in_array($table, ['pages', 'pages_language_overlay'])) {
                $ignore_array['starttime'] = true;
                $ignore_array['endtime'] = true;
            }
            if ($show_hidden === -1 && in_array($table, ['pages', 'pages_language_overlay']) && $this->getTypoScriptFrontendController()->showHiddenPage) {
                $ignore_array['starttime'] = true;
                $ignore_array['endtime'] = true;
            }

            return parent::enableFields($table, $show_hidden, $ignore_array, $noVersionPreview);
        }
    }
}