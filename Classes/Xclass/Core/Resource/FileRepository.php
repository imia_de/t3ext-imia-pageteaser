<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\Xclass\Core\Resource;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\MathUtility;

/**
 * @package     imia_pageteaser
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class FileRepository extends \TYPO3\CMS\Core\Resource\FileRepository
{
    /**
     * @param string $tableName
     * @param string $fieldName
     * @param integer $uid
     * @return array
     * @throws \InvalidArgumentException
     */
    public function findByRelation($tableName, $fieldName, $uid)
    {
        $itemList = [];

        if (!MathUtility::canBeInterpretedAsInteger($uid)) {
            throw new \InvalidArgumentException('Uid of related record has to be an integer.', 1316789798);
        }
        $considerWorkspaces = !empty($GLOBALS['BE_USER']->workspace) && BackendUtility::isTableWorkspaceEnabled($tableName) ? 1 : 0;

        if ($considerWorkspaces) {
            $rows = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                '*',
                $tableName,
                'uid=' . (int)$uid
            );
            $row = $rows[0];

            BackendUtility::workspaceOL($tableName, $row);
            if (isset($row['_ORIG_uid'])) {
                $uid = $row['_ORIG_uid'];
            }
        }

        $references = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
            '*',
            'sys_file_reference',
            'tablenames=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($tableName, 'sys_file_reference') .
            ' AND deleted = 0' .
            ' AND hidden = 0' .
            ' AND uid_foreign=' . (int)$uid .
            ' AND fieldname=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($fieldName, 'sys_file_reference'),
            '',
            'sorting_foreign'
        );

        foreach ($references as $referenceRecord) {
            if ($referenceRecord['t3ver_state'] == 0) {
                if ($considerWorkspaces) {
                    BackendUtility::workspaceOL('sys_file_reference', $referenceRecord);
                }

                $itemList[] = $this->createFileReferenceObject($referenceRecord);
            }
        }

        return $itemList;
    }
}