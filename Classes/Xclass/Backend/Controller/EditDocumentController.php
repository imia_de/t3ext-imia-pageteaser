<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaPageteaser\Xclass\Backend\Controller;

use IMIA\ImiaBaseExt\Traits\HookTrait;
use TYPO3\CMS\Backend\Controller\PageLayoutController;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Backend\View\PageLayoutView;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;

/**
 * @package     imia_pageteaser
 * @subpackage  Xclass
 * @author      David Frerich <d.frerich@imia.de>
 */
class EditDocumentController extends \TYPO3\CMS\Backend\Controller\EditDocumentController
{
    /**
     * @var array
     */
    protected $hookObjects;

    public function init()
    {
        parent::init();

        $this->moduleTemplate->getPageRenderer()->addCssInlineBlock('imia_pageteaser', '
            .x-tree .x-panel-body {
                background-color: transparent !important;
            }
            .typo3-tceforms-tree + div .x-panel,
            .typo3-tceforms-tree + div .x-panel-tbar,
            .typo3-tceforms-tree + div .x-panel-body {
                width: auto !important;
            }
            .typo3-tceforms-tree + div .x-panel {
                border: 2px solid #cccccc;
                background-color: #ffffff;
                border-radius: 2px;
                box-shadow: 0px 2px 0px rgba(0,0,0,0.1);
            }
            .typo3-tceforms-tree + div .x-panel-tbar {
                background-color: #EDEDED;
                border-bottom: 1px solid #cccccc;
                height: 38px;
                line-height: 0;
                padding: 6px;
            }
            .typo3-tceforms-tree + div .x-panel-body {
                padding: 6px;
            }
            .typo3-tceforms-tree + div .x-tree-node-over {
                background-color: transparent !important;
            }
            .typo3-tceforms-tree + div .x-tree-node-el {
                cursor: default;
            }
            '
        );
    }

    /**
     * @inheritdoc
     */
    public function languageSwitch($table, $uid, $pid = null)
    {
        if (is_numeric($uid) && $this->getBackendUser()->check('tables_modify', $table)) {
            $languageField = $GLOBALS['TCA'][$table]['ctrl']['languageField'];

            $transForeignTable = null;
            $transOrigPointerTable = null;
            $transOrigPointerField = null;
            if ($table == 'pages_language_overlay') {
                $transOrigPointerTable = 'pages';
                $transOrigPointerField = 'pid';
            } elseif ($table == 'pages') {
                $transForeignTable = 'pages_language_overlay';
            }

            $linkParams = [
                'returnUrl' => $this->retUrl,
            ];
            if (GeneralUtility::_GP('columnsOnly')) {
                $linkParams['columnsOnly'] = GeneralUtility::_GP('columnsOnly');
            }
            if (GeneralUtility::_GP('columnsOnlyTS')) {
                $linkParams['columnsOnlyTS'] = GeneralUtility::_GP('columnsOnlyTS');
            }
            if (GeneralUtility::_GP('pagelayout')) {
                $linkParams['pagelayout'] = GeneralUtility::_GP('pagelayout');
            }

            if ($transForeignTable && $this->getBackendUser()->check('tables_modify', $transForeignTable)) {
                $transForeignConfig = $GLOBALS['TCA'][$transForeignTable]['ctrl'];
                $transForeignLanguageField = $transForeignConfig['languageField'];

                if ($transForeignTable == 'pages_language_overlay') {
                    $pid = (int)$uid;
                } elseif (is_null($pid)) {
                    $row = BackendUtility::getRecord($table, $uid, 'pid');
                    $pid = (int)$row['pid'];
                }

                $langRows = $this->getDb()->exec_SELECTgetRows($transForeignTable . '.uid AS foreign_uid,' .
                    $transForeignTable . '.' . $transForeignLanguageField . ', sys_language.title, sys_language.uid',
                    $transForeignTable . ' LEFT JOIN sys_language ON sys_language.uid = ' . $transForeignTable . '.' . $transForeignLanguageField,
                    $transForeignTable . '.pid = ' . $pid . ' AND ' . $transForeignTable . '.deleted = 0');

                $this->callHook('getLanguages', [&$langRows, $pid, $this]);

                if ($langRows > 0) {
                    $languageMenu = $this->moduleTemplate->getDocHeaderComponent()->getMenuRegistry()->makeMenu();
                    $languageMenu->setIdentifier('_langSelector');
                    $languageMenu->setLabel($this->getLanguageService()->sL(
                        'LLL:EXT:lang/locallang_general.xlf:LGL.language',
                        true
                    ));

                    $tsConfig = BackendUtility::getPagesTSconfig($pid);
                    if (isset($tsConfig['mod.']['SHARED.']['defaultLanguageLabel'])) {
                        $defaultLabel = $tsConfig['mod.']['SHARED.']['defaultLanguageLabel'];
                    } else {
                        $defaultLabel = $this->translate('backend_language_default');
                    }

                    if ($this->getBackendUser()->checkLanguageAccess(0)) {
                        $href = BackendUtility::getModuleUrl('record_edit', array_merge($linkParams, [
                            'edit[' . $table . '][' . $uid . ']' => 'edit',
                        ]));

                        $menuItem = $languageMenu->makeMenuItem()
                            ->setTitle($defaultLabel . ' (' . $this->getLanguageService()->sL('LLL:EXT:lang/locallang_mod_web_list.xlf:defaultLanguage') . ')')
                            ->setHref($href)
                            ->setActive(true);
                        $languageMenu->addMenuItem($menuItem);
                    }

                    foreach ($langRows as $langRow) {
                        if ($this->getBackendUser()->checkLanguageAccess($langRow[$transForeignLanguageField])) {
                            $href = BackendUtility::getModuleUrl('record_edit', array_merge($linkParams, [
                                'edit[' . $transForeignTable . '][' . $langRow['foreign_uid'] . ']' => 'edit',
                            ]));

                            $menuItem = $languageMenu->makeMenuItem()
                                ->setTitle($langRow['title'])
                                ->setHref($href);
                            $languageMenu->addMenuItem($menuItem);
                        }
                    }
                    $this->callHook('languageMenu', [&$languageMenu, $langRows, $table, $uid, $pid, $this]);

                    $this->moduleTemplate->getDocHeaderComponent()->getMenuRegistry()->addMenu($languageMenu);
                }
            } elseif (is_numeric($uid) && $transOrigPointerTable && $transOrigPointerField && $this->getBackendUser()->check('tables_modify', $transOrigPointerTable)) {
                $row = BackendUtility::getRecord($table, $uid, $transOrigPointerField);
                $pid = $row['pid'];

                $langRows = $this->getDb()->exec_SELECTgetRows($table . '.uid AS foreign_uid, sys_language.uid,' .
                    $table . '.' . $languageField . ', sys_language.title',
                    $table . ' LEFT JOIN sys_language ON sys_language.uid = ' . $table . '.' . $languageField,
                    $table . '.pid = ' . $pid . ' AND ' . $table . '.deleted = 0');

                $this->callHook('getLanguages', [&$langRows, $pid, $this]);

                $languageMenu = $this->moduleTemplate->getDocHeaderComponent()->getMenuRegistry()->makeMenu();
                $languageMenu->setIdentifier('_langSelector');
                $languageMenu->setLabel($this->getLanguageService()->sL(
                    'LLL:EXT:lang/locallang_general.xlf:LGL.language',
                    true
                ));

                $tsConfig = BackendUtility::getPagesTSconfig($pid);
                if (isset($tsConfig['mod.']['SHARED.']['defaultLanguageLabel'])) {
                    $defaultLabel = $tsConfig['mod.']['SHARED.']['defaultLanguageLabel'];
                } else {
                    $defaultLabel = $this->translate('backend_language_default');
                }

                if ($this->getBackendUser()->checkLanguageAccess(0)) {
                    $transRecord = $this->getDb()->exec_SELECTgetSingleRow('uid',
                        $transOrigPointerTable, 'uid = ' . (int)$row[$transOrigPointerField]);

                    if ($transRecord) {
                        $href = BackendUtility::getModuleUrl('record_edit', array_merge($linkParams, [
                            'edit[' . $transOrigPointerTable . '][' . $transRecord['uid'] . ']' => 'edit',
                        ]));
                        $menuItem = $languageMenu->makeMenuItem()
                            ->setTitle($defaultLabel . ' (' . $this->getLanguageService()->sL('LLL:EXT:lang/locallang_mod_web_list.xlf:defaultLanguage') . ')')
                            ->setHref($href);
                        $languageMenu->addMenuItem($menuItem);
                    }
                }

                if (is_array($langRows)) {
                    foreach ($langRows as $langRow) {
                        if ($this->getBackendUser()->checkLanguageAccess($langRow[$languageField])) {
                            $href = BackendUtility::getModuleUrl('record_edit', array_merge($linkParams, [
                                'edit[' . $table . '][' . $langRow['foreign_uid'] . ']' => 'edit',
                            ]));

                            $menuItem = $languageMenu->makeMenuItem()
                                ->setTitle($langRow['title'])
                                ->setHref($href);
                            if ($langRow['foreign_uid'] == $uid) {
                                $menuItem->setActive(true);
                            }
                            $languageMenu->addMenuItem($menuItem);
                        }
                    }
                }
                $this->callHook('languageMenu', [&$languageMenu, $langRows, $table, $uid, $pid, $this]);

                $this->moduleTemplate->getDocHeaderComponent()->getMenuRegistry()->addMenu($languageMenu);
            } else {
                parent::languageSwitch($table, $uid, $pid);

                $languageMenu = $this->moduleTemplate->getDocHeaderComponent()->getMenuRegistry()->getMenus()['_langSelector'];
                $this->callHook('languageMenu', [&$languageMenu, null, $table, $uid, $pid, $this]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getLanguages($id)
    {
        $languages = parent::getLanguages($id);
        $this->callHook('getLanguages', [&$languages, $id, $this]);

        return $languages;
    }

    /**
     * @param string $key
     * @return string
     */
    protected function translate($key)
    {
        return $this->getLanguageService()->sL('LLL:EXT:imia_pageteaser/Resources/Private/Language/locallang.xlf:' . $key);
    }

    /**
     * @return \TYPO3\CMS\Backend\Template\ModuleTemplate
     */
    public function getModuleTemplate()
    {
        return $this->moduleTemplate;
    }

    /**
     * @param string $form
     * @return string
     */
    public function compileForm($form)
    {
        $content = parent::compileForm($form);

        $edit = GeneralUtility::_GP('edit');
        $table = $record = null;
        if ($edit && is_array($edit)) {
            $table = array_shift(array_keys($edit));

            $editInfo = array_shift($edit);
            $uid = array_shift(array_keys($editInfo));
            $action = array_shift($editInfo);

            if ($uid && $action != 'new') {
                $record = BackendUtility::getRecord($table, $uid);
            }
        }

        if ($record && in_array($table, ['pages', 'pages_language_overlay']) && is_numeric($record['uid'])) {
            $pageLayoutEnabled = false;

            if (GeneralUtility::_GP('pagelayout')) {
                $pageLayoutEnabled = true;
            } else {
                $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['imia_pageteaser']);
                if ($extConf['pagelayoutForced']) {
                    $pageLayoutEnabled = true;
                } else {
                    $pageTreeHiddenDoktypes = array_map('intval', explode(',', $extConf['doktypesPageTreeHidden']));
                    if (!is_array($GLOBALS['pageTreeHiddenDoktypes'])) {
                        $GLOBALS['pageTreeHiddenDoktypes'] = [];
                    }
                    if (in_array($record['doktype'], $pageTreeHiddenDoktypes) || in_array($record['doktype'], $GLOBALS['pageTreeHiddenDoktypes'])) {
                        $_GET['pagelayout'] = $_REQUEST['pagelayout'] = 1;
                        $pageLayoutEnabled = true;
                    }
                }
            }


            if ($GLOBALS['BE_USER']->getTSConfigVal('options.pageteaser.pageLayout.disabled')) {
                $pageLayoutEnabled = false;
            }

            if ($pageLayoutEnabled) {
                $id = $table == 'pages' ? $record['uid'] : $record['pid'];

                if (ExtensionManagementUtility::isLoaded('templavoila')) {
                    $pagelayout = '';
                } else {
                    $tmpSOBE = $GLOBALS['SOBE'];

                    $_REQUEST['id'] = $_GET['id'] = $id;

                    /** @var PageLayoutController $pageLayoutController */
                    $pageLayoutController = GeneralUtility::makeInstance(PageLayoutController::class);
                    $GLOBALS['SOBE'] = $pageLayoutController;

                    $pageLayoutController->init();
                    $pageLayoutController->MOD_SETTINGS['function'] = 1;
                    $pageLayoutController->MOD_SETTINGS['language'] = $record['sys_language_uid'] ?: '0';
                    $pageLayoutController->current_sys_language = $record['sys_language_uid'] ?: '0';
                    $pageLayoutController->main();

                    $pagelayout = $pageLayoutController->renderContent();

                    if ($table == 'pages_language_overlay') {
                        /** @var $dbList \TYPO3\CMS\Backend\View\PageLayoutView */
                        $dbList = GeneralUtility::makeInstance(PageLayoutView::class);
                        $dbList->thumbs = $pageLayoutController->imagemode;
                        $dbList->no_noWrap = 1;
                        $dbList->descrTable = $pageLayoutController->descrTable;
                        $dbList->showIcon = 0;
                        $dbList->setLMargin = 0;
                        $dbList->doEdit = $pageLayoutController->EDIT_CONTENT;
                        $dbList->ext_CALC_PERMS = $pageLayoutController->CALC_PERMS;
                        $dbList->agePrefixes = $this->getLanguageService()->sL('LLL:EXT:lang/locallang_core.xlf:labels.minutesHoursDaysYears');
                        $dbList->id = $pageLayoutController->id;
                        $dbList->nextThree = MathUtility::forceIntegerInRange($pageLayoutController->modTSconfig['properties']['editFieldsAtATime'], 0, 10);
                        $dbList->option_newWizard = $pageLayoutController->modTSconfig['properties']['disableNewContentElementWizard'] ? 0 : 1;
                        $dbList->defLangBinding = $pageLayoutController->modTSconfig['properties']['defLangBinding'] ? 1 : 0;

                        $defaultLanguageContentUids = [];
                        $result = $this->getDb()->exec_SELECTquery('uid', 'tt_content', 'pid = ' . $record['pid'] . ' AND (l18n_parent = 0 OR l18n_parent IS NULL)');
                        while ($row = $this->getDb()->sql_fetch_assoc($result)) {
                            $defaultLanguageContentUids[] = $row['uid'];
                        }

                        if (preg_match_all('/<td.*?data-colpos="([0-9]+)".*?<div class="t3-page-column-header-label">/ism', $pagelayout, $matches)) {
                            foreach ($matches[1] as $key => $colPos) {
                                $translateButton = $dbList->newLanguageButton(
                                    $dbList->getNonTranslatedTTcontentUids($defaultLanguageContentUids, $record['pid'], $record['sys_language_uid']),
                                    $record['sys_language_uid'],
                                    $colPos
                                );

                                $pagelayout = str_replace($matches[0][$key], $matches[0][$key] . '<div style="float: right">' . $translateButton . '</div>', $pagelayout);
                            }
                        }
                    }

                    $GLOBALS['SOBE'] = $tmpSOBE;
                }

                $content .= $pagelayout . '<style type="text/css">body #typo3-inner-docbody > div {top: 20px;}' .
                    '.t3-gridContainer {margin: 0 -12px !important;} .t3-gridContainer > table {margin: 0} .t3-page-lang-copyce { ' .
                    'margin: 0; top: -12px; right: -10px; position: relative }</style>';

                $tsConfig = BackendUtility::getPagesTSconfig($id);
                if (isset($tsConfig['mod.']['SHARED.']['defaultLanguageLabel'])) {
                    $defaultLabel = $tsConfig['mod.']['SHARED.']['defaultLanguageLabel'];
                } else {
                    $defaultLabel = $this->getLanguageService()->sL('LLL:EXT:imia_pageteaser/Resources/Private/Language/locallang.xlf:backend_language_default');
                }

                $page = $this->getDb()->exec_SELECTgetSingleRow('uid, l18n_cfg', 'pages', 'uid = ' . $id . ' AND deleted = 0');
                $disabled = '';
                if (in_array((int)$page['l18n_cfg'], [1, 3])) {
                    $disabled = ' ' . $this->getLanguageService()->sL('LLL:EXT:imia_pageteaser/Resources/Private/Language/locallang.xlf:backend_language_disabled');
                }

                $languages = [
                    0 => [
                        'uid'   => $id,
                        'label' => $defaultLabel . $disabled,
                        'table' => 'pages',
                    ],
                ];

                $result = $this->getDb()->exec_SELECTquery('uid, title', 'sys_language', '1=1');
                while ($row = $this->getDb()->sql_fetch_assoc($result)) {
                    $pageOverlay = $this->getDb()->exec_SELECTgetSingleRow('uid', 'pages_language_overlay', 'pid = ' .
                        $id . ' AND sys_language_uid = ' . $row['uid'] . ' AND deleted = 0');

                    $languages[$row['uid']] = [
                        'uid'   => $pageOverlay ? $pageOverlay['uid'] : null,
                        'label' => $row['title'],
                        'table' => 'pages_language_overlay',
                    ];
                }

                $returnUrl = GeneralUtility::_GP('returnUrl');
                $columnsOnly = GeneralUtility::_GP('columnsOnly');
                $columnsOnlyAlt = GeneralUtility::_GP('columnsOnlyAlt');
                $columnsOnlyTS = GeneralUtility::_GP('columnsOnlyTS');

                $languageOptions = '';
                foreach ($languages as $langUid => $language) {
                    $params = [];

                    if ($language['uid']) {
                        $params[] = 'edit[' . $language['table'] . '][' . $language['uid'] . ']=edit';
                    } else {
                        $params[] = 'edit[' . $language['table'] . '][' . $id . ']=new&overrideVals[pages_language_overlay][doktype]=' .
                            $record['doktype'] . '&overrideVals[pages_language_overlay][sys_language_uid]=' . $langUid;

                        $language['label'] .= ' ' . $this->getLanguageService()->sL('LLL:EXT:imia_pageteaser/Resources/Private/Language/locallang.xlf:backend_language_new');
                    }

                    if ($columnsOnly) {
                        $params[] = 'columnsOnly=' . urlencode($columnsOnly);
                    }
                    if ($columnsOnlyAlt) {
                        $params[] = 'columnsOnlyAlt=' . urlencode($columnsOnlyAlt);
                    }
                    if ($columnsOnlyTS) {
                        $params[] = 'columnsOnlyTS=' . urlencode($columnsOnlyTS);
                    }
                    if ($returnUrl) {
                        $params[] = 'returnUrl=' . urlencode($returnUrl);
                    }
                    $params[] = 'pagelayout=1';

                    $selected = '';
                    if ($language['table'] == $table && $language['uid'] == $record['uid']) {
                        $selected = ' selected = "selected"';
                    }

                    $languageOptions .= '<option' . $selected . ' value="alt_doc.php?' . implode('&', $params) . '">' . $language['label'] . '</option>';
                }

                $parts[0] = '
                    <div style="float:right">
                        <label for="reclang">' . $GLOBALS['LANG']->sL('EXT:lang/locallang_general.xlf:LGL.language') . '</label>
                        <select id="reclang" name="reclang" onchange="location.href=this.value">' . $languageOptions . '</select>
                    </div>
                    <h1>' . $record['title'] . '&nbsp;</h1>
                    <div class="typo3-TCEforms">';

                if (ExtensionManagementUtility::isLoaded('imia_base')) {
                    $parts[1] .= \IMIA\ImiaBase\Xclass\Backend\View\PageLayoutView::getAdditionalStyles();
                }
            }
        }

        return $content;
    }

    /**
     * @inheritdoc
     */
    protected function getButtons()
    {
        $this->callHook('getButtons', [&$this]);

        return parent::getButtons();
    }

    /**
     * @param string $method
     * @param array $args
     */
    protected function callHook($method, $args)
    {
        foreach ($this->getHookObjects() as $hookObject) {
            if (method_exists($hookObject, $method)) {
                call_user_func_array([$hookObject, $method], $args);
            }
        }
    }

    /**
     * @return array
     */
    protected function getHookObjects()
    {
        if (!$this->hookObjects) {
            $this->hookObjects = [];
            $className = get_class($this);
            $className = substr($className, strrpos($className, '\\') + 1);
            $extKey = 'tx_imiapageteaser';

            if (is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF'][$extKey][$className])) {
                foreach ($GLOBALS['TYPO3_CONF_VARS']['EXTCONF'][$extKey][$className] as $_classRef) {
                    $this->hookObjects[] = GeneralUtility::getUserObj($_classRef);
                }
            }
        }

        return $this->hookObjects;
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDb()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}