<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'IMIA.' . $_EXTKEY,
    'teaser',
    [
        'Teaser' => 'index',
    ],
    [
        'Teaser' => 'index',
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'IMIA.' . $_EXTKEY,
    'teaser_cached',
    [
        'Teaser' => 'index',
    ],
    [
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'IMIA.' . $_EXTKEY,
    'teaser_navigation',
    [
        'Teaser' => 'navigation',
    ],
    [
        'Teaser' => 'navigation',
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'IMIA.' . $_EXTKEY,
    'teaser_categories',
    [
        'Teaser' => 'categories',
    ],
    [
        'Teaser' => 'categories',
    ]
);

/* Hooks */
$GLOBALS ['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processCmdmapClass'][$_EXTKEY] =
    \IMIA\ImiaPageteaser\Hook\Core\ProcessCmdmap::class;

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][$_EXTKEY] =
    \IMIA\ImiaPageteaser\Hook\Core\ProcessDatamap::class;

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['extTablesInclusion-PostProcessing'][$_EXTKEY] =
    \IMIA\ImiaPageteaser\Hook\Core\TableConfigurationPostProcessing::class;

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['extTablesInclusion-PostProcessingCached'][$_EXTKEY] =
    \IMIA\ImiaPageteaser\Hook\Core\TableConfigurationPostProcessingCached::class;

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['urlProcessing']['urlHandlers'][$_EXTKEY]['handler'] =
    \IMIA\ImiaPageteaser\Hook\Frontend\UrlHandler::class;

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['configArrayPostProc'][$_EXTKEY] =
    \IMIA\ImiaPageteaser\Hook\Frontend\TypoScriptFrontendController::class . '->configArrayPostProc';

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['get_cache_timeout'][$_EXTKEY] =
    \IMIA\ImiaPageteaser\Hook\Frontend\TypoScriptFrontendController::class . '->getCacheTimeout';

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['contentPostProc-all'][$_EXTKEY] =
    \IMIA\ImiaPageteaser\Hook\Frontend\TypoScriptFrontendController::class . '->contentPostProc';

/* Xclasses */
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['tx_templavoila_mod1_specialdoktypes'] = [
    'className' => \IMIA\ImiaPageteaser\Xclass\Templavoila\SpecialDoktypes::class,
];
if ((float)TYPO3_version < 7.0) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Core\Resource\FileRepository::class] = [
        'className' => \IMIA\ImiaPageteaser\Xclass\Core\Resource\FileRepository::class,
    ];
}
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController::class] = [
    'className' => \IMIA\ImiaPageteaser\Xclass\Frontend\Controller\TypoScriptFrontendController::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Frontend\Page\PageRepository::class] = [
    'className' => \IMIA\ImiaPageteaser\Xclass\Frontend\Page\PageRepository::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Controller\EditDocumentController::class] = [
    'className' => \IMIA\ImiaPageteaser\Xclass\Backend\Controller\EditDocumentController::class,
];

$disableTypoScript = '';
$doktypes = [100 => 'news', 110 => 'gallery', 120 => 'product', 130 => 'event', 140 => 'article'];
$extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY]);
foreach ($doktypes as $doktype => $doktypeName) {
    if (!in_array($doktype, explode(',', $extConf['doktypes']))) {
        $disableTypoScript .= "\n" . 'plugin.tx_imiapageteaser.flexforms.doktypes.' . $doktypeName . ' > ';
    }
}
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript($_EXTKEY, 'setup',
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TypoScript/setup.typoscript">' . $disableTypoScript);