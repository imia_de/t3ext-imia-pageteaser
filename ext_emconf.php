<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title'            => 'IMIA Pageteaser',
    'description'      => 'Plugin for List-View and Navigation for Pages. Custom doktypes for News, Galleries, Products, Events and Articles',
    'category'         => 'plugin',
    'author'           => 'David Frerich',
    'author_email'     => 'd.frerich@imia.de',
    'author_company'   => 'IMIA net based solutions',
    'state'            => 'stable',
    'uploadfolder'     => 0,
    'clearCacheOnLoad' => 1,
    'version'          => '8.2.0',
    'constraints'      => [
        'depends'   => [
            'php'           => '7.0.0-0.0.0',
            'typo3'         => '8.7.0-8.7.999',
            'imia_base_ext' => '8.0.0-0.0.0',
        ],
        'conflicts' => [
        ],
        'suggests'  => [
        ],
    ],
];