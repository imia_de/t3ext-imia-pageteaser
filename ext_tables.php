<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'IMIA.' . $_EXTKEY,
    'teaser',
    'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/Plugin.xlf:teaser'
);
\IMIA\ImiaBaseExt\Utility\ExtensionUtility::registerPluginFlexform($_EXTKEY, 'teaser', 'Configuration/FlexForms/Teaser.xml');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'IMIA.' . $_EXTKEY,
    'teaser_cached',
    'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/Plugin.xlf:teaser_cached'
);
\IMIA\ImiaBaseExt\Utility\ExtensionUtility::registerPluginFlexform($_EXTKEY, 'teaser_cached', 'Configuration/FlexForms/TeaserCached.xml');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'IMIA.' . $_EXTKEY,
    'teaser_categories',
    'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/Plugin.xlf:teaser_categories'
);
\IMIA\ImiaBaseExt\Utility\ExtensionUtility::registerPluginFlexform($_EXTKEY, 'teaser_categories', 'Configuration/FlexForms/TeaserCategories.xml');

if (TYPO3_MODE === 'BE') {
    $initModuleGroup = false;
    $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY]);
    for ($i = 1; $i <= 5; $i++) {
        if ($extConf['module' . $i . 'Enable']) {
            switch ($extConf['module' . $i . 'Icon']) {
                case 110:
                    $icon = 'PageDoktypeGallery';
                    break;
                case 120:
                    $icon = 'PageDoktypeProduct';
                    break;
                case 130:
                    $icon = 'PageDoktypeEvent';
                    break;
                case 140:
                    $icon = 'PageDoktypeArticle';
                    break;
                case 100:
                default:
                    $icon = 'PageDoktypeNews';
                    break;
            }

            $langFile = 'typo3temp/pageteaser_module' . $i . '.xlf';
            $langContents = '<xliff version="1.0">
    <file source-language="en" datatype="plaintext" original="messages" date="2015-03-30T18:00:00Z" product-name="imia_pageteaser">
        <header/>
        <body>
            <trans-unit id="mlang_tabs_tab">
                <source><![CDATA[' . $extConf['module' . $i . 'Title'] . ']]></source>
            </trans-unit>
            <trans-unit id="mlang_labels_tabdescr">
                <source><![CDATA[' . $extConf['module' . $i . 'Description'] . ']]></source>
            </trans-unit>
            <trans-unit id="mlang_labels_tablabel">
                <source><![CDATA[' . $extConf['module' . $i . 'Description'] . ']]></source>
            </trans-unit>
        </body>
    </file>
</xliff>';

            @file_put_contents(PATH_site . $langFile, $langContents);

            if (!$initModuleGroup && $extConf['module' . $i . 'Pid']) {
                \IMIA\ImiaBaseExt\Utility\ExtensionUtility::registerModuleGroup($_EXTKEY, 'pageteaser', 'Configuration/Module/', 'web');
                $initModuleGroup = true;
            }

            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
                'IMIA.' . $_EXTKEY,
                $extConf['module' . $i . 'Pid'] ? 'pageteaser' : 'web',
                'pageteaser_module' . $i,
                '',
                [
                    'Backend' => 'index',
                ],
                [
                    'access' => 'user,group',
                    'icon'   => 'EXT:' . $_EXTKEY . '/Resources/Public/Images/Icons/' . $icon . '.svg',
                    'labels' => 'LLL:' . $langFile,
                ]
            );
        }
    }
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TSConfig/page.typoscript">');