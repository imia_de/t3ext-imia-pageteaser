(function($){
    $(document).ready(function() {
        initFilter();
    });

    function initFilter() {
        var filter = $('#filter');
        var form = filter.find('form');
        filter.children('button').click(function(e){
            e.preventDefault();

            filter.toggleClass('active');
            form.slideToggle();

            return false;
        });
    }
})(TYPO3.jQuery);