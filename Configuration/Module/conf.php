<?php

define('TYPO3_MOD_PATH', '../typo3conf/ext/imia_pageteaser/Configuration/Module/');
$BACK_PATH = '../../../../typo3/';

$MCONF['name'] = 'pageteaser';
$MCONF['access'] = 'user,group';
$MLANG['default']['ll_ref']= 'LLL:EXT:imia_pageteaser/Resources/Private/Language/ModuleBackend.xlf';

$MLANG['icon'] = 'EXT:imia_pageteaser/Resources/Public/Images/Icons/Backend.svg';
