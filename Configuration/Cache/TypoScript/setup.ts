### EXTENSION: imia_pageteaser ###

config.tx_extbase {
    persistence {
        classes {
            IMIA\ImiaPageteaser\Domain\Model\BackendUser {
                mapping {
                    tableName = be_users
                    columns {
                        username.mapOnProperty = userName
                        admin.mapOnProperty = isAdministrator
                        disable.mapOnProperty = isDisabled
                        starttime.mapOnProperty = startDateAndTime
                        endtime.mapOnProperty = endDateAndTime
                        realName.mapOnProperty = realName
                        lastlogin.mapOnProperty = lastLoginDateAndTime
                        disableIPlock.mapOnProperty = ipLockIsDisabled
                    }
                }
            }
            IMIA\ImiaPageteaser\Domain\Model\Category {
                mapping {
                    tableName = sys_category
                    columns {
                        sorting.mapOnProperty = sorting
                    }
                }
            }
            IMIA\ImiaPageteaser\Domain\Model\Page {
                mapping {
                    tableName = pages
                    columns {
                        lastUpdated.mapOnProperty = lastUpdated
                        sorting.mapOnProperty = sorting
                        cruser_id.mapOnProperty = cruser
                    }
                }
            }
            IMIA\ImiaPageteaser\Domain\Model\PageAlt {
                mapping {
                    tableName = pages_language_overlay
                    columns {
                        pid.mapOnProperty = page
                    }
                }
            }
        }
    }

}

plugin.tx_imiapageteaser {
    persistence {
        classes {
            IMIA\ImiaPageteaser\Domain\Model\BackendUser {
                mapping {
                    tableName = be_users
                    columns {
                        username.mapOnProperty = userName
                        admin.mapOnProperty = isAdministrator
                        disable.mapOnProperty = isDisabled
                        starttime.mapOnProperty = startDateAndTime
                        endtime.mapOnProperty = endDateAndTime
                        realName.mapOnProperty = realName
                        lastlogin.mapOnProperty = lastLoginDateAndTime
                        disableIPlock.mapOnProperty = ipLockIsDisabled
                    }
                }
            }
            IMIA\ImiaPageteaser\Domain\Model\Category {
                mapping {
                    tableName = sys_category
                    columns {
                        sorting.mapOnProperty = sorting
                    }
                }
            }
            IMIA\ImiaPageteaser\Domain\Model\Page {
                mapping {
                    tableName = pages
                    columns {
                        lastUpdated.mapOnProperty = lastUpdated
                        sorting.mapOnProperty = sorting
                        cruser_id.mapOnProperty = cruser
                    }
                }
            }
            IMIA\ImiaPageteaser\Domain\Model\PageAlt {
                mapping {
                    tableName = pages_language_overlay
                    columns {
                        pid.mapOnProperty = page
                    }
                }
            }
        }
    }

}

module.tx_imiapageteaser {
    persistence {
        classes {
            IMIA\ImiaPageteaser\Domain\Model\BackendUser {
                mapping {
                    tableName = be_users
                    columns {
                        username.mapOnProperty = userName
                        admin.mapOnProperty = isAdministrator
                        disable.mapOnProperty = isDisabled
                        starttime.mapOnProperty = startDateAndTime
                        endtime.mapOnProperty = endDateAndTime
                        realName.mapOnProperty = realName
                        lastlogin.mapOnProperty = lastLoginDateAndTime
                        disableIPlock.mapOnProperty = ipLockIsDisabled
                    }
                }
            }
            IMIA\ImiaPageteaser\Domain\Model\Category {
                mapping {
                    tableName = sys_category
                    columns {
                        sorting.mapOnProperty = sorting
                    }
                }
            }
            IMIA\ImiaPageteaser\Domain\Model\Page {
                mapping {
                    tableName = pages
                    columns {
                        lastUpdated.mapOnProperty = lastUpdated
                        sorting.mapOnProperty = sorting
                        cruser_id.mapOnProperty = cruser
                    }
                }
            }
            IMIA\ImiaPageteaser\Domain\Model\PageAlt {
                mapping {
                    tableName = pages_language_overlay
                    columns {
                        pid.mapOnProperty = page
                    }
                }
            }
        }
    }

}

