<?php
if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}

$TCA['sys_category'] = [
    'ctrl' => $TCA['sys_category']['ctrl'],
    'interface' => [
        'showRecordFieldList' => 'children',
    ],
    'types' => [
        0 => [
            'showitem' => 'children',
        ],
    ],
    'palettes' => [
    ],
    'columns' => [
        'children' => [
            'label' => '[category_children]',
            'l10n_mode' => 'exclude',
            'config' => [
                'foreign_table' => 'sys_category',
                'appearance' => [
                    'collapseAll' => true,
                    'expandSingle' => true,
                ],
                'foreign_field' => 'parent',
                'maxitems' => 9999,
                'type' => 'inline',
            ],
        ],
        'items' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
   ],
];