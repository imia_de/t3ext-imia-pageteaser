<?php
if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}

$TCA['be_users'] = [
    'ctrl' => $TCA['be_users']['ctrl'],
    'interface' => [
        'showRecordFieldList' => '',
    ],
    'types' => [
        0 => [
            'showitem' => '',
        ],
    ],
    'palettes' => [
    ],
    'columns' => [
   ],
];