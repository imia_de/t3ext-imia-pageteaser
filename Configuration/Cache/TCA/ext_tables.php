<?php
if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}


// ******************************************************************
// TCA for IMIA\ImiaPageteaser\Domain\Model\BackendUser
// (be_users)
// ******************************************************************
if (array_key_exists('be_users', $GLOBALS['TCA'])) {
    $GLOBALS['TCA']['be_users']['ctrl']['label'] = 'realName';
    $GLOBALS['TCA']['be_users']['ctrl']['label_alt'] = 'username';
    $GLOBALS['TCA']['be_users']['ctrl']['label_alt_force'] = true;
    $GLOBALS['TCA']['be_users']['ctrl']['hideAtCopy'] = true;
    $GLOBALS['TCA']['be_users']['ctrl']['prependAtCopy'] = '';
    $GLOBALS['TCA']['be_users']['columns']['username']['mapOnProperty'] = "userName";
    $GLOBALS['TCA']['be_users']['columns']['admin']['mapOnProperty'] = "isAdministrator";
    $GLOBALS['TCA']['be_users']['columns']['disable']['mapOnProperty'] = "isDisabled";
    $GLOBALS['TCA']['be_users']['columns']['starttime']['mapOnProperty'] = "startDateAndTime";
    $GLOBALS['TCA']['be_users']['columns']['endtime']['mapOnProperty'] = "endDateAndTime";
    $GLOBALS['TCA']['be_users']['columns']['realName']['mapOnProperty'] = "realName";
    $GLOBALS['TCA']['be_users']['columns']['lastlogin']['mapOnProperty'] = "lastLoginDateAndTime";
    $GLOBALS['TCA']['be_users']['columns']['disableIPlock']['mapOnProperty'] = "ipLockIsDisabled";
} else {
    $GLOBALS['TCA']['be_users'] = [
        'ctrl' => [
            'title' => '[beusers]',
            'label' => 'realName',
            'label_alt' => 'username',
            'label_alt_force' => true,
            'dividers2tabs' => true,
            'hideAtCopy' => true,
            'prependAtCopy' => '',
            'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('imia_pageteaser', 'Configuration/Cache/TCA/BackendUser.php'),
        ]
    ];
}
// ******************************************************************
// TCA for IMIA\ImiaPageteaser\Domain\Model\Category
// (sys_category)
// ******************************************************************
if (array_key_exists('sys_category', $GLOBALS['TCA'])) {
    $GLOBALS['TCA']['sys_category']['ctrl']['sortby'] = 'sorting';
    $GLOBALS['TCA']['sys_category']['ctrl']['default_sortby'] = 'ORDER BY sorting ASC';
    $GLOBALS['TCA']['sys_category']['ctrl']['hideAtCopy'] = true;
    $GLOBALS['TCA']['sys_category']['ctrl']['prependAtCopy'] = '';
    if (array_key_exists('children', $GLOBALS['TCA']['sys_category']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['sys_category']['columns']['children'])) {
            $GLOBALS['TCA']['sys_category']['columns']['children']['config']['foreign_table'] = 'sys_category';
            $GLOBALS['TCA']['sys_category']['columns']['children']['config']['foreign_field'] = 'parent';
            $GLOBALS['TCA']['sys_category']['columns']['children']['config']['maxitems'] = 9999;
            $GLOBALS['TCA']['sys_category']['columns']['children']['config']['type'] = 'inline';
        } else {
            $GLOBALS['TCA']['sys_category']['columns']['children']['config'] = [
                'foreign_table' => 'sys_category',
                'foreign_field' => 'parent',
                'maxitems' => 9999,
                'type' => 'inline',
            ];
        }
    } else {
        $tempColumn = [
            'children' => [
                'label' => '[children]',
                'l10n_mode' => 'exclude',
                'config' => [
                    'foreign_table' => 'sys_category',
                    'appearance' => [
                        'collapseAll' => true,
                        'expandSingle' => true,
                    ],
                    'foreign_field' => 'parent',
                    'maxitems' => 9999,
                    'type' => 'inline',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_category', $tempColumn);
    }
    if (array_key_exists('items', $GLOBALS['TCA']['sys_category']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['sys_category']['columns']['items'])) {
            $GLOBALS['TCA']['sys_category']['columns']['items']['config']['type'] = 'passthrough';
        } else {
            $GLOBALS['TCA']['sys_category']['columns']['items']['config'] = [
                'type' => 'passthrough',
            ];
        }
    } else {
        $tempColumn = [
            'items' => [
                'label' => '[items]',
                'config' => [
                    'type' => 'passthrough',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_category', $tempColumn);
    }
} else {
    $GLOBALS['TCA']['sys_category'] = [
        'ctrl' => [
            'title' => '[syscategory]',
            'label' => 'uid',
            'dividers2tabs' => true,
            'sortby' => 'sorting',
            'default_sortby' => 'ORDER BY sorting ASC',
            'hideAtCopy' => true,
            'prependAtCopy' => '',
            'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('imia_pageteaser', 'Configuration/Cache/TCA/Category.php'),
        ]
    ];
}
// ******************************************************************
// TCA for IMIA\ImiaPageteaser\Domain\Model\Page
// (pages)
// ******************************************************************
if (array_key_exists('pages', $GLOBALS['TCA'])) {
    $GLOBALS['TCA']['pages']['ctrl']['hideAtCopy'] = true;
    $GLOBALS['TCA']['pages']['ctrl']['prependAtCopy'] = '';
    if (array_key_exists('types', $GLOBALS['TCA']['pages'])) {
        if (array_key_exists('196', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['196']['showitem'] = '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['196'] = [
                'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('100', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['100']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['100'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('103', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['103']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['103'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('104', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['104']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['104'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('106', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['106']['showitem'] = '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['106'] = [
                'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('110', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['110']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['110'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('111', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['111']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['111'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('112', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['112']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['112'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('113', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['113']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['113'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('114', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['114']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['114'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('115', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['115']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['115'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('116', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['116']['showitem'] = '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['116'] = [
                'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('120', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['120']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['120'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('123', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['123']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['123'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('124', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['124']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['124'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('126', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['126']['showitem'] = '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['126'] = [
                'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('130', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['130']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['130'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('133', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['133']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['133'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('134', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['134']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['134'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('136', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['136']['showitem'] = '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['136'] = [
                'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('140', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['140']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['140'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('143', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['143']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['143'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('144', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['144']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['144'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('146', $GLOBALS['TCA']['pages']['types'])) {
            $GLOBALS['TCA']['pages']['types']['146']['showitem'] = '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages']['types']['146'] = [
                'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
    } else {
        $GLOBALS['TCA']['pages']['types'] = [
                196 => [
                    'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                100 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                103 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                104 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                106 => [
                    'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                110 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                111 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                112 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                113 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                114 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                115 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                116 => [
                    'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                120 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                123 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                124 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                126 => [
                    'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                130 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                133 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                134 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                136 => [
                    'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                140 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                143 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                144 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                146 => [
                    'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, --palette--;;archiving,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.language;language,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,categories,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
            ];
    }
    if (array_key_exists('palettes', $GLOBALS['TCA']['pages'])) {
        if (array_key_exists('language', $GLOBALS['TCA']['pages']['palettes'])) {
            $GLOBALS['TCA']['pages']['palettes']['language']['showitem'] = 'create_language, --linebreak--, l18n_cfg;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.l18n_cfg_formlabel';
            $GLOBALS['TCA']['pages']['palettes']['language']['canNotCollapse'] = true;
        } else {
            $GLOBALS['TCA']['pages']['palettes']['language'] = [
                'showitem' => 'create_language, --linebreak--, l18n_cfg;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.l18n_cfg_formlabel',
                'canNotCollapse' => true,
            ];
        }
        if (array_key_exists('filepalette', $GLOBALS['TCA']['pages']['palettes'])) {
            $GLOBALS['TCA']['pages']['palettes']['filepalette']['showitem'] = 'doktype;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.doktype_formlabel, --linebreak--, file';
            $GLOBALS['TCA']['pages']['palettes']['filepalette']['canNotCollapse'] = true;
        } else {
            $GLOBALS['TCA']['pages']['palettes']['filepalette'] = [
                'showitem' => 'doktype;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.doktype_formlabel, --linebreak--, file',
                'canNotCollapse' => true,
            ];
        }
        if (array_key_exists('archiving', $GLOBALS['TCA']['pages']['palettes'])) {
            $GLOBALS['TCA']['pages']['palettes']['archiving']['showitem'] = 'archivedate';
            $GLOBALS['TCA']['pages']['palettes']['archiving']['canNotCollapse'] = true;
        } else {
            $GLOBALS['TCA']['pages']['palettes']['archiving'] = [
                'showitem' => 'archivedate',
                'canNotCollapse' => true,
            ];
        }
        if (array_key_exists('miscalt', $GLOBALS['TCA']['pages']['palettes'])) {
            $GLOBALS['TCA']['pages']['palettes']['miscalt']['showitem'] = 'no_search;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.no_search_formlabel, editlock;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.editlock_formlabel';
            $GLOBALS['TCA']['pages']['palettes']['miscalt']['canNotCollapse'] = true;
        } else {
            $GLOBALS['TCA']['pages']['palettes']['miscalt'] = [
                'showitem' => 'no_search;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.no_search_formlabel, editlock;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.editlock_formlabel',
                'canNotCollapse' => true,
            ];
        }
    } else {
        $GLOBALS['TCA']['pages']['palettes'] = [
                'language' => [
                    'showitem' => 'create_language, --linebreak--, l18n_cfg;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.l18n_cfg_formlabel',
                    'canNotCollapse' => true,
                ],
                'filepalette' => [
                    'showitem' => 'doktype;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.doktype_formlabel, --linebreak--, file',
                    'canNotCollapse' => true,
                ],
                'archiving' => [
                    'showitem' => 'archivedate',
                    'canNotCollapse' => true,
                ],
                'miscalt' => [
                    'showitem' => 'no_search;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.no_search_formlabel, editlock;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.editlock_formlabel',
                    'canNotCollapse' => true,
                ],
            ];
    }
    if (array_key_exists('pid', $GLOBALS['TCA']['pages']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['pages']['columns']['pid'])) {
            $GLOBALS['TCA']['pages']['columns']['pid']['config']['type'] = 'passthrough';
        } else {
            $GLOBALS['TCA']['pages']['columns']['pid']['config'] = [
                'type' => 'passthrough',
            ];
        }
    } else {
        $tempColumn = [
            'pid' => [
                'label' => '[pid]',
                'config' => [
                    'type' => 'passthrough',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $tempColumn);
    }
    if (array_key_exists('doktype', $GLOBALS['TCA']['pages']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['pages']['columns']['doktype'])) {
            if (array_key_exists('items', $GLOBALS['TCA']['pages']['columns']['doktype']['config'])) {
                if (array_key_exists('196', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['196']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_default_file';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['196']['1'] = 196;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['196']['2'] = 'tcarecords-pages-196';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['196'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_default_file',
                1 => 196,
                2 => 'tcarecords-pages-196',
            ];
                }
                if (array_key_exists('99', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['99']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_news';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['99']['1'] = '--div--';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['99'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_news',
                1 => '--div--',
            ];
                }
                if (array_key_exists('100', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['100']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['100']['1'] = 100;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['100']['2'] = 'tcarecords-pages-100';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['100'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news',
                1 => 100,
                2 => 'tcarecords-pages-100',
            ];
                }
                if (array_key_exists('101', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['101']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_shortcut';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['101']['1'] = 104;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['101']['2'] = 'tcarecords-pages-104';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['101'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_shortcut',
                1 => 104,
                2 => 'tcarecords-pages-104',
            ];
                }
                if (array_key_exists('102', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['102']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_external';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['102']['1'] = 103;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['102']['2'] = 'tcarecords-pages-103';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['102'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_external',
                1 => 103,
                2 => 'tcarecords-pages-103',
            ];
                }
                if (array_key_exists('103', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['103']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_file';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['103']['1'] = 106;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['103']['2'] = 'tcarecords-pages-106';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['103'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_file',
                1 => 106,
                2 => 'tcarecords-pages-106',
            ];
                }
                if (array_key_exists('109', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['109']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_gallery';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['109']['1'] = '--div--';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['109'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_gallery',
                1 => '--div--',
            ];
                }
                if (array_key_exists('110', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['110']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['110']['1'] = 110;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['110']['2'] = 'tcarecords-pages-110';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['110'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery',
                1 => 110,
                2 => 'tcarecords-pages-110',
            ];
                }
                if (array_key_exists('112', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['112']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_shortcut';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['112']['1'] = 114;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['112']['2'] = 'tcarecords-pages-114';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['112'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_shortcut',
                1 => 114,
                2 => 'tcarecords-pages-114',
            ];
                }
                if (array_key_exists('113', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['113']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_external';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['113']['1'] = 113;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['113']['2'] = 'tcarecords-pages-113';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['113'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_external',
                1 => 113,
                2 => 'tcarecords-pages-113',
            ];
                }
                if (array_key_exists('114', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['114']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_images';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['114']['1'] = 111;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['114']['2'] = 'tcarecords-pages-111';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['114'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_images',
                1 => 111,
                2 => 'tcarecords-pages-111',
            ];
                }
                if (array_key_exists('115', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['115']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_video';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['115']['1'] = 112;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['115']['2'] = 'tcarecords-pages-112';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['115'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_video',
                1 => 112,
                2 => 'tcarecords-pages-112',
            ];
                }
                if (array_key_exists('116', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['116']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_pdf';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['116']['1'] = 115;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['116']['2'] = 'tcarecords-pages-115';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['116'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_pdf',
                1 => 115,
                2 => 'tcarecords-pages-115',
            ];
                }
                if (array_key_exists('117', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['117']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_file';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['117']['1'] = 116;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['117']['2'] = 'tcarecords-pages-116';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['117'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_file',
                1 => 116,
                2 => 'tcarecords-pages-116',
            ];
                }
                if (array_key_exists('119', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['119']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_product';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['119']['1'] = '--div--';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['119'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_product',
                1 => '--div--',
            ];
                }
                if (array_key_exists('120', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['120']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['120']['1'] = 120;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['120']['2'] = 'tcarecords-pages-120';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['120'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product',
                1 => 120,
                2 => 'tcarecords-pages-120',
            ];
                }
                if (array_key_exists('122', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['122']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_shortcut';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['122']['1'] = 124;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['122']['2'] = 'tcarecords-pages-124';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['122'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_shortcut',
                1 => 124,
                2 => 'tcarecords-pages-124',
            ];
                }
                if (array_key_exists('123', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['123']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_external';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['123']['1'] = 123;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['123']['2'] = 'tcarecords-pages-123';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['123'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_external',
                1 => 123,
                2 => 'tcarecords-pages-123',
            ];
                }
                if (array_key_exists('124', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['124']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_file';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['124']['1'] = 126;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['124']['2'] = 'tcarecords-pages-126';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['124'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_file',
                1 => 126,
                2 => 'tcarecords-pages-126',
            ];
                }
                if (array_key_exists('129', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['129']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_event';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['129']['1'] = '--div--';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['129'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_event',
                1 => '--div--',
            ];
                }
                if (array_key_exists('130', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['130']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['130']['1'] = 130;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['130']['2'] = 'tcarecords-pages-130';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['130'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event',
                1 => 130,
                2 => 'tcarecords-pages-130',
            ];
                }
                if (array_key_exists('132', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['132']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_shortcut';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['132']['1'] = 134;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['132']['2'] = 'tcarecords-pages-134';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['132'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_shortcut',
                1 => 134,
                2 => 'tcarecords-pages-134',
            ];
                }
                if (array_key_exists('133', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['133']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_external';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['133']['1'] = 133;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['133']['2'] = 'tcarecords-pages-133';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['133'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_external',
                1 => 133,
                2 => 'tcarecords-pages-133',
            ];
                }
                if (array_key_exists('134', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['134']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_file';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['134']['1'] = 136;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['134']['2'] = 'tcarecords-pages-136';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['134'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_file',
                1 => 136,
                2 => 'tcarecords-pages-136',
            ];
                }
                if (array_key_exists('139', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['139']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_article';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['139']['1'] = '--div--';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['139'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_article',
                1 => '--div--',
            ];
                }
                if (array_key_exists('140', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['140']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['140']['1'] = 140;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['140']['2'] = 'tcarecords-pages-140';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['140'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article',
                1 => 140,
                2 => 'tcarecords-pages-140',
            ];
                }
                if (array_key_exists('142', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['142']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_shortcut';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['142']['1'] = 144;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['142']['2'] = 'tcarecords-pages-144';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['142'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_shortcut',
                1 => 144,
                2 => 'tcarecords-pages-144',
            ];
                }
                if (array_key_exists('143', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['143']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_external';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['143']['1'] = 143;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['143']['2'] = 'tcarecords-pages-143';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['143'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_external',
                1 => 143,
                2 => 'tcarecords-pages-143',
            ];
                }
                if (array_key_exists('144', $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['144']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_file';
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['144']['1'] = 146;
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['144']['2'] = 'tcarecords-pages-146';
                } else {
                    $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items']['144'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_file',
                1 => 146,
                2 => 'tcarecords-pages-146',
            ];
                }
            } else {
                $GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'] = [
                196 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_default_file',
                    1 => 196,
                    2 => 'tcarecords-pages-196',
                ],
                99 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_news',
                    1 => '--div--',
                ],
                100 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news',
                    1 => 100,
                    2 => 'tcarecords-pages-100',
                ],
                101 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_shortcut',
                    1 => 104,
                    2 => 'tcarecords-pages-104',
                ],
                102 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_external',
                    1 => 103,
                    2 => 'tcarecords-pages-103',
                ],
                103 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_file',
                    1 => 106,
                    2 => 'tcarecords-pages-106',
                ],
                109 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_gallery',
                    1 => '--div--',
                ],
                110 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery',
                    1 => 110,
                    2 => 'tcarecords-pages-110',
                ],
                112 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_shortcut',
                    1 => 114,
                    2 => 'tcarecords-pages-114',
                ],
                113 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_external',
                    1 => 113,
                    2 => 'tcarecords-pages-113',
                ],
                114 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_images',
                    1 => 111,
                    2 => 'tcarecords-pages-111',
                ],
                115 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_video',
                    1 => 112,
                    2 => 'tcarecords-pages-112',
                ],
                116 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_pdf',
                    1 => 115,
                    2 => 'tcarecords-pages-115',
                ],
                117 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_file',
                    1 => 116,
                    2 => 'tcarecords-pages-116',
                ],
                119 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_product',
                    1 => '--div--',
                ],
                120 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product',
                    1 => 120,
                    2 => 'tcarecords-pages-120',
                ],
                122 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_shortcut',
                    1 => 124,
                    2 => 'tcarecords-pages-124',
                ],
                123 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_external',
                    1 => 123,
                    2 => 'tcarecords-pages-123',
                ],
                124 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_file',
                    1 => 126,
                    2 => 'tcarecords-pages-126',
                ],
                129 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_event',
                    1 => '--div--',
                ],
                130 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event',
                    1 => 130,
                    2 => 'tcarecords-pages-130',
                ],
                132 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_shortcut',
                    1 => 134,
                    2 => 'tcarecords-pages-134',
                ],
                133 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_external',
                    1 => 133,
                    2 => 'tcarecords-pages-133',
                ],
                134 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_file',
                    1 => 136,
                    2 => 'tcarecords-pages-136',
                ],
                139 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_article',
                    1 => '--div--',
                ],
                140 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article',
                    1 => 140,
                    2 => 'tcarecords-pages-140',
                ],
                142 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_shortcut',
                    1 => 144,
                    2 => 'tcarecords-pages-144',
                ],
                143 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_external',
                    1 => 143,
                    2 => 'tcarecords-pages-143',
                ],
                144 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_file',
                    1 => 146,
                    2 => 'tcarecords-pages-146',
                ],
            ];
            }
            $GLOBALS['TCA']['pages']['columns']['doktype']['config']['type'] = 'select';
        } else {
            $GLOBALS['TCA']['pages']['columns']['doktype']['config'] = [
                'items' => [
                    196 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_default_file',
                        1 => 196,
                        2 => 'tcarecords-pages-196',
                    ],
                    99 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_news',
                        1 => '--div--',
                    ],
                    100 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news',
                        1 => 100,
                        2 => 'tcarecords-pages-100',
                    ],
                    101 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_shortcut',
                        1 => 104,
                        2 => 'tcarecords-pages-104',
                    ],
                    102 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_external',
                        1 => 103,
                        2 => 'tcarecords-pages-103',
                    ],
                    103 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_file',
                        1 => 106,
                        2 => 'tcarecords-pages-106',
                    ],
                    109 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_gallery',
                        1 => '--div--',
                    ],
                    110 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery',
                        1 => 110,
                        2 => 'tcarecords-pages-110',
                    ],
                    112 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_shortcut',
                        1 => 114,
                        2 => 'tcarecords-pages-114',
                    ],
                    113 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_external',
                        1 => 113,
                        2 => 'tcarecords-pages-113',
                    ],
                    114 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_images',
                        1 => 111,
                        2 => 'tcarecords-pages-111',
                    ],
                    115 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_video',
                        1 => 112,
                        2 => 'tcarecords-pages-112',
                    ],
                    116 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_pdf',
                        1 => 115,
                        2 => 'tcarecords-pages-115',
                    ],
                    117 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_file',
                        1 => 116,
                        2 => 'tcarecords-pages-116',
                    ],
                    119 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_product',
                        1 => '--div--',
                    ],
                    120 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product',
                        1 => 120,
                        2 => 'tcarecords-pages-120',
                    ],
                    122 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_shortcut',
                        1 => 124,
                        2 => 'tcarecords-pages-124',
                    ],
                    123 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_external',
                        1 => 123,
                        2 => 'tcarecords-pages-123',
                    ],
                    124 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_file',
                        1 => 126,
                        2 => 'tcarecords-pages-126',
                    ],
                    129 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_event',
                        1 => '--div--',
                    ],
                    130 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event',
                        1 => 130,
                        2 => 'tcarecords-pages-130',
                    ],
                    132 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_shortcut',
                        1 => 134,
                        2 => 'tcarecords-pages-134',
                    ],
                    133 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_external',
                        1 => 133,
                        2 => 'tcarecords-pages-133',
                    ],
                    134 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_file',
                        1 => 136,
                        2 => 'tcarecords-pages-136',
                    ],
                    139 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_article',
                        1 => '--div--',
                    ],
                    140 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article',
                        1 => 140,
                        2 => 'tcarecords-pages-140',
                    ],
                    142 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_shortcut',
                        1 => 144,
                        2 => 'tcarecords-pages-144',
                    ],
                    143 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_external',
                        1 => 143,
                        2 => 'tcarecords-pages-143',
                    ],
                    144 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_file',
                        1 => 146,
                        2 => 'tcarecords-pages-146',
                    ],
                ],
                'type' => 'select',
            ];
        }
    } else {
        $tempColumn = [
            'doktype' => [
                'label' => '[doktype]',
                'l10n_mode' => 'exclude',
                'config' => [
                    'items' => [
                        196 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_default_file',
                            1 => 196,
                            2 => 'tcarecords-pages-196',
                        ],
                        99 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_news',
                            1 => '--div--',
                        ],
                        100 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news',
                            1 => 100,
                            2 => 'tcarecords-pages-100',
                        ],
                        101 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_shortcut',
                            1 => 104,
                            2 => 'tcarecords-pages-104',
                        ],
                        102 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_external',
                            1 => 103,
                            2 => 'tcarecords-pages-103',
                        ],
                        103 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_file',
                            1 => 106,
                            2 => 'tcarecords-pages-106',
                        ],
                        109 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_gallery',
                            1 => '--div--',
                        ],
                        110 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery',
                            1 => 110,
                            2 => 'tcarecords-pages-110',
                        ],
                        112 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_shortcut',
                            1 => 114,
                            2 => 'tcarecords-pages-114',
                        ],
                        113 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_external',
                            1 => 113,
                            2 => 'tcarecords-pages-113',
                        ],
                        114 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_images',
                            1 => 111,
                            2 => 'tcarecords-pages-111',
                        ],
                        115 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_video',
                            1 => 112,
                            2 => 'tcarecords-pages-112',
                        ],
                        116 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_pdf',
                            1 => 115,
                            2 => 'tcarecords-pages-115',
                        ],
                        117 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_file',
                            1 => 116,
                            2 => 'tcarecords-pages-116',
                        ],
                        119 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_product',
                            1 => '--div--',
                        ],
                        120 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product',
                            1 => 120,
                            2 => 'tcarecords-pages-120',
                        ],
                        122 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_shortcut',
                            1 => 124,
                            2 => 'tcarecords-pages-124',
                        ],
                        123 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_external',
                            1 => 123,
                            2 => 'tcarecords-pages-123',
                        ],
                        124 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_file',
                            1 => 126,
                            2 => 'tcarecords-pages-126',
                        ],
                        129 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_event',
                            1 => '--div--',
                        ],
                        130 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event',
                            1 => 130,
                            2 => 'tcarecords-pages-130',
                        ],
                        132 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_shortcut',
                            1 => 134,
                            2 => 'tcarecords-pages-134',
                        ],
                        133 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_external',
                            1 => 133,
                            2 => 'tcarecords-pages-133',
                        ],
                        134 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_file',
                            1 => 136,
                            2 => 'tcarecords-pages-136',
                        ],
                        139 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_article',
                            1 => '--div--',
                        ],
                        140 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article',
                            1 => 140,
                            2 => 'tcarecords-pages-140',
                        ],
                        142 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_shortcut',
                            1 => 144,
                            2 => 'tcarecords-pages-144',
                        ],
                        143 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_external',
                            1 => 143,
                            2 => 'tcarecords-pages-143',
                        ],
                        144 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_file',
                            1 => 146,
                            2 => 'tcarecords-pages-146',
                        ],
                    ],
                    'renderType' => 'selectSingle',
                    'type' => 'select',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $tempColumn);
    }
    if (array_key_exists('lastUpdated', $GLOBALS['TCA']['pages']['columns'])) {
        $GLOBALS['TCA']['pages']['columns']['lastUpdated']['label'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_lastupdated';
        if (array_key_exists('config', $GLOBALS['TCA']['pages']['columns']['lastUpdated'])) {
            $GLOBALS['TCA']['pages']['columns']['lastUpdated']['config']['type'] = 'input';
        } else {
            $GLOBALS['TCA']['pages']['columns']['lastUpdated']['config'] = [
                'type' => 'input',
            ];
        }
    } else {
        $tempColumn = [
            'lastUpdated' => [
                'label' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_lastupdated',
                'l10n_mode' => 'exclude',
                'config' => [
                    'size' => 13,
                    'max' => 25,
                    'eval' => 'datetime',
                    'checkbox' => 0,
                    'renderType' => 'inputDateTime',
                    'type' => 'input',
                    'default' => 0,
                ],
                'mapOnProperty' => "lastUpdated",
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $tempColumn);
    }
    if (array_key_exists('sorting', $GLOBALS['TCA']['pages']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['pages']['columns']['sorting'])) {
            $GLOBALS['TCA']['pages']['columns']['sorting']['config']['type'] = 'passthrough';
        } else {
            $GLOBALS['TCA']['pages']['columns']['sorting']['config'] = [
                'type' => 'passthrough',
            ];
        }
    } else {
        $tempColumn = [
            'sorting' => [
                'label' => '[sorting]',
                'config' => [
                    'type' => 'passthrough',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $tempColumn);
    }
    if (array_key_exists('cruser_id', $GLOBALS['TCA']['pages']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['pages']['columns']['cruser_id'])) {
            $GLOBALS['TCA']['pages']['columns']['cruser_id']['config']['foreign_table'] = 'be_users';
            $GLOBALS['TCA']['pages']['columns']['cruser_id']['config']['foreign_table_where'] = ' AND be_users.username NOT LIKE \'\_%\'';
            $GLOBALS['TCA']['pages']['columns']['cruser_id']['config']['type'] = 'select';
        } else {
            $GLOBALS['TCA']['pages']['columns']['cruser_id']['config'] = [
                'foreign_table' => 'be_users',
                'foreign_table_where' => ' AND be_users.username NOT LIKE \'\_%\'',
                'type' => 'select',
            ];
        }
    } else {
        $tempColumn = [
            'cruser_id' => [
                'label' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_cruser',
                'l10n_mode' => 'exclude',
                'config' => [
                    'foreign_table' => 'be_users',
                    'foreign_table_where' => ' AND be_users.username NOT LIKE \'\_%\'',
                    'renderType' => 'selectSingle',
                    'type' => 'select',
                ],
                'mapOnProperty' => "cruser",
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $tempColumn);
    }
    if (array_key_exists('teaser_pages', $GLOBALS['TCA']['pages']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['pages']['columns']['teaser_pages'])) {
            $GLOBALS['TCA']['pages']['columns']['teaser_pages']['config']['internal_type'] = 'db';
            $GLOBALS['TCA']['pages']['columns']['teaser_pages']['config']['allowed'] = 'pages';
            $GLOBALS['TCA']['pages']['columns']['teaser_pages']['config']['show_thumbs'] = true;
            $GLOBALS['TCA']['pages']['columns']['teaser_pages']['config']['size'] = 5;
            $GLOBALS['TCA']['pages']['columns']['teaser_pages']['config']['maxitems'] = 5;
            $GLOBALS['TCA']['pages']['columns']['teaser_pages']['config']['minitems'] = 0;
            $GLOBALS['TCA']['pages']['columns']['teaser_pages']['config']['type'] = 'group';
        } else {
            $GLOBALS['TCA']['pages']['columns']['teaser_pages']['config'] = [
                'internal_type' => 'db',
                'allowed' => 'pages',
                'show_thumbs' => true,
                'size' => 5,
                'maxitems' => 5,
                'minitems' => 0,
                'type' => 'group',
            ];
        }
    } else {
        $tempColumn = [
            'teaser_pages' => [
                'label' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_teaserpages',
                'l10n_mode' => 'exclude',
                'config' => [
                    'internal_type' => 'db',
                    'allowed' => 'pages',
                    'show_thumbs' => true,
                    'size' => 5,
                    'maxitems' => 5,
                    'minitems' => 0,
                    'type' => 'group',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $tempColumn);
    }
    if (array_key_exists('teaser_image', $GLOBALS['TCA']['pages']['columns'])) {
        $GLOBALS['TCA']['pages']['columns']['teaser_image']['config'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('teaser_image', [
            'maxitems' => 1,
            'overrideChildTca' => [
                'types' => [
                    2 => [
                        'showitem' => 'title, alternative, crop, --palette--;;filePalette',
                    ],
                ],
            ],
        ]);
    } else {
        $tempColumn = [
            'teaser_image' => [
                'label' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_teaserimage',
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('teaser_image', [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference',
                        'headerThumbnail' => [
                            'width' => '64m',
                            'height' => '64m',
                        ],
                    ],
                    'maxitems' => 1,
                    'overrideChildTca' => [
                        'types' => [
                            0 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                            1 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                            2 => [
                                'showitem' => 'title, alternative, crop, --palette--;;filePalette',
                            ],
                            3 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                            4 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                            5 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                        ],
                    ],
                ], 'gif,jpg,jpeg,tif,tiff,bmp,pcx,tga,png,pdf,ai,svg,webp'),
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $tempColumn);
    }
    if (array_key_exists('gallery_images', $GLOBALS['TCA']['pages']['columns'])) {
        $GLOBALS['TCA']['pages']['columns']['gallery_images']['config'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('gallery_images', [
            'overrideChildTca' => [
                'types' => [
                    2 => [
                        'showitem' => 'title, alternative, description, crop, --palette--;;filePalette',
                    ],
                ],
            ],
        ]);
    } else {
        $tempColumn = [
            'gallery_images' => [
                'label' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_galleryimages',
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('gallery_images', [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference',
                        'headerThumbnail' => [
                            'width' => '64m',
                            'height' => '64m',
                        ],
                    ],
                    'overrideChildTca' => [
                        'types' => [
                            0 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                            1 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                            2 => [
                                'showitem' => 'title, alternative, description, crop, --palette--;;filePalette',
                            ],
                            3 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                            4 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                            5 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                        ],
                    ],
                ], 'gif,jpg,jpeg,tif,tiff,bmp,pcx,tga,png,pdf,ai,svg,webp'),
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $tempColumn);
    }
    if (array_key_exists('file', $GLOBALS['TCA']['pages']['columns'])) {
        $GLOBALS['TCA']['pages']['columns']['file']['config'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('file', [
            'maxitems' => 1,
            'overrideChildTca' => [
                'types' => [
                    0 => [
                        'showitem' => '--palette--;;filePalette',
                    ],
                    1 => [
                        'showitem' => '--palette--;;filePalette',
                    ],
                    2 => [
                        'showitem' => '--palette--;;filePalette',
                    ],
                    3 => [
                        'showitem' => '--palette--;;filePalette',
                    ],
                    4 => [
                        'showitem' => '--palette--;;filePalette',
                    ],
                    5 => [
                        'showitem' => '--palette--;;filePalette',
                    ],
                ],
            ],
        ]);
    } else {
        $tempColumn = [
            'file' => [
                'label' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_file',
                'l10n_mode' => 'exclude',
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('file', [
                    'appearance' => [
                        'headerThumbnail' => [
                            'width' => '64m',
                            'height' => '64m',
                        ],
                    ],
                    'maxitems' => 1,
                    'overrideChildTca' => [
                        'types' => [
                            0 => [
                                'showitem' => '--palette--;;filePalette',
                            ],
                            1 => [
                                'showitem' => '--palette--;;filePalette',
                            ],
                            2 => [
                                'showitem' => '--palette--;;filePalette',
                            ],
                            3 => [
                                'showitem' => '--palette--;;filePalette',
                            ],
                            4 => [
                                'showitem' => '--palette--;;filePalette',
                            ],
                            5 => [
                                'showitem' => '--palette--;;filePalette',
                            ],
                        ],
                    ],
                ]),
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $tempColumn);
    }
    if (array_key_exists('gallery_pdf', $GLOBALS['TCA']['pages']['columns'])) {
        $GLOBALS['TCA']['pages']['columns']['gallery_pdf']['config'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('gallery_pdf', [
            'maxitems' => 1,
        ]);
    } else {
        $tempColumn = [
            'gallery_pdf' => [
                'label' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_gallerypdf',
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('gallery_pdf', [
                    'foreign_types' => [
                        0 => [
                            'showitem' => 'title, --palette--;;filePalette',
                        ],
                        1 => [
                            'showitem' => 'title, --palette--;;filePalette',
                        ],
                        2 => [
                            'showitem' => 'title, --palette--;;filePalette',
                        ],
                        3 => [
                            'showitem' => 'title, --palette--;;filePalette',
                        ],
                        4 => [
                            'showitem' => 'title, --palette--;;filePalette',
                        ],
                        5 => [
                            'showitem' => 'title, --palette--;;filePalette',
                        ],
                    ],
                    'maxitems' => 1,
                ], 'pdf'),
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $tempColumn);
    }
    if (array_key_exists('categories', $GLOBALS['TCA']['pages']['columns'])) {
        $GLOBALS['TCA']['pages']['columns']['categories']['label'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_categories';
        if (array_key_exists('config', $GLOBALS['TCA']['pages']['columns']['categories'])) {
            $GLOBALS['TCA']['pages']['columns']['categories']['config']['foreign_table_where'] = ' AND sys_category.sys_language_uid = 0 OR sys_category.sys_language_uid = -1';
            $GLOBALS['TCA']['pages']['columns']['categories']['config']['size'] = 5;
            $GLOBALS['TCA']['pages']['columns']['categories']['config']['autoSizeMax'] = 10;
            if (array_key_exists('wizards', $GLOBALS['TCA']['pages']['columns']['categories']['config'])) {
                $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['_VERTICAL'] = 1;
                if (array_key_exists('add', $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards'])) {
                    $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['add']['type'] = 'script';
                    $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['add']['title'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_categories_add';
                    $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['add']['icon'] = 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_add.gif';
                    if (array_key_exists('params', $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['add'])) {
                        $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['add']['params']['table'] = 'sys_category';
                        $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['add']['params']['pid'] = '###SITEROOT###';
                        $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['add']['params']['setValue'] = 'prepend';
                    } else {
                        $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['add']['params'] = [
                'table' => 'sys_category',
                'pid' => '###SITEROOT###',
                'setValue' => 'prepend',
            ];
                    }
                    if (array_key_exists('module', $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['add'])) {
                        $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['add']['module']['name'] = 'wizard_add';
                    } else {
                        $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['add']['module'] = [
                'name' => 'wizard_add',
            ];
                    }
                } else {
                    $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['add'] = [
                'type' => 'script',
                'title' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_categories_add',
                'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_add.gif',
                'params' => [
                    'table' => 'sys_category',
                    'pid' => '###SITEROOT###',
                    'setValue' => 'prepend',
                ],
                'module' => [
                    'name' => 'wizard_add',
                ],
            ];
                }
                if (array_key_exists('list', $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards'])) {
                    $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['list']['type'] = 'script';
                    $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['list']['title'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_categories_list';
                    $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['list']['icon'] = 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_list.gif';
                    if (array_key_exists('params', $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['list'])) {
                        $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['list']['params']['table'] = 'sys_category';
                        $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['list']['params']['pid'] = '###SITEROOT###';
                    } else {
                        $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['list']['params'] = [
                'table' => 'sys_category',
                'pid' => '###SITEROOT###',
            ];
                    }
                    if (array_key_exists('module', $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['list'])) {
                        $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['list']['module']['name'] = 'wizard_list';
                    } else {
                        $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['list']['module'] = [
                'name' => 'wizard_list',
            ];
                    }
                } else {
                    $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards']['list'] = [
                'type' => 'script',
                'title' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_categories_list',
                'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_list.gif',
                'params' => [
                    'table' => 'sys_category',
                    'pid' => '###SITEROOT###',
                ],
                'module' => [
                    'name' => 'wizard_list',
                ],
            ];
                }
            } else {
                $GLOBALS['TCA']['pages']['columns']['categories']['config']['wizards'] = [
                '_VERTICAL' => 1,
                'add' => [
                    'type' => 'script',
                    'title' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_categories_add',
                    'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_add.gif',
                    'params' => [
                        'table' => 'sys_category',
                        'pid' => '###SITEROOT###',
                        'setValue' => 'prepend',
                    ],
                    'module' => [
                        'name' => 'wizard_add',
                    ],
                ],
                'list' => [
                    'type' => 'script',
                    'title' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_categories_list',
                    'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_list.gif',
                    'params' => [
                        'table' => 'sys_category',
                        'pid' => '###SITEROOT###',
                    ],
                    'module' => [
                        'name' => 'wizard_list',
                    ],
                ],
            ];
            }
            $GLOBALS['TCA']['pages']['columns']['categories']['config']['type'] = 'select';
        } else {
            $GLOBALS['TCA']['pages']['columns']['categories']['config'] = [
                'foreign_table_where' => ' AND sys_category.sys_language_uid = 0 OR sys_category.sys_language_uid = -1',
                'size' => 5,
                'autoSizeMax' => 10,
                'wizards' => [
                    '_VERTICAL' => 1,
                    'add' => [
                        'type' => 'script',
                        'title' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_categories_add',
                        'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_add.gif',
                        'params' => [
                            'table' => 'sys_category',
                            'pid' => '###SITEROOT###',
                            'setValue' => 'prepend',
                        ],
                        'module' => [
                            'name' => 'wizard_add',
                        ],
                    ],
                    'list' => [
                        'type' => 'script',
                        'title' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_categories_list',
                        'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_list.gif',
                        'params' => [
                            'table' => 'sys_category',
                            'pid' => '###SITEROOT###',
                        ],
                        'module' => [
                            'name' => 'wizard_list',
                        ],
                    ],
                ],
                'type' => 'select',
            ];
        }
    } else {
        $tempColumn = [
            'categories' => [
                'label' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_categories',
                'l10n_mode' => 'exclude',
                'config' => [
                    'foreign_table' => 'sys_category',
                    'foreign_table_where' => ' AND sys_category.sys_language_uid = 0 OR sys_category.sys_language_uid = -1',
                    'MM' => 'sys_category_record_mm',
                    'MM_opposite_field' => 'items',
                    'MM_match_fields' => [
                        'tablenames' => '###CURRENT_TABLE###',
                    ],
                    'size' => 5,
                    'autoSizeMax' => 10,
                    'renderMode' => 'tree',
                    'treeConfig' => [
                        'parentField' => 'parent',
                        'appearance' => [
                            'expandAll' => true,
                            'showHeader' => true,
                        ],
                    ],
                    'maxitems' => 9999,
                    'wizards' => [
                        '_VERTICAL' => 1,
                        'add' => [
                            'type' => 'script',
                            'title' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_categories_add',
                            'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_add.gif',
                            'params' => [
                                'table' => 'sys_category',
                                'pid' => '###SITEROOT###',
                                'setValue' => 'prepend',
                            ],
                            'module' => [
                                'name' => 'wizard_add',
                            ],
                        ],
                        'list' => [
                            'type' => 'script',
                            'title' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_categories_list',
                            'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_list.gif',
                            'params' => [
                                'table' => 'sys_category',
                                'pid' => '###SITEROOT###',
                            ],
                            'module' => [
                                'name' => 'wizard_list',
                            ],
                        ],
                    ],
                    'type' => 'select',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $tempColumn);
    }
    if (array_key_exists('archivedate', $GLOBALS['TCA']['pages']['columns'])) {
        $GLOBALS['TCA']['pages']['columns']['archivedate']['label'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_archivedate';
        if (array_key_exists('config', $GLOBALS['TCA']['pages']['columns']['archivedate'])) {
            $GLOBALS['TCA']['pages']['columns']['archivedate']['config']['type'] = 'input';
        } else {
            $GLOBALS['TCA']['pages']['columns']['archivedate']['config'] = [
                'type' => 'input',
            ];
        }
    } else {
        $tempColumn = [
            'archivedate' => [
                'label' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_archivedate',
                'l10n_mode' => 'exclude',
                'config' => [
                    'size' => 13,
                    'max' => 25,
                    'eval' => 'datetime',
                    'checkbox' => 0,
                    'renderType' => 'inputDateTime',
                    'type' => 'input',
                    'default' => 0,
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $tempColumn);
    }
    if (array_key_exists('create_language', $GLOBALS['TCA']['pages']['columns'])) {
        $GLOBALS['TCA']['pages']['columns']['create_language']['displayCond'] = 'USER:IMIA\ImiaPageteaser\User\Page->checkCreateLanguage';
        if (array_key_exists('config', $GLOBALS['TCA']['pages']['columns']['create_language'])) {
            $GLOBALS['TCA']['pages']['columns']['create_language']['config']['special'] = 'languages';
            $GLOBALS['TCA']['pages']['columns']['create_language']['config']['size'] = 1;
            $GLOBALS['TCA']['pages']['columns']['create_language']['config']['maxitems'] = 1;
            $GLOBALS['TCA']['pages']['columns']['create_language']['config']['minitems'] = 1;
            $GLOBALS['TCA']['pages']['columns']['create_language']['config']['type'] = 'select';
            $GLOBALS['TCA']['pages']['columns']['create_language']['config']['default'] = '0';
        } else {
            $GLOBALS['TCA']['pages']['columns']['create_language']['config'] = [
                'special' => 'languages',
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 1,
                'type' => 'select',
                'default' => '0',
            ];
        }
    } else {
        $tempColumn = [
            'create_language' => [
                'label' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_createlanguage',
                'l10n_mode' => 'exclude',
                'displayCond' => 'USER:IMIA\ImiaPageteaser\User\Page->checkCreateLanguage',
                'config' => [
                    'special' => 'languages',
                    'size' => 1,
                    'renderType' => 'selectSingle',
                    'maxitems' => 1,
                    'minitems' => 1,
                    'type' => 'select',
                    'default' => '0',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $tempColumn);
    }
} else {
    $GLOBALS['TCA']['pages'] = [
        'ctrl' => [
            'title' => '[pages]',
            'label' => 'uid',
            'dividers2tabs' => true,
            'hideAtCopy' => true,
            'prependAtCopy' => '',
            'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('imia_pageteaser', 'Configuration/Cache/TCA/Page.php'),
        ]
    ];
}
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('pages', 'teaser_image', '', 'after:target');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('pages', 'teaser_pages', '', 'after:target');
// ******************************************************************
// TCA for IMIA\ImiaPageteaser\Domain\Model\PageAlt
// (pages_language_overlay)
// ******************************************************************
if (array_key_exists('pages_language_overlay', $GLOBALS['TCA'])) {
    $GLOBALS['TCA']['pages_language_overlay']['ctrl']['hideAtCopy'] = true;
    $GLOBALS['TCA']['pages_language_overlay']['ctrl']['prependAtCopy'] = '';
    if (array_key_exists('types', $GLOBALS['TCA']['pages_language_overlay'])) {
        if (array_key_exists('196', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['196']['showitem'] = '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['196'] = [
                'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('100', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['100']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['100'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('103', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['103']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['103'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('104', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['104']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['104'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('106', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['106']['showitem'] = '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['106'] = [
                'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('110', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['110']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['110'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('111', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['111']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['111'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('112', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['112']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['112'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('113', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['113']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['113'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('114', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['114']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['114'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('115', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['115']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['115'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('116', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['116']['showitem'] = '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['116'] = [
                'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('120', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['120']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['120'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('123', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['123']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['123'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('124', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['124']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['124'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('126', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['126']['showitem'] = '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['126'] = [
                'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('130', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['130']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['130'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('133', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['133']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['133'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('134', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['134']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['134'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('136', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['136']['showitem'] = '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['136'] = [
                'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('140', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['140']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['140'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('143', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['143']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['143'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('144', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['144']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['144'] = [
                'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
        if (array_key_exists('146', $GLOBALS['TCA']['pages_language_overlay']['types'])) {
            $GLOBALS['TCA']['pages_language_overlay']['types']['146']['showitem'] = '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['types']['146'] = [
                'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            ];
        }
    } else {
        $GLOBALS['TCA']['pages_language_overlay']['types'] = [
                196 => [
                    'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                100 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                103 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                104 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                106 => [
                    'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                110 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                111 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                112 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                113 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                114 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                115 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                116 => [
                    'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                120 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                123 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                124 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                126 => [
                    'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                130 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                133 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                134 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                136 => [
                    'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                140 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.replace;replace,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.caching;caching,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.module;module,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                143 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.external;external,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                144 => [
                    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;standard,,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcut;shortcut,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.shortcutpage;shortcutpage,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
                146 => [
                    'showitem' => '-div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.standard;filepalette,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.title;title,
--div--;LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_tab_content,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.abstract;abstract, teaser_image,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;editorial, 
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.appearance,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.layout;layout,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.behaviour,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.links;links,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.miscellaneous;miscalt,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.resources,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.media;media,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.config;config,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;hiddenonly,
--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
                ],
            ];
    }
    if (array_key_exists('palettes', $GLOBALS['TCA']['pages_language_overlay'])) {
        if (array_key_exists('filepalette', $GLOBALS['TCA']['pages_language_overlay']['palettes'])) {
            $GLOBALS['TCA']['pages_language_overlay']['palettes']['filepalette']['showitem'] = 'doktype;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.doktype_formlabel, --linebreak--, file';
            $GLOBALS['TCA']['pages_language_overlay']['palettes']['filepalette']['canNotCollapse'] = true;
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['palettes']['filepalette'] = [
                'showitem' => 'doktype;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.doktype_formlabel, --linebreak--, file',
                'canNotCollapse' => true,
            ];
        }
    } else {
        $GLOBALS['TCA']['pages_language_overlay']['palettes'] = [
                'filepalette' => [
                    'showitem' => 'doktype;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.doktype_formlabel, --linebreak--, file',
                    'canNotCollapse' => true,
                ],
            ];
    }
    if (array_key_exists('doktype', $GLOBALS['TCA']['pages_language_overlay']['columns'])) {
        if (array_key_exists('config', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype'])) {
            if (array_key_exists('items', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config'])) {
                if (array_key_exists('196', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['196']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_default_file';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['196']['1'] = 196;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['196']['2'] = 'tcarecords-pages-196';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['196'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_default_file',
                1 => 196,
                2 => 'tcarecords-pages-196',
            ];
                }
                if (array_key_exists('99', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['99']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_news';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['99']['1'] = '--div--';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['99'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_news',
                1 => '--div--',
            ];
                }
                if (array_key_exists('100', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['100']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['100']['1'] = 100;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['100']['2'] = 'tcarecords-pages-100';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['100'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news',
                1 => 100,
                2 => 'tcarecords-pages-100',
            ];
                }
                if (array_key_exists('101', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['101']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_shortcut';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['101']['1'] = 104;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['101']['2'] = 'tcarecords-pages-104';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['101'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_shortcut',
                1 => 104,
                2 => 'tcarecords-pages-104',
            ];
                }
                if (array_key_exists('102', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['102']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_external';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['102']['1'] = 103;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['102']['2'] = 'tcarecords-pages-103';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['102'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_external',
                1 => 103,
                2 => 'tcarecords-pages-103',
            ];
                }
                if (array_key_exists('103', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['103']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_file';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['103']['1'] = 106;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['103']['2'] = 'tcarecords-pages-106';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['103'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_file',
                1 => 106,
                2 => 'tcarecords-pages-106',
            ];
                }
                if (array_key_exists('109', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['109']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_gallery';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['109']['1'] = '--div--';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['109'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_gallery',
                1 => '--div--',
            ];
                }
                if (array_key_exists('110', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['110']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['110']['1'] = 110;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['110']['2'] = 'tcarecords-pages-110';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['110'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery',
                1 => 110,
                2 => 'tcarecords-pages-110',
            ];
                }
                if (array_key_exists('112', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['112']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_shortcut';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['112']['1'] = 114;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['112']['2'] = 'tcarecords-pages-114';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['112'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_shortcut',
                1 => 114,
                2 => 'tcarecords-pages-114',
            ];
                }
                if (array_key_exists('113', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['113']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_external';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['113']['1'] = 113;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['113']['2'] = 'tcarecords-pages-113';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['113'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_external',
                1 => 113,
                2 => 'tcarecords-pages-113',
            ];
                }
                if (array_key_exists('114', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['114']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_images';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['114']['1'] = 111;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['114']['2'] = 'tcarecords-pages-111';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['114'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_images',
                1 => 111,
                2 => 'tcarecords-pages-111',
            ];
                }
                if (array_key_exists('115', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['115']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_video';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['115']['1'] = 112;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['115']['2'] = 'tcarecords-pages-112';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['115'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_video',
                1 => 112,
                2 => 'tcarecords-pages-112',
            ];
                }
                if (array_key_exists('116', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['116']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_pdf';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['116']['1'] = 115;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['116']['2'] = 'tcarecords-pages-115';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['116'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_pdf',
                1 => 115,
                2 => 'tcarecords-pages-115',
            ];
                }
                if (array_key_exists('117', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['117']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_file';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['117']['1'] = 116;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['117']['2'] = 'tcarecords-pages-116';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['117'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_file',
                1 => 116,
                2 => 'tcarecords-pages-116',
            ];
                }
                if (array_key_exists('119', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['119']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_product';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['119']['1'] = '--div--';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['119'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_product',
                1 => '--div--',
            ];
                }
                if (array_key_exists('120', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['120']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['120']['1'] = 120;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['120']['2'] = 'tcarecords-pages-120';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['120'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product',
                1 => 120,
                2 => 'tcarecords-pages-120',
            ];
                }
                if (array_key_exists('122', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['122']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_shortcut';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['122']['1'] = 124;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['122']['2'] = 'tcarecords-pages-124';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['122'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_shortcut',
                1 => 124,
                2 => 'tcarecords-pages-124',
            ];
                }
                if (array_key_exists('123', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['123']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_external';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['123']['1'] = 123;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['123']['2'] = 'tcarecords-pages-123';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['123'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_external',
                1 => 123,
                2 => 'tcarecords-pages-123',
            ];
                }
                if (array_key_exists('124', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['124']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_file';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['124']['1'] = 126;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['124']['2'] = 'tcarecords-pages-126';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['124'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_file',
                1 => 126,
                2 => 'tcarecords-pages-126',
            ];
                }
                if (array_key_exists('129', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['129']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_event';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['129']['1'] = '--div--';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['129'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_event',
                1 => '--div--',
            ];
                }
                if (array_key_exists('130', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['130']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['130']['1'] = 130;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['130']['2'] = 'tcarecords-pages-130';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['130'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event',
                1 => 130,
                2 => 'tcarecords-pages-130',
            ];
                }
                if (array_key_exists('132', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['132']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_shortcut';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['132']['1'] = 134;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['132']['2'] = 'tcarecords-pages-134';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['132'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_shortcut',
                1 => 134,
                2 => 'tcarecords-pages-134',
            ];
                }
                if (array_key_exists('133', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['133']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_external';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['133']['1'] = 133;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['133']['2'] = 'tcarecords-pages-133';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['133'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_external',
                1 => 133,
                2 => 'tcarecords-pages-133',
            ];
                }
                if (array_key_exists('134', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['134']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_file';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['134']['1'] = 136;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['134']['2'] = 'tcarecords-pages-136';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['134'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_file',
                1 => 136,
                2 => 'tcarecords-pages-136',
            ];
                }
                if (array_key_exists('139', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['139']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_article';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['139']['1'] = '--div--';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['139'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_article',
                1 => '--div--',
            ];
                }
                if (array_key_exists('140', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['140']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['140']['1'] = 140;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['140']['2'] = 'tcarecords-pages-140';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['140'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article',
                1 => 140,
                2 => 'tcarecords-pages-140',
            ];
                }
                if (array_key_exists('142', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['142']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_shortcut';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['142']['1'] = 144;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['142']['2'] = 'tcarecords-pages-144';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['142'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_shortcut',
                1 => 144,
                2 => 'tcarecords-pages-144',
            ];
                }
                if (array_key_exists('143', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['143']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_external';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['143']['1'] = 143;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['143']['2'] = 'tcarecords-pages-143';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['143'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_external',
                1 => 143,
                2 => 'tcarecords-pages-143',
            ];
                }
                if (array_key_exists('144', $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'])) {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['144']['0'] = 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_file';
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['144']['1'] = 146;
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['144']['2'] = 'tcarecords-pages-146';
                } else {
                    $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items']['144'] = [
                0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_file',
                1 => 146,
                2 => 'tcarecords-pages-146',
            ];
                }
            } else {
                $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'] = [
                196 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_default_file',
                    1 => 196,
                    2 => 'tcarecords-pages-196',
                ],
                99 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_news',
                    1 => '--div--',
                ],
                100 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news',
                    1 => 100,
                    2 => 'tcarecords-pages-100',
                ],
                101 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_shortcut',
                    1 => 104,
                    2 => 'tcarecords-pages-104',
                ],
                102 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_external',
                    1 => 103,
                    2 => 'tcarecords-pages-103',
                ],
                103 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_file',
                    1 => 106,
                    2 => 'tcarecords-pages-106',
                ],
                109 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_gallery',
                    1 => '--div--',
                ],
                110 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery',
                    1 => 110,
                    2 => 'tcarecords-pages-110',
                ],
                112 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_shortcut',
                    1 => 114,
                    2 => 'tcarecords-pages-114',
                ],
                113 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_external',
                    1 => 113,
                    2 => 'tcarecords-pages-113',
                ],
                114 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_images',
                    1 => 111,
                    2 => 'tcarecords-pages-111',
                ],
                115 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_video',
                    1 => 112,
                    2 => 'tcarecords-pages-112',
                ],
                116 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_pdf',
                    1 => 115,
                    2 => 'tcarecords-pages-115',
                ],
                117 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_file',
                    1 => 116,
                    2 => 'tcarecords-pages-116',
                ],
                119 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_product',
                    1 => '--div--',
                ],
                120 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product',
                    1 => 120,
                    2 => 'tcarecords-pages-120',
                ],
                122 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_shortcut',
                    1 => 124,
                    2 => 'tcarecords-pages-124',
                ],
                123 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_external',
                    1 => 123,
                    2 => 'tcarecords-pages-123',
                ],
                124 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_file',
                    1 => 126,
                    2 => 'tcarecords-pages-126',
                ],
                129 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_event',
                    1 => '--div--',
                ],
                130 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event',
                    1 => 130,
                    2 => 'tcarecords-pages-130',
                ],
                132 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_shortcut',
                    1 => 134,
                    2 => 'tcarecords-pages-134',
                ],
                133 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_external',
                    1 => 133,
                    2 => 'tcarecords-pages-133',
                ],
                134 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_file',
                    1 => 136,
                    2 => 'tcarecords-pages-136',
                ],
                139 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_article',
                    1 => '--div--',
                ],
                140 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article',
                    1 => 140,
                    2 => 'tcarecords-pages-140',
                ],
                142 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_shortcut',
                    1 => 144,
                    2 => 'tcarecords-pages-144',
                ],
                143 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_external',
                    1 => 143,
                    2 => 'tcarecords-pages-143',
                ],
                144 => [
                    0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_file',
                    1 => 146,
                    2 => 'tcarecords-pages-146',
                ],
            ];
            }
            $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['type'] = 'select';
        } else {
            $GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config'] = [
                'items' => [
                    196 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_default_file',
                        1 => 196,
                        2 => 'tcarecords-pages-196',
                    ],
                    99 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_news',
                        1 => '--div--',
                    ],
                    100 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news',
                        1 => 100,
                        2 => 'tcarecords-pages-100',
                    ],
                    101 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_shortcut',
                        1 => 104,
                        2 => 'tcarecords-pages-104',
                    ],
                    102 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_external',
                        1 => 103,
                        2 => 'tcarecords-pages-103',
                    ],
                    103 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_file',
                        1 => 106,
                        2 => 'tcarecords-pages-106',
                    ],
                    109 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_gallery',
                        1 => '--div--',
                    ],
                    110 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery',
                        1 => 110,
                        2 => 'tcarecords-pages-110',
                    ],
                    112 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_shortcut',
                        1 => 114,
                        2 => 'tcarecords-pages-114',
                    ],
                    113 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_external',
                        1 => 113,
                        2 => 'tcarecords-pages-113',
                    ],
                    114 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_images',
                        1 => 111,
                        2 => 'tcarecords-pages-111',
                    ],
                    115 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_video',
                        1 => 112,
                        2 => 'tcarecords-pages-112',
                    ],
                    116 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_pdf',
                        1 => 115,
                        2 => 'tcarecords-pages-115',
                    ],
                    117 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_file',
                        1 => 116,
                        2 => 'tcarecords-pages-116',
                    ],
                    119 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_product',
                        1 => '--div--',
                    ],
                    120 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product',
                        1 => 120,
                        2 => 'tcarecords-pages-120',
                    ],
                    122 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_shortcut',
                        1 => 124,
                        2 => 'tcarecords-pages-124',
                    ],
                    123 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_external',
                        1 => 123,
                        2 => 'tcarecords-pages-123',
                    ],
                    124 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_file',
                        1 => 126,
                        2 => 'tcarecords-pages-126',
                    ],
                    129 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_event',
                        1 => '--div--',
                    ],
                    130 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event',
                        1 => 130,
                        2 => 'tcarecords-pages-130',
                    ],
                    132 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_shortcut',
                        1 => 134,
                        2 => 'tcarecords-pages-134',
                    ],
                    133 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_external',
                        1 => 133,
                        2 => 'tcarecords-pages-133',
                    ],
                    134 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_file',
                        1 => 136,
                        2 => 'tcarecords-pages-136',
                    ],
                    139 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_article',
                        1 => '--div--',
                    ],
                    140 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article',
                        1 => 140,
                        2 => 'tcarecords-pages-140',
                    ],
                    142 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_shortcut',
                        1 => 144,
                        2 => 'tcarecords-pages-144',
                    ],
                    143 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_external',
                        1 => 143,
                        2 => 'tcarecords-pages-143',
                    ],
                    144 => [
                        0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_file',
                        1 => 146,
                        2 => 'tcarecords-pages-146',
                    ],
                ],
                'type' => 'select',
            ];
        }
    } else {
        $tempColumn = [
            'doktype' => [
                'label' => '[doktype]',
                'l10n_mode' => 'exclude',
                'config' => [
                    'items' => [
                        196 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_default_file',
                            1 => 196,
                            2 => 'tcarecords-pages-196',
                        ],
                        99 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_news',
                            1 => '--div--',
                        ],
                        100 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news',
                            1 => 100,
                            2 => 'tcarecords-pages-100',
                        ],
                        101 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_shortcut',
                            1 => 104,
                            2 => 'tcarecords-pages-104',
                        ],
                        102 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_external',
                            1 => 103,
                            2 => 'tcarecords-pages-103',
                        ],
                        103 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_news_file',
                            1 => 106,
                            2 => 'tcarecords-pages-106',
                        ],
                        109 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_gallery',
                            1 => '--div--',
                        ],
                        110 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery',
                            1 => 110,
                            2 => 'tcarecords-pages-110',
                        ],
                        112 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_shortcut',
                            1 => 114,
                            2 => 'tcarecords-pages-114',
                        ],
                        113 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_external',
                            1 => 113,
                            2 => 'tcarecords-pages-113',
                        ],
                        114 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_images',
                            1 => 111,
                            2 => 'tcarecords-pages-111',
                        ],
                        115 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_video',
                            1 => 112,
                            2 => 'tcarecords-pages-112',
                        ],
                        116 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_pdf',
                            1 => 115,
                            2 => 'tcarecords-pages-115',
                        ],
                        117 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_gallery_file',
                            1 => 116,
                            2 => 'tcarecords-pages-116',
                        ],
                        119 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_product',
                            1 => '--div--',
                        ],
                        120 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product',
                            1 => 120,
                            2 => 'tcarecords-pages-120',
                        ],
                        122 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_shortcut',
                            1 => 124,
                            2 => 'tcarecords-pages-124',
                        ],
                        123 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_external',
                            1 => 123,
                            2 => 'tcarecords-pages-123',
                        ],
                        124 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_product_file',
                            1 => 126,
                            2 => 'tcarecords-pages-126',
                        ],
                        129 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_event',
                            1 => '--div--',
                        ],
                        130 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event',
                            1 => 130,
                            2 => 'tcarecords-pages-130',
                        ],
                        132 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_shortcut',
                            1 => 134,
                            2 => 'tcarecords-pages-134',
                        ],
                        133 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_external',
                            1 => 133,
                            2 => 'tcarecords-pages-133',
                        ],
                        134 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_event_file',
                            1 => 136,
                            2 => 'tcarecords-pages-136',
                        ],
                        139 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_divider_article',
                            1 => '--div--',
                        ],
                        140 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article',
                            1 => 140,
                            2 => 'tcarecords-pages-140',
                        ],
                        142 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_shortcut',
                            1 => 144,
                            2 => 'tcarecords-pages-144',
                        ],
                        143 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_external',
                            1 => 143,
                            2 => 'tcarecords-pages-143',
                        ],
                        144 => [
                            0 => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:page_doktype_article_file',
                            1 => 146,
                            2 => 'tcarecords-pages-146',
                        ],
                    ],
                    'renderType' => 'selectSingle',
                    'type' => 'select',
                ],
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages_language_overlay', $tempColumn);
    }
    $GLOBALS['TCA']['pages_language_overlay']['columns']['pid']['mapOnProperty'] = "page";
    if (array_key_exists('teaser_image', $GLOBALS['TCA']['pages_language_overlay']['columns'])) {
        $GLOBALS['TCA']['pages_language_overlay']['columns']['teaser_image']['l10n_mode'] = '';
        $GLOBALS['TCA']['pages_language_overlay']['columns']['teaser_image']['config'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('teaser_image', [
            'foreign_types' => [
                2 => [
                    'showitem' => 'title, alternative, crop,--palette--;;filePalette',
                ],
            ],
            'maxitems' => 1,
        ]);
    } else {
        $tempColumn = [
            'teaser_image' => [
                'label' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:pagealt_teaserimage',
                'l10n_mode' => '',
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('teaser_image', [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference',
                        'headerThumbnail' => [
                            'width' => '64m',
                            'height' => '64m',
                        ],
                    ],
                    'foreign_types' => [
                        2 => [
                            'showitem' => 'title, alternative, crop,--palette--;;filePalette',
                        ],
                    ],
                    'maxitems' => 1,
                    'overrideChildTca' => [
                        'types' => [
                            0 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                            1 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                            2 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                            3 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                            4 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                            5 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                        ],
                    ],
                ], 'gif,jpg,jpeg,tif,tiff,bmp,pcx,tga,png,pdf,ai,svg,webp'),
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages_language_overlay', $tempColumn);
    }
    if (array_key_exists('gallery_images', $GLOBALS['TCA']['pages_language_overlay']['columns'])) {
        $GLOBALS['TCA']['pages_language_overlay']['columns']['gallery_images']['l10n_mode'] = '';
        $GLOBALS['TCA']['pages_language_overlay']['columns']['gallery_images']['config'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('gallery_images', [
            'foreign_types' => [
                2 => [
                    'showitem' => 'title, alternative, description, crop, --palette--;;filePalette',
                ],
            ],
        ]);
    } else {
        $tempColumn = [
            'gallery_images' => [
                'label' => '[galleryimages]',
                'l10n_mode' => '',
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('gallery_images', [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference',
                        'headerThumbnail' => [
                            'width' => '64m',
                            'height' => '64m',
                        ],
                    ],
                    'foreign_types' => [
                        2 => [
                            'showitem' => 'title, alternative, description, crop, --palette--;;filePalette',
                        ],
                    ],
                    'overrideChildTca' => [
                        'types' => [
                            0 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                            1 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                            2 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                            3 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                            4 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                            5 => [
                                'showitem' => '--palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                            ],
                        ],
                    ],
                ], 'gif,jpg,jpeg,tif,tiff,bmp,pcx,tga,png,pdf,ai,svg,webp'),
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages_language_overlay', $tempColumn);
    }
    if (array_key_exists('file', $GLOBALS['TCA']['pages_language_overlay']['columns'])) {
        $GLOBALS['TCA']['pages_language_overlay']['columns']['file']['l10n_mode'] = 'mergeIfNotBlank';
        $GLOBALS['TCA']['pages_language_overlay']['columns']['file']['config'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('file', [
            'foreign_types' => [
                0 => [
                    'showitem' => '--palette--;;filePalette',
                ],
                1 => [
                    'showitem' => '--palette--;;filePalette',
                ],
                2 => [
                    'showitem' => '--palette--;;filePalette',
                ],
                3 => [
                    'showitem' => '--palette--;;filePalette',
                ],
                4 => [
                    'showitem' => '--palette--;;filePalette',
                ],
                5 => [
                    'showitem' => '--palette--;;filePalette',
                ],
            ],
            'maxitems' => 1,
        ]);
    } else {
        $tempColumn = [
            'file' => [
                'label' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:pagealt_file',
                'l10n_mode' => 'mergeIfNotBlank',
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('file', [
                    'appearance' => [
                        'headerThumbnail' => [
                            'width' => '64m',
                            'height' => '64m',
                        ],
                    ],
                    'foreign_types' => [
                        0 => [
                            'showitem' => '--palette--;;filePalette',
                        ],
                        1 => [
                            'showitem' => '--palette--;;filePalette',
                        ],
                        2 => [
                            'showitem' => '--palette--;;filePalette',
                        ],
                        3 => [
                            'showitem' => '--palette--;;filePalette',
                        ],
                        4 => [
                            'showitem' => '--palette--;;filePalette',
                        ],
                        5 => [
                            'showitem' => '--palette--;;filePalette',
                        ],
                    ],
                    'maxitems' => 1,
                ]),
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages_language_overlay', $tempColumn);
    }
    if (array_key_exists('gallery_pdf', $GLOBALS['TCA']['pages_language_overlay']['columns'])) {
        $GLOBALS['TCA']['pages_language_overlay']['columns']['gallery_pdf']['config'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('gallery_pdf', [
            'maxitems' => 1,
        ]);
    } else {
        $tempColumn = [
            'gallery_pdf' => [
                'label' => 'LLL:EXT:imia_pageteaser/Resources/Private/Language/DB.xlf:pagealt_gallerypdf',
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('gallery_pdf', [
                    'foreign_types' => [
                        0 => [
                            'showitem' => 'title, --palette--;;filePalette',
                        ],
                        1 => [
                            'showitem' => 'title, --palette--;;filePalette',
                        ],
                        2 => [
                            'showitem' => 'title, --palette--;;filePalette',
                        ],
                        3 => [
                            'showitem' => 'title, --palette--;;filePalette',
                        ],
                        4 => [
                            'showitem' => 'title, --palette--;;filePalette',
                        ],
                        5 => [
                            'showitem' => 'title, --palette--;;filePalette',
                        ],
                    ],
                    'maxitems' => 1,
                ], 'pdf'),
            ],
        ];
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages_language_overlay', $tempColumn);
    }
} else {
    $GLOBALS['TCA']['pages_language_overlay'] = [
        'ctrl' => [
            'title' => '[pageslanguageoverlay]',
            'label' => 'uid',
            'dividers2tabs' => true,
            'hideAtCopy' => true,
            'prependAtCopy' => '',
            'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('imia_pageteaser', 'Configuration/Cache/TCA/PageAlt.php'),
        ]
    ];
}
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('pages_language_overlay', 'teaser_image', '', 'after:abstract');