### EXTENSION: imia_pageteaser ###

## CLASS: IMIA\ImiaPageteaser\Domain\Model\Category
# Table structure for table 'sys_category'
CREATE TABLE `sys_category` (
    `sorting` int(11) DEFAULT '0' NOT NULL,
    `children` int(11) DEFAULT '0' NOT NULL
);

## CLASS: IMIA\ImiaPageteaser\Domain\Model\Page
# Table structure for table 'pages'
CREATE TABLE `pages` (
    `teaser_pages` varchar(255) DEFAULT '' NOT NULL,
    `teaser_image` int(11) DEFAULT '0' NOT NULL,
    `gallery_images` int(11) DEFAULT '0' NOT NULL,
    `file` int(11) DEFAULT '0' NOT NULL,
    `gallery_pdf` int(11) DEFAULT '0' NOT NULL,
    `categories` int(11) DEFAULT '0' NOT NULL,
    `archivedate` int(11) DEFAULT '0' NOT NULL,
    `create_language` int(11) DEFAULT '0' NOT NULL
);

## CLASS: IMIA\ImiaPageteaser\Domain\Model\PageAlt
# Table structure for table 'pages_language_overlay'
CREATE TABLE `pages_language_overlay` (
    `teaser_image` int(11) DEFAULT '0' NOT NULL,
    `gallery_images` int(11) DEFAULT '0' NOT NULL,
    `file` int(11) DEFAULT '0' NOT NULL,
    `gallery_pdf` int(11) DEFAULT '0' NOT NULL
);

